import { LightningElement, track, wire, api } from "lwc";
import { getListUi } from "lightning/uiListApi";
import { getRecord } from "lightning/uiRecordApi";
import getTimesheet from "@salesforce/apex/ganttChart.getTimesheetForDate";
import USER_ID from "@salesforce/user/Id";
import NAME_FIELD from "@salesforce/schema/User.Name";

export default class TimeLogger extends LightningElement {
    @track resourceAssignmentOptions = [];
    @track timesheetWeek;
    @track timesheetHours;
    @track userName;
    @api assignmentListviewId;

    @wire(getRecord, {
        recordId: USER_ID,
        fields: [NAME_FIELD]
    })
    getUser({ error, data }) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.userName = data.fields.Name.value;
        }
    }

    @wire(getListUi, {
        listViewId: "00B0W000006cVnHUAU"
    })
    getResourceAssignmentOptions({ data, error }) {
        if (error) {
            // this.error = error;
        } else if (data) {
            this.resourceAssignmentOptions = data.records.records.map(record => {
                return {
                    label: record.fields.Project_Name__c.value,
                    value: record.id
                };
            });
        }
    }

    loadTimesheet(date) {
        getTimesheet({ dateWorked: date })
            .then(result => {
                this.template.querySelector('[data-name="FMZ_Timesheet__c"]').value = result.Id;
                this.timesheetHours = result.Total_Hours__c;
                this.timesheetWeek = `${new Date(result.Week_Start_Date__c).toLocaleDateString()} - ${new Date(
                    result.Week_End_Date__c
                ).toLocaleDateString()}`;
            })
            .catch(err => {});
    }

    handleDateChange(evt) {
        this.loadTimesheet(evt.detail.value);
    }

    handleResourceAssignmentSelected(evt) {
        this.template.querySelector('[data-name="Resource_Assignment__c"]').value = evt.detail.value;
    }

    /**
     * record-edit-form methods
     */
    submit() {
        this.template.querySelector('[data-name="form"]').submit();
    }

    handleLoad(evt) {
        this.template.querySelector('[data-name="Resource_Assignment_Selector"]').focus();
        if (!this.timesheet) {
            this.loadTimesheet();
        }
    }

    handleSuccess() {
        // reset form
        const inputFields = this.template.querySelectorAll("lightning-input-field");
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }

        // reset the selected option
        this.resourceAssignment = undefined;
        this.template.querySelector('[data-name="Resource_Assignment_Selector"]').value = null;
    }
}
