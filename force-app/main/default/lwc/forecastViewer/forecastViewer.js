import { LightningElement, wire, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import * as date from "c/lwcDateFunctions";
import getResourceAssignments from "@salesforce/apex/ForecastViewerController.getResourceAssignments";
import submitTimesheetDetails from "@salesforce/apex/ForecastViewerController.submitTimesheetDetails";

export default class forecastViewer extends LightningElement {
  @track weekStartDate = date.startOfWeek(date.toDateString(new Date()));
  @track resourceAssignments;
  @track error;

  @wire(getResourceAssignments, { weekStartDate: "$weekStartDate" })
  getResourceAssignments({ error, data }) {
    if (data) {
      this.resourceAssignments = data;
      this.error = null;
    } else {
      this.resourceAssignments = null;
      this.error = error;
    }
  }

  get formattedWeekStartDate() {
    return date.displayDate(this.weekStartDate) + String.fromCharCode(160);
  }

  handleMenuSelect(event) {
    const selectedItemValue = event.detail.value;
    if (selectedItemValue === "submit") {
      submitTimesheetDetails({ weekStartDate: this.weekStartDate })
        .then(() => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Submission Successful",
              message: `Timesheet for ${this.formattedWeekStartDate} has been submitted successfully.`
            })
          );
        })
        .catch(error => {
          this.error = error;
        });
    }
  }

  handlePreviousWeekClick() {
    this.weekStartDate = date.addDays(this.weekStartDate, -7);
  }

  handleNextWeekClick() {
    this.weekStartDate = date.addDays(this.weekStartDate, 7);
  }
}