import { LightningElement, api, track } from "lwc";
import { NavigationMixin } from "lightning/navigation";

export default class ForecastViewerItem extends NavigationMixin(
  LightningElement
) {
  @api resourceAssignment;
  @track recordPageUrl;

  connectedCallback() {
    this[NavigationMixin.GenerateUrl]({
      type: "standard__recordPage",
      attributes: {
        recordId: this.resourceAssignment.Id,
        actionName: "view"
      }
    }).then(url => {
      this.recordPageUrl = url;
    });
  }

  get project() {
    return this.resourceAssignment.FMZ_Project_Role__r.FMZ_Project__r.Name;
  }

  get role() {
    return this.resourceAssignment.FMZ_Project_Role__r.Role__c;
  }

  get forecastHours() {
    if (this.resourceAssignment.FMZ_Resource_Forecasts__r) {
      return this.resourceAssignment.FMZ_Resource_Forecasts__r.reduce(
        (total, x) => total + x.Forecast_Hours__c,
        0
      );
    }
    return null;
  }

  get hoursWorked() {
    if (this.resourceAssignment.FMZ_Timesheet_Details__r) {
      return this.resourceAssignment.FMZ_Timesheet_Details__r.reduce(
        (total, x) => total + x.Hours_Worked__c,
        0
      );
    }
    return null;
  }
}