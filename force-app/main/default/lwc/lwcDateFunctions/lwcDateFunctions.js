const fromDateString = date => {
  let parts = date.split("-");
  return new Date(parts[0], parts[1] - 1, parts[2]);
};

const toDateString = date => {
  return (
    date.getFullYear() +
    "-" +
    (date.getMonth() + 1).toString().padStart(2, "0") +
    "-" +
    date
      .getDate()
      .toString()
      .padStart(2, "0")
  );
};

const displayDate = dateString => {
  return fromDateString(dateString).toLocaleDateString("en-US", {
    year: "numeric",
    month: "short",
    day: "numeric"
  });
};

const startOfMonth = dateString => {
  let date = fromDateString(dateString);
  date.setDate(1);
  return toDateString(date);
};

const startOfWeek = dateString => {
  let date = fromDateString(dateString);
  date.setDate(date.getDate() - date.getDay());
  return toDateString(date);
};

const daysBetween = (firstDateString, secondDateString) => {
  const oneDay = 24 * 60 * 60 * 1000;
  let firstDate = fromDateString(firstDateString);
  let secondDate = fromDateString(secondDateString);
  return Math.round(Math.abs((firstDate - secondDate) / oneDay));
};

const addDays = (dateString, num) => {
  let date = fromDateString(dateString);
  date.setDate(date.getDate() + num);
  return toDateString(date);
};

const addMonths = (dateString, num) => {
  let date = fromDateString(dateString);
  let currentMonth = date.getMonth() + date.getFullYear() * 12;
  date.setMonth(date.getMonth() + num);
  let diff = date.getMonth() + date.getFullYear() * 12 - currentMonth;
  if (diff !== num) {
    date.setDate(0);
  }
  return toDateString(date);
};

export {
  toDateString,
  displayDate,
  startOfMonth,
  startOfWeek,
  daysBetween,
  addDays,
  addMonths
};