trigger FMZ_Resource_AssignmentTrigger on FMZ_Resource_Assignment__c (after insert, after update) {
    ResourceAssignmentTriggerHandler handler = new ResourceAssignmentTriggerHandler();
    
    if(Trigger.isAfter){
        if (Trigger.isInsert) {
            handler.afterInsert(Trigger.new);
        }
        else if(Trigger.isUpdate){
            handler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}