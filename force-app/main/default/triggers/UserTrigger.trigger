trigger UserTrigger on User (after insert, after update) {

    UserTriggerHandler handler = new UserTriggerHandler();
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            handler.afterInsert(Trigger.new);
        }else if(Trigger.isUpdate){
            handler.afterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}