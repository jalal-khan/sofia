// ==================================================================================
//  Company : Force Optimized
//  Object : Projects_ForceOptimized__c
//  Author : Mahesh S
//  Comments : To stop making more than one project coordinator for a single opporutnity
// ==================================================================================
//  Changes: 2013-07-16 Initial version.
//           2013-07-20 Populating Hourly Rate field from user when record is created or updated
// ==================================================================================
trigger ResourceAssignment_Trigger on Projects_ForceOptimized__c (before insert, before update) {
 
    public Set<String> opportunityIds = new Set<String>();
    public Map<String, String> oppRAMap = new Map<String, String>();
    public set<String> UserIds = new Set<String>();
    public Map<String,Decimal> UserMap= new Map<String, Decimal>();
    
    for(Projects_ForceOptimized__c p : Trigger.new)
    {
        opportunityIds.add(p.Opportunity__c);
        UserIds.add(p.Resource__c);
    }
    
    List<Projects_ForceOptimized__c> RAList = [select id,
                                                        Opportunity__c from Projects_ForceOptimized__c
                                                        where Opportunity__c in: opportunityIds and Project_Coordinator__c = true];
    
    
    for(Projects_ForceOptimized__c RA : RAList)
    {
        oppRAMap.put(RA.opportunity__c, RA.id);
    }
    List<User> userList = [select id,
                                    Hourly_rate__c from User
                                    where ID in:UserIDs];
    for(User U:userList){
        Usermap.put(U.ID,U.Hourly_Rate__c);
    }
    
    for(Projects_ForceOptimized__c p : Trigger.new)
    { 
        p.Hourly_Rate__c=Usermap.get(p.Resource__C);
        if(p.Project_Coordinator__c && oppRAMap.get(p.opportunity__c) != null && oppRAMap.get(p.opportunity__c) != p.id)
        {
            p.addError('Cannot have more than one project coordinator for opportunity');
        }
    }
    
}