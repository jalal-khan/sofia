trigger ForecastTrigger on Forecast__c (before insert, before update) {
    if(Trigger.isInsert||Trigger.isUpdate) {
        if(Trigger.isBefore) {
            for(Forecast__c item: Trigger.New) {
                DateTime dt = datetime.newInstance(item.Work_Week_Start_Date__c, Time.newInstance(23,59,59,59));
                item.Work_Week_Start_Date_Number__c = dt.getTime();
            }
        }
    } 
}