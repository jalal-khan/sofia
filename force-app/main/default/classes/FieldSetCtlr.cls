public class FieldSetCtlr {
    public static boolean THROW_EXCEPTION = false; // this is so we can test failure conditions on DML
    
    @AuraEnabled
    public static sObject getRecord(Id recordId, string fieldSetName, string objectType){
        if(recordId != null){
            list<string> fieldList = FieldSetServices.getFieldsAsStringList(objectType, fieldSetName);
            string qry = 'SELECT ' + string.join(fieldList,',') + ' FROM ' + objectType;
			qry += ' WHERE Id = \''+recordId+'\'';
            qry += ' ORDER BY CreatedDate DESC';
            system.debug('qry: ' + qry);
            list<sObject> objs = Database.query(qry);
            return objs[0];
        }
        return null;
    }
    
    @AuraEnabled
    public static list<sObject> getRelatedRecords(string sObjectType, string fieldStr, string parentId, string parentField){
        list<FieldSetCtlr.FieldSetMember> fields = (list<FieldSetCtlr.FieldSetMember>)JSON.deserialize(fieldStr,list<FieldSetCtlr.FieldSetMember>.class);
        list<string> fieldList = new list<string>();
        for(FieldSetCtlr.FieldSetMember f:fields){
            fieldList.add(f.fieldPath);
        }
        string qry = 'SELECT ' + string.join(fieldList,',') + ' FROM ' + sObjectType;
        qry += ' WHERE ' + parentField + ' = \''+parentId+'\'';
        qry += ' ORDER BY CreatedDate DESC';
        system.debug('qry: ' + qry);
        return  Database.query(qry);
    }
    
	@AuraEnabled
    public static List<FieldSetCtlr.FieldSetMember> getFields(String typeName, String fsName, string viewType) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get(fsName);
        List<Schema.FieldSetMember> fieldSet = fs.getFields();
        List<FieldSetCtlr.FieldSetMember> fset = new List<FieldSetCtlr.FieldSetMember>();
        for (Schema.FieldSetMember f: fieldSet) {
            FieldSetCtlr.FieldSetMember fsmNew = new FieldSetMember(f, viewType, typeName);
            fset.add(fsmNew);
        }
        return fset;
    }
    
    @AuraEnabled
    public static ResponseObject saveRecord(string objString){
            system.debug('obj: ' + objString);
        sObject obj = (sObject)(JSON.deserialize(objString,sObject.class));
        try{
            upsert obj;
            return new ResponseObject(SUCCESS, true, null);
        }catch(Exception e){
            return new ResponseObject(FAILURE, false, e.getMessage());
        }
    }
    
    @AuraEnabled
    public static ResponseObject deleteRecord(string objString){
        sObject obj = (sObject)(JSON.deserialize(objString,sObject.class));
        try{
            if(test.isRunningTest() && THROW_EXCEPTION){
                throw new FieldSetException('test');
            }
            delete obj;
            return new ResponseObject(SUCCESS, true, null);
        }catch(Exception e){
            return new ResponseObject(FAILURE, false, e.getMessage());
        }
    }
    
    public static string SUCCESS = 'Success';
    public static string FAILURE = 'Failure';
    
    public class ResponseObject{
        @AuraEnabled
        public string status{get;set;}
        @AuraEnabled
        public boolean isSuccess{get;set;}
        @AuraEnabled
        public string errorMessage{get;set;}
        public ResponseObject(string s, boolean b, string e){
            this.status = s;
            this.isSuccess = b;
            this.errorMessage = e;
        }
    }

    public class FieldSetMember {

        public FieldSetMember(Schema.FieldSetMember f, string viewType, string objectType) {
            //system.debug('Schema.FieldSetMember f: ' + JSON.serialize(f));
            this.DBRequired = f.DBRequired;
            this.fieldPath = f.fieldPath;
            this.displayFieldPath = f.fieldPath;
            this.label = f.label;
            this.required = f.required;
            this.type = '' + f.getType();
            this.definition = new FieldSetServices.FieldDefinition(this.type, viewType, objectType);
            this.definition.attributes.put('label',this.label);
            this.definition.attributes.put('fieldPath',this.fieldPath);
            if(this.DBRequired){
            	this.definition.attributes.put('required',string.valueOf(this.DBRequired));
            }
            if(!this.definition.componentDef.startsWithIgnoreCase('ui:')){
                // this is a custom component and we need to add the related object api name.
                if(this.definition.componentDef.containsIgnoreCase('customlookup')){
                    Schema.SObjectType t = Schema.getGlobalDescribe().get(objectType);
                    Schema.DescribeSObjectResult r = t.getDescribe();
                    Schema.DescribeFieldResult fieldRes = r.fields.getMap().get(this.fieldPath).getDescribe();
                    for(Schema.SObjectType reference : fieldRes.getReferenceTo()) {
                        system.debug('setting field ref to: ' + reference.getDescribe().getName());
                        this.definition.attributes.put('objectAPIName',reference.getDescribe().getName());
                    }
                }else{
                    this.definition.attributes.put('objectAPIName',objectType);
                }
            }
            // for reference output fields, show the __r.Name
            if(this.type.toLowerCase() == 'reference' && viewType == FieldSetServices.VIEW_TYPE_OUTPUT){
                if(this.fieldPath.endsWithIgnoreCase('__c')){
                	this.fieldPath = this.fieldPath.removeEnd('c');
                    this.fieldPath += 'r';
                }else{
                    this.fieldPath = this.fieldPath.removeEnd('Id');
                }
                this.fieldPath += '.Name';
            }
        }
        
        @AuraEnabled
        public FieldSetServices.FieldDefinition definition{get;set;}

        public FieldSetMember(Boolean DBRequired) {
            this.DBRequired = DBRequired;
        }
		        
        @AuraEnabled
        public Boolean DBRequired { get;set; }

        @AuraEnabled
        public String fieldPath { get;set; }
        
        @AuraEnabled
        public String displayFieldPath { get;set; }

        @AuraEnabled
        public String label { get;set; }

        @AuraEnabled
        public Boolean required { get;set; }

        @AuraEnabled
        public String type { get; set; }
        
        @AuraEnabled
        public String viewType{get;set;}
    }
    
    public class FieldSetException extends Exception{}
}