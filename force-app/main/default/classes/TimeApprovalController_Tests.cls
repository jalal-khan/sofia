@isTest
public class TimeApprovalController_Tests {

    static list<Work_Week__c> weeks;
    static Account account;
    static integer numWeeks = 3;
    static{
        weeks = new list<Work_Week__c>();
        date startDate = date.today().toStartOfWeek();
        for(integer i=0;i<numWeeks;i++){
            Work_Week__c ww = new Work_Week__c();
            ww.Start_Date__c = startDate;
            ww.End_Date__c = startDate.addDays(6);
            weeks.add(ww);
            startDate = startDate.addDays(7);
        }
        // make one for last week.
        Work_Week__c wwPrev = new Work_Week__c();
        Date oldDate = Date.today().addDays(-7).toStartOfWeek();
        wwPrev.Start_Date__c = oldDate;
        wwPrev.End_Date__c = oldDate.addDays(6);
        weeks.add(wwPrev);
        insert weeks;
        
        account = new Account();
        account.name = 'test';
        insert account;
        
        // make 5 projects.
        list<Project__c> projects = new list<Project__c>();
        integer i = 0;
        while(i<5){
            Project__c project = new Project__c();
            project.name = 'Test project ' + i;
            project.account__c = account.Id;
            project.Project_Manager__c = UserInfo.getUserId();
            projects.add(project);
            i+=1;
        }
        insert projects;
        
        list<Work__c> works = new list<Work__c>();
        for(Project__c p:projects){
            Work__c work = new Work__c();
            work.Fortimize_Project__c = p.Id;
            work.Hour_Type__c = 'Billable';
            works.add(work);
        }
        insert works;
        
        list<Work_Task__c> workTasks = new list<Work_Task__c>();
        
        for(Work__c w:works){
            Work_Task__c workTask = new Work_Task__c();
            workTask.Allocated_Hours__c = 100;
            workTask.Assigned_to__c = UserInfo.getUserId();
            workTask.Task_Type__c = 'Other';
            workTask.Work__c = w.Id;
            workTask.Bill_Rate__c = 100;
            workTask.Task_Subject__c = 'Test Subject';
            workTasks.add(workTask);
        }
        insert workTasks;
        
        // make a timesheet task that's not been submitted
        Timesheet__c ts = new Timesheet__c();
        ts.Work_Week__c = [SELECT Id FROM Work_Week__c WHERE Start_Date__c = :Date.today().toStartOfWeek() LIMIT 1][0].Id;
        ts.Start_Date__c = Date.today().toStartOfWeek();
        ts.OwnerId = UserInfo.getUserId();
        insert ts;
        
        list<Timesheet_Task__c> tasks = new list<Timesheet_Task__c>();
        
        for(Work_Task__c wt: [SELECT Id FROM Work_Task__c]){
            Timesheet_Task__c tst = new Timesheet_Task__c();
            tst.Timesheet__c = ts.Id;
            tst.Work_Task__c = wt.Id;
            tst.Status__c = WorkTaskLine.STATUS_OPEN;
            tst.Hours_Used__c = 4;
            tasks.add(tst);
        }
        insert tasks;
        
        list<Time_Log__c> logs = new list<Time_Log__c>();

        for(Timesheet_Task__c tst:tasks){
            Time_Log__c tl = new Time_Log__c();
            tl.Timesheet_Task__c = tst.Id;
            tl.External_Comments__c = 'Test';
            tl.Hours_Completed__c = 4;
            tl.Work_Task__c = tst.Work_Task__c;
            logs.add(tl);
        }
        insert logs;
    }
    
    @isTest
    static void loadPage_NothingToApprove(){
        TimeApprovalController tac = new TimeApprovalController();
        system.assertEquals(UserInfo.getUserId(), tac.viewingAs);
        system.assertEquals(Date.today().toStartOfWeek(), tac.startDate);
        system.assertEquals(5,tac.projects.size());
        
        // simulate clicking to view a project's hours.
        Project__c pr = [SELECT Id, Name FROM Project__c LIMIT 1];
        tac.activeProject = pr.Id;
        tac.setActiveProject();
        
        system.assertEquals(pr.Name, tac.activeProjectRecord.projectName);
        system.assertEquals(1, tac.activeProjectRecord.openWorkTaskLines.size());
        system.assertEquals(4, tac.activeProjectRecord.openHours);
        
        tac.unsetActiveProject();
       	system.assertEquals(null, tac.activeProjectRecord);
    }
    
    @isTest
    static void loadPage_ToApprove(){
        list<Timesheet_Task__c> tasks = [SELECT Id FROM Timesheet_Task__c];
        for(Timesheet_Task__c t:tasks){
            t.Status__c = WorkTaskLine.STATUS_SUBMITTED;
        }
        update tasks;
        
        TimeApprovalController tac = new TimeApprovalController();
        system.assertEquals(UserInfo.getUserId(), tac.viewingAs);
        system.assertEquals(Date.today().toStartOfWeek(), tac.startDate);
        system.assertEquals(5,tac.projects.size());
        
        // simulate clicking to view a project's hours.
        Project__c pr = [SELECT Id, Name FROM Project__c LIMIT 1];
        tac.activeProject = pr.Id;
        tac.setActiveProject();
        
        system.assertEquals(pr.Name, tac.activeProjectRecord.projectName);
        system.assertEquals(1, tac.activeProjectRecord.workTaskLines.size());
        system.assertEquals(0, tac.activeProjectRecord.openHours);
        system.assertEquals(4, tac.activeProjectRecord.submittedHours);
    }
    
    @isTest
    static void loadPage_NoneToApprove(){
        list<Timesheet_Task__c> tasks = [SELECT Id FROM Timesheet_Task__c];
        for(Timesheet_Task__c t:tasks){
            t.Status__c = WorkTaskLine.STATUS_APPROVED;
        }
        update tasks;
        
        TimeApprovalController tac = new TimeApprovalController();
        system.assertEquals(UserInfo.getUserId(), tac.viewingAs);
        system.assertEquals(Date.today().toStartOfWeek(), tac.startDate);
        system.assertEquals(0,tac.projects.size());
    }
    
    @isTest
    static void testApproveLines(){
        list<Timesheet_Task__c> tasks = [SELECT Id FROM Timesheet_Task__c];
        for(Timesheet_Task__c t:tasks){
            t.Status__c = WorkTaskLine.STATUS_SUBMITTED;
        }
        update tasks;
        
        TimeApprovalController tac = new TimeApprovalController();
        
        // simulate clicking to view a project's hours.
        Project__c pr = [SELECT Id, Name FROM Project__c LIMIT 1];
        tac.activeProject = pr.Id;
        tac.setActiveProject();
        
        // simulate approving the task hours
        tac.activeProjectRecord.workTaskLines[0].isSelected = true;
        Id timesheetTaskId = tac.activeProjectRecord.workTaskLines[0].timesheetTaskId;
        tac.approveLines();
        
        system.assertEquals(WorkTaskLine.STATUS_APPROVED, [SELECT Status__c FROM Timesheet_Task__c WHERE Id = :timesheetTaskId].Status__c);
        system.assertEquals(0, tac.activeProjectRecord.submittedHours);
        system.assertEquals(0, tac.activeProjectRecord.workTaskLines.size());
    }
    
    @isTest
    static void testRejectLines(){
        list<Timesheet_Task__c> tasks = [SELECT Id FROM Timesheet_Task__c];
        for(Timesheet_Task__c t:tasks){
            t.Status__c = WorkTaskLine.STATUS_SUBMITTED;
        }
        update tasks;
        
        TimeApprovalController tac = new TimeApprovalController();
        
        // simulate clicking to view a project's hours.
        Project__c pr = [SELECT Id, Name FROM Project__c LIMIT 1];
        tac.activeProject = pr.Id;
        tac.setActiveProject();
        
        /// simulate approving the task hours
        tac.activeProjectRecord.workTaskLines[0].isSelected = true;
        Id timesheetTaskId = tac.activeProjectRecord.workTaskLines[0].timesheetTaskId;
        tac.rejectReason = 'Test Reason';
        tac.rejectLines();
        
        system.assertEquals(WorkTaskLine.STATUS_REJECTED, [SELECT Status__c FROM Timesheet_Task__c WHERE Id = :timesheetTaskId].Status__c);
        system.assertEquals(0, tac.activeProjectRecord.openHours);
        system.assertEquals(4, tac.activeProjectRecord.rejectedHours);
        system.assertEquals(1, tac.activeProjectRecord.rejectedWorkTaskLines.size());
    }
    
    @isTest
    static void testisApprovalManager(){
        TimeApprovalController tac1 = new TimeApprovalController();
        system.assertEquals(false, tac1.isApprovalManager);
        
        PageReference pr = Page.TimeApproval;
        pr.getParameters().put('adminView','true');
        Test.setCurrentPage(pr);
        
        TimeApprovalController tac = new TimeApprovalController();
        system.assertEquals(true, tac.isApprovalManager);
    }
    
    @isTest
    static void testGetters(){
        TimeApprovalController tac = new TimeApprovalController();
        system.assertEquals(false, tac.pms.isEmpty());
        tac.viewingAs = tac.pms[0].getValue();
        tac.changeUser();
        system.assertEquals(0,tac.projects.size());
    }
    
    @isTest
    static void testPaging(){
        TimeApprovalController tac = new TimeApprovalController();
        system.assertEquals(Date.today().toStartOfWeek(), tac.startDate);
        
        tac.nextView();
        system.assertEquals(Date.today().addDays(7).toStartOfWeek(), tac.startDate);
        
        tac.previousView();
        system.assertEquals(Date.today().toStartOfWeek(), tac.startDate);

        tac.previousView();
        system.assertEquals(Date.today().addDays(-7).toStartOfWeek(), tac.startDate);
    }
}