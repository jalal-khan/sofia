@isTest
public class OpportunityTriggerTest {

    static testMethod void opportunityWithForecasts_changeStartDate_forecastsChanged() {

        RecordType consultingServices = [SELECT Id FROM RecordType WHERE DeveloperName = 'Consulting_Services' AND SobjectType = 'Opportunity'];

        List<Work_Week__c> workWeeks = new List<Work_Week__c>();
        Date workDate = Date.today().toStartOfWeek();

        for(Integer i = 0; i < 10; i++) {
            workWeeks.add(new Work_Week__c( Start_Date__c = workDate.addDays(i * 7)) );
        }
        insert workWeeks;

        Opportunity opp = new Opportunity (
            RecordTypeId = consultingServices.Id,
            Name = 'Test Opp',
            StageName = 'Prospect/ Qualifying',
            CloseDate = Date.today(),
            Sales_Projected_Start_Date__c = workWeeks[0].Id
        );
        insert opp;

        List<Project_Role__c> roles = new List<Project_Role__c> {
            new Project_Role__c( Opportunity__c = opp.Id, Role__c = 'Developer (D)', Hour_Type__c = 'Billable', Hours_Needed__c = 100 ),
            new Project_Role__c( Opportunity__c = opp.Id, Role__c = 'Technical Architect (TA)', Hour_Type__c = 'Billable', Hours_Needed__c = 20 )
        };
        insert roles;

        List<Forecast__c> forecasts = new List<Forecast__c> {
            new Forecast__c( Project_Role__c = roles[0].Id, Work_Week__c = workWeeks[0].Id, Hours__c = 1 ),
            new Forecast__c( Project_Role__c = roles[0].Id, Work_Week__c = workWeeks[1].Id, Hours__c = 2 ),
            new Forecast__c( Project_Role__c = roles[1].Id, Work_Week__c = workWeeks[0].Id, Hours__c = 3 ),
            new Forecast__c( Project_Role__c = roles[1].Id, Work_Week__c = workWeeks[1].Id, Hours__c = 4 )
        };
        insert forecasts;

        Test.startTest();

        opp.Sales_Projected_Start_Date__c = workWeeks[4].Id;
        update opp;

        Test.stopTest();

        forecasts = [SELECT Id, Work_Week__c FROM Forecast__c ORDER BY Hours__c];

        System.assertEquals(workWeeks[4].Id, forecasts[0].Work_Week__c);
        System.assertEquals(workWeeks[5].Id, forecasts[1].Work_Week__c);
        System.assertEquals(workWeeks[4].Id, forecasts[2].Work_Week__c);
        System.assertEquals(workWeeks[5].Id, forecasts[3].Work_Week__c);

    }

    static testMethod void opportunityWithForecasts_addStartDate_forecastsChanged() {

        RecordType consultingServices = [SELECT Id FROM RecordType WHERE DeveloperName = 'Consulting_Services' AND SobjectType = 'Opportunity'];

        List<Work_Week__c> workWeeks = new List<Work_Week__c>();
        Date workDate = Date.today().toStartOfWeek();

        for(Integer i = 0; i < 10; i++) {
            workWeeks.add(new Work_Week__c( Start_Date__c = workDate.addDays(i * 7)) );
        }
        insert workWeeks;

        Opportunity opp = new Opportunity (
            RecordTypeId = consultingServices.Id,
            Name = 'Test Opp',
            StageName = 'Prospect/ Qualifying',
            CloseDate = Date.today()
        );
        insert opp;

        List<Project_Role__c> roles = new List<Project_Role__c> {
            new Project_Role__c( Opportunity__c = opp.Id, Role__c = 'Developer (D)', Hour_Type__c = 'Billable', Hours_Needed__c = 100 ),
            new Project_Role__c( Opportunity__c = opp.Id, Role__c = 'Technical Architect (TA)', Hour_Type__c = 'Billable', Hours_Needed__c = 20 )
        };
        insert roles;

        List<Forecast__c> forecasts = new List<Forecast__c> {
            new Forecast__c( Project_Role__c = roles[0].Id, Work_Week__c = workWeeks[0].Id, Hours__c = 1 ),
            new Forecast__c( Project_Role__c = roles[0].Id, Work_Week__c = workWeeks[1].Id, Hours__c = 2 ),
            new Forecast__c( Project_Role__c = roles[1].Id, Work_Week__c = workWeeks[0].Id, Hours__c = 3 ),
            new Forecast__c( Project_Role__c = roles[1].Id, Work_Week__c = workWeeks[1].Id, Hours__c = 4 )
        };
        insert forecasts;

        Test.startTest();

        opp.Sales_Projected_Start_Date__c = workWeeks[4].Id;
        update opp;

        Test.stopTest();

        forecasts = [SELECT Id, Work_Week__c FROM Forecast__c ORDER BY Hours__c];

        System.assertEquals(workWeeks[4].Id, forecasts[0].Work_Week__c);
        System.assertEquals(workWeeks[5].Id, forecasts[1].Work_Week__c);
        System.assertEquals(workWeeks[4].Id, forecasts[2].Work_Week__c);
        System.assertEquals(workWeeks[5].Id, forecasts[3].Work_Week__c);

    }

}