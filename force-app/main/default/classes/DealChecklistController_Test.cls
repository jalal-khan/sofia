@isTest
private class DealChecklistController_Test {

	private static final string TEMPLATE_NAME = 'Template 1';

	@testSetup
	private static void setup() {
		list<Checklist_Template__c> templates = CKLST_TestObjectFactory.getChecklistTemplates(3);
		templates[0].name = TEMPLATE_NAME;
		insert templates;

		list<Checklist_Template_Entry__c> entries = CKLST_TestObjectFactory.getChecklistTemplateEntries(templates, 3);
		insert entries;
	}

	@isTest
	private static void testNewChecklist() {
		Opportunity deal = CKLST_TestObjectFactory.getDealWithDates();
		insert deal;

		Checklist_Template__c template = [select id, name from Checklist_Template__c where name = :TEMPLATE_NAME];

		Checklist__c cklist = new Checklist__c(Opportunity__c = deal.id);
		ApexPages.StandardController sctrl = new ApexPages.StandardController(cklist);
		DealChecklistController ctrl = new DealChecklistController(sctrl);
		System.assertEquals(null, ctrl.templateId);
		System.assertEquals(true, ctrl.okToCreate);
		System.assertEquals(false, ctrl.templateOk);
		System.assertEquals('New Checklist', ctrl.subtitle);
		System.assertEquals(4, ctrl.templateOptions.size());  // 3 templates created in Setup, plus '--select--'
		ctrl.templateId = template.id;
		ctrl.createChecklist();
		System.assertEquals(true, ctrl.templateOk);
		System.assertEquals(TEMPLATE_NAME, ctrl.checklist.name);
		System.assertEquals(1, ctrl.sections.size());
		System.assertEquals(3, ctrl.sections[0].items.size());
	}

	@isTest
	private static void testSaveNewChecklist() {
		Opportunity deal = CKLST_TestObjectFactory.getDealWithDates();
		insert deal;

		Checklist_Template__c template = [select id, name from Checklist_Template__c where name = :TEMPLATE_NAME];

		Checklist__c cklist = new Checklist__c(Opportunity__c = deal.id);
		ApexPages.StandardController sctrl = new ApexPages.StandardController(cklist);
		DealChecklistController ctrl = new DealChecklistController(sctrl);
		System.assertNotEquals(0, ctrl.templateOptions.size());
		ctrl.templateId = template.id;
		ctrl.createChecklist();
		PageReference saveRes = ctrl.saveChecklist();
		System.assertNotEquals(null, saveRes);

		list<Checklist__c> res = [
				select id, name, (select id, name from Checklist_Entries__r where template__c != null)
				  from Checklist__c
				 where Opportunity__c = :deal.id
		];
		System.assertEquals(1, res.size());
		System.assertEquals(TEMPLATE_NAME, res[0].name);
		System.assertEquals(3, res[0].Checklist_Entries__r.size());

	}

	@isTest
	private static void testLoadExistingChecklist() {
		Opportunity deal = CKLST_TestObjectFactory.getDealWithDates();
		insert deal;

		Checklist_Template__c template = [select id, name from Checklist_Template__c where name = :TEMPLATE_NAME];

		Checklist__c cklist = new Checklist__c(Opportunity__c = deal.id);
		ApexPages.StandardController sctrl = new ApexPages.StandardController(cklist);
		DealChecklistController ctrl = new DealChecklistController(sctrl);
		System.assertNotEquals(0, ctrl.templateOptions.size());
		ctrl.templateId = template.id;
		ctrl.createChecklist();
		PageReference saveRes = ctrl.saveChecklist();

		list<Checklist__c> res = [
				select id, name, Opportunity__c, Template__c
				  from Checklist__c
				 where Opportunity__c = :deal.id
		];

		Checklist__c existing = res[0];
		sctrl = new ApexPages.StandardController(existing);
		ctrl = new DealChecklistController(sctrl);
		System.assertEquals(true, ctrl.templateOk);
		System.assertEquals(TEMPLATE_NAME, ctrl.subtitle);
		System.assertEquals(1, ctrl.sections.size());
		System.assertEquals(3, ctrl.sections[0].items.size());

	}

	@isTest
	private static void testResetDates() {
		Opportunity deal = CKLST_TestObjectFactory.getDealWithDates();
		insert deal;

		Checklist_Template__c template = [select id, name from Checklist_Template__c where name = :TEMPLATE_NAME];

		Checklist__c cklist = new Checklist__c(Opportunity__c = deal.id);
		ApexPages.StandardController sctrl = new ApexPages.StandardController(cklist);
		DealChecklistController ctrl = new DealChecklistController(sctrl);
		ctrl.templateId = template.id;
		ctrl.createChecklist();
		DealChecklistController.ChecklistSection sect = ctrl.sections[0];
		list<Date> dates = new list<Date>();
		for (integer i=0; i<sect.items.size(); i++) {
			DealChecklistController.ChecklistItem item = sect.items[i];
			dates.add(item.obj.Due_Date__c);																		//track old value
			item.obj.Due_Date__c = item.obj.Due_Date__c.addDays(1);							//change value
			System.assertNotEquals(dates[i], sect.items[i].obj.Due_Date__c);		//assert new != old
		}
		ctrl.actionSectionIndex = 0;  // test reset section link
		ctrl.resetDueDates();
		for (integer i=0; i<sect.items.size(); i++) {
			DealChecklistController.ChecklistItem item = sect.items[i];
			System.assertEquals(dates[i], sect.items[i].obj.Due_Date__c);				//assert item val == old
			item.obj.Due_Date__c = item.obj.Due_Date__c.addDays(1);							//change value for next test
			System.assertNotEquals(dates[i], sect.items[i].obj.Due_Date__c);		//assert new != old
		}

		ctrl.actionSectionIndex = -1;  // test reset all link
		ctrl.resetDueDates();
		for (integer i=0; i<sect.items.size(); i++) {
			DealChecklistController.ChecklistItem item = sect.items[i];
			System.assertEquals(dates[i], sect.items[i].obj.Due_Date__c);				//assert item val == old
		}

	}

	@isTest
	private static void testGetDealContactRoles() {
		Opportunity deal = CKLST_TestObjectFactory.getDealWithDates();
		insert deal;

		Checklist_Template__c template = [select id, name from Checklist_Template__c where name = :TEMPLATE_NAME];

		Checklist__c cklist = new Checklist__c(Opportunity__c = deal.id);
		ApexPages.StandardController sctrl = new ApexPages.StandardController(cklist);
		DealChecklistController ctrl = new DealChecklistController(sctrl);

		list<SelectOption> dealContactRoles = ctrl.dealContactRoles;
		System.assertEquals(true, dealContactRoles.size() > 0);
		System.assertEquals('--Select--', dealContactRoles[0].getLabel());

	}
}