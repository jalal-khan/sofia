@IsTest
private class TimesheetMaintenanceBatchTest {
    
    @TestSetup
    static void makeData(){
        
    }

    @IsTest
    static void scheduleBatch() {
        
        Test.startTest();

        String jobId = System.schedule('testTimesheetMaintenance', TimesheetMaintenanceBatch.CRON_EXP, new TimesheetMaintenanceBatch());

        Test.stopTest();
    }

    @IsTest
    static void batch() {
        Test.startTest();

        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Employee').getRecordTypeId();
        c.FO_Employment_Status__c = 'Active';
        c.Start_Date__c = Date.today();
        c.User_Link__c = UserInfo.getUserId();
        insert c;

        delete [SELECT Id FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id];

        Database.executeBatch(new TimesheetMaintenanceBatch());

        Test.stopTest();

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];

        System.assertEquals(52, timesheets.size());
        System.assertEquals(Date.today().toStartOfWeek(), timesheets[0].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek().addDays(51*7), timesheets[51].Week_Start_Date__c);
    }

}