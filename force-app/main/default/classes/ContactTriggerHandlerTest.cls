@IsTest(IsParallel = true)
private class ContactTriggerHandlerTest {
    
    @TestSetup
    static void makeData(){

    }

    @IsTest
    static void insertActiveEmployee() {
        Test.startTest();
		
        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Employee').getRecordTypeId();
        c.FO_Employment_Status__c = 'Active';
        c.Start_Date__c = Date.today();
        c.User_Link__c = UserInfo.getUserId();
        insert c;

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];

        Test.stopTest();

        System.assertEquals(52, timesheets.size());
        System.assertEquals(Date.today().toStartOfWeek(), timesheets[0].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek().addDays(51*7), timesheets[51].Week_Start_Date__c);
    }

    @IsTest
    static void updateActiveEmployee() {
        Test.startTest();
		
        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Employee').getRecordTypeId();
        c.User_Link__c = UserInfo.getUserId();
        insert c;

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];

        System.assertEquals(0, timesheets.size());

        c.FO_Employment_Status__c = 'Active';
        c.Start_Date__c = Date.today();
        update c;

        timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];

        Test.stopTest();

        System.assertEquals(52, timesheets.size());
        System.assertEquals(Date.today().toStartOfWeek(), timesheets[0].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek().addDays(51*7), timesheets[51].Week_Start_Date__c);
    }

    @IsTest
    static void updateFormerEmployee() {
        Test.startTest();
		
        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Employee').getRecordTypeId();
        c.FO_Employment_Status__c = 'Active';
        c.Start_Date__c = Date.today();
        c.User_Link__c = UserInfo.getUserId();
        insert c;        

        c.FO_Employment_Status__c = 'Former';
        update c;

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];

        Test.stopTest();

        System.assertEquals(1, timesheets.size());
    }
}