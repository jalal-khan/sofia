@RestResource(urlMapping='/ClientStatus/')
global class FMZ_RestStatusCheck {

    @HttpPost
    global static string phoneHome(Id clientId){
        Id clientOrg = clientId;
        
        // query for the client_link__c record w/org Id
        list<Client_Link__c> clinks = [SELECT Id, Client_Org_Id__c, Subscription_End__c, Active_Monitoring_Enabled__c, Active_Monitoring_Restarts__c FROM Client_Link__c WHERE Client_Org_Id__c = :clientOrg];
        string res = '{';
        
        if(!clinks.isEmpty()){
            res += '"status" : "active"';
        }else{
            res += '"status" : "inactive"';
        }
        res += '}';
        
        return res;
    }
}