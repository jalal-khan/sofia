@isTest
private class TestCheckRAHours_Trigger {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
         // TO DO: implement unit test
        
        Account account = TestDataFactory_AC.createAccounts(1)[0];
    insert account;
    
    Contact contct = TestDataFactory_AC.createContacts(1)[0];
    contct.AccountId = account.id;
    contct.FirstName = 'Test';
    insert contct;
    
    Opportunity opp = TestDataFactory_AC.createOpportunities(1)[0];
    opp.AccountId = account.id;
    opp.SFDC_Account_Executive__c = contct.id;
    
    insert opp;
    
    /* Commented on 14-08-14 to remove milestone Package 
    Milestone1_Project__c Project = TestDataFactory_AC.createProjects(1)[0];
    Project.Opportunity__c = opp.id;
    
    insert Project;
    */
    
    /* Testing Single RA Record */
    
    Projects_ForceOptimized__c RA = TestDataFactory_AC.createRAs(1)[0];
    RA.Project_Coordinator__c = true;
    RA.Opportunity__c = opp.id;
    RA.Budgeted_Hours__c = 10;
    insert RA;
    
    /* Testing Task Log Record */
    
    Task_Log__c t = new Task_Log__c();
    t.Resource_Assignment__c = RA.id;
    t.Hours_Completed__c = 5;
    t.Task_Type__c = 'Custom Programming';
    t.Description__c = 'Test';
    t.Type__c = 'Billable';
    t.Date__c = System.today();
    
    insert t;
   
    }
}