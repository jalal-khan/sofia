public with sharing class ForecastViewerController {
    
    @AuraEnabled(cacheable=true)
    public static List<FMZ_Resource_Assignment__c> getResourceAssignments(Date weekStartDate) {
        /*
        return [SELECT Id, Name, FMZ_Project_Role__r.Role__c, FMZ_Project_Role__r.FMZ_Project__r.Name,
            (SELECT Id, Forecast_Hours__c FROM FMZ_Resource_Forecasts__r WHERE Week_Start_Date__c = :weekStartDate),
            (SELECT Id, Hours_Worked__c FROM FMZ_Timesheet_Details__r WHERE Week_Start_Date__c = :weekStartDate)
            FROM FMZ_Resource_Assignment__c
            WHERE FMZ_Resource__r.User_Link__c = :UserInfo.getUserId()
            AND Assignment_Start_Date__c < :weekStartDate.toStartOfWeek().addDays(7) AND Assignment_End_Date__c >= :weekStartDate];
    	*/
        return [SELECT Id, Name, FMZ_Project_Role__r.FMZ_Role__c, FMZ_Project_Role__r.FMZ_Project__r.Name,
            (SELECT Id, Forecast_Hours__c FROM FMZ_Resource_Forecasts__r WHERE Week_Start_Date__c = :weekStartDate),
            (SELECT Id, Hours_Worked__c FROM FMZ_Timesheet_Details__r WHERE Week_Start_Date__c = :weekStartDate)
            FROM FMZ_Resource_Assignment__c
            WHERE FMZ_Project_Role__r.FMZ_Project__r.Display_for_Resource_Management__c = true
                AND FMZ_Resource__r.User_Link__c = :UserInfo.getUserId() 
                AND Status__c = 'Confirmed'
            	AND Assignment_Start_Date__c < :weekStartDate.toStartOfWeek().addDays(7) 
                AND Assignment_End_Date__c >= :weekStartDate];
	}

    @AuraEnabled
    public static void submitTimesheetDetails(Date weekStartDate) {
        List<FMZ_Timesheet_Detail__c> details = [
            SELECT Id, Status__c
            FROM FMZ_Timesheet_Detail__c
            WHERE Status__c = 'Open' AND Week_Start_Date__c = :weekStartDate 
            AND FMZ_Resource_Assignment__r.FMZ_Resource__r.User_Link__c = :UserInfo.getUserId()];
        
        for (FMZ_Timesheet_Detail__c detail : details)
            detail.Status__c = 'Submitted';

        update details;
    }
}