// All fieldset tests will need to be updated to refer to an object that has a fieldset; queries and field lists will need to be updated as well.
@isTest
private class FMZ_SchemaUtils_Test {

	@isTest
	static void getObjectDescribeByName() {
		Schema.DescribeSObjectResult objDesc = FMZ_SchemaUtils.getObjectDescribe('Account');
		system.assertEquals(Account.getSObjectType().getDescribe().getName(), objDesc.getName());
	}

	@isTest
	static void getObjectDescribeByType() {
		Schema.DescribeSObjectResult objDesc = FMZ_SchemaUtils.getObjectDescribe(Account.getSObjectType());
		system.assertEquals(Account.getSObjectType().getDescribe().getName(), objDesc.getName());
	}

	@isTest
	static void getSObjectTypeByName() {
		system.assertEquals(Account.getSObjectType(), FMZ_SchemaUtils.getSObjectType('Account'));
	}

	@isTest
	static void getObjectFields() {
		map<string, Schema.DescribeFieldResult> fieldMap = new map<string, Schema.DescribeFieldResult>();
		for (Schema.sObjectField fldToken : Account.getSObjectType().getDescribe().fields.getMap().values()) {
			Schema.DescribeFieldResult dfr = fldToken.getDescribe();
			fieldMap.put(dfr.getName().toLowerCase(), dfr);
		}
		system.assertEquals(fieldMap, FMZ_SchemaUtils.getObjectFieldDescribes(Account.getSObjectType()));
	}

	@isTest // Test the simple case, of a field directly on the object
	static void getSObjectFieldSimple() {
		Account acct = CKLST_TestObjectFactory.getAccount();
		system.assertEquals(acct.name, FMZ_SchemaUtils.getSObjectField(acct, 'name'));
	}

	@isTest // Test the complex case, of a field on a related object.
	static void getSObjectFieldRelationship() {
		Account acct = CKLST_TestObjectFactory.getAccount();
		insert acct;
		Contact ct = CKLST_TestObjectFactory.getContact(acct);
		insert ct;
		Contact res = [select id, name, Account.name from Contact where id = :ct.id];

		system.assertEquals(acct.name, FMZ_SchemaUtils.getSObjectField(res, 'Account.name'));
	}

	@isTest
	static void getPickListValues() {
		list<string> res = FMZ_SchemaUtils.getPickListValues(Checklist_Entry__c.sObjectType, FMZ_SchemaUtils.currentNamespacePrefix+'Status__c');
		set<string> resSet = new set<string>(res);
		set<string> expected = new set<string>{'Needed', 'Not Needed', 'Complete'};
		System.assertEquals(expected, resSet);
	}

	@isTest
	static void getPickListValueMap() {
		map<string, string> res = FMZ_SchemaUtils.getPickListValueMap(Checklist_Entry__c.sObjectType, FMZ_SchemaUtils.currentNamespacePrefix+'Status__c');
		map<string, string> expected = new map<string, string>{'Needed' => 'Needed', 'Not Needed' => 'Not Needed', 'Complete' => 'Complete'};
		System.assertEquals(expected, res);
	}
}