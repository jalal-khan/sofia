public class InvoicingExt {
    public Client_Invoice__c invoice{get;set;}
    public decimal pendingInvoiced{get;set;}
    public boolean canEdit{
        get{
            return this.invoice.Payment_Status__c == 'Projected' || this.invoice.Payment_Status__c == 'Ready to Send';
        }set;
    }
    public integer totalTimesheetsInvoiced{
        get{
            return this.invoicedLines.size();
        }set;
    }
    public integer totalExpensesInvoiced{
        get{
            return this.invoicedExpenseLines.size();
        }set;
    }
    
    public InvoicingExt(ApexPages.StandardController sc){
        
        if(!test.isRunningTest()){
            sc.addFields(addFields);
        }
        this.invoice = (Client_Invoice__c)sc.getRecord();
        
            doSetup();
    }
    public static final string INVOICE_TYPE_PROJECT = 'Project';
    public static final string INVOICE_TYPE_EXPENSES = 'Travel/Expense';
    
    public void doSetup(){
        // get the un-invoiced timesheet tasks.
            string approvedStatus = WorkTaskLine.STATUS_APPROVED;
            string invoicedStatus = WorkTaskLine.STATUS_INVOICED;
            string pId = this.invoice.Project__c;
            Id invoiceId = this.invoice.Id;
            this.openLines = new list<WorkTaskLine>();
            this.openExpenseLines = new list<ExpenseReport>();
            this.invoicedLines = new list<WorkTaskLine>();
            this.invoicedExpenseLines = new list<ExpenseReport>();
        if(this.invoice.Invoice_Type__c == INVOICE_TYPE_PROJECT || this.invoice.Invoice_Type__c == INVOICE_TYPE_EXPENSES){
            
            
            
            string qry = 'SELECT Id, Timesheet__c, Work_Task__c, Work_Task__r.Remaining_Hours__c, Work_Task__r.Completed_Hours__c, Status__c, Billable_Amount__c,';
            qry += 'Client_Invoice__c, Hours_Used__c, Rejected_Hours_Comment__c , Work_Task__r.Allocated_Hours__c, Work_Task__r.Project_Name__c,Work_Task__r.Assigned_to__r.FirstName,';
            qry += 'Work_Task__r.Hour_Type__c, Work_Task__r.Assigned_to__r.LastName, Work_Task__r.Reference_Name__c,Work_Task__r.Task_Type__c, ';
            qry += 'Work_Task__r.Work__r.Fortimize_Project__c, Timesheet__r.Start_Date__c ';
            qry += 'FROM Timesheet_Task__c ';
            qry += 'WHERE Client_Invoice__c = :invoiceId OR (';
            qry += getWhereClause(this.invoice);
            qry += ' AND Status__c = :approvedStatus';
            qry += ' AND Work_Task__r.Work__r.Fortimize_Project__c = :pId';
            qry += ' AND Client_Invoice__c = null)';
            qry += ' ORDER BY Timesheet__r.Start_Date__c, Timesheet__r.Owner.FirstName';
            for(Timesheet_Task__c tt : Database.query(qry)){
                if(tt.Client_Invoice__c == invoiceId){
                    invoicedLines.add(new WorkTaskLine(tt));
                }else{
                    openLines.add(new WorkTaskLine(tt));
                }
            }
            system.debug('this.invoice: ' + this.invoice);
            // if it's an expense invoice, then get all expense reports.
            if(this.invoice.Invoice_Type__c == 'Travel/Expense'){
                system.debug('Travel/Expense');
                system.debug('pid: ' + pId);
                
                for(Expense_Report__c er:[SELECT Id, Owner.Name, Expense_Report_Total__c, Client_Invoice__c, Pay_Period_Ending__c   
                                          FROM Expense_Report__c 
                                          WHERE ((Fortimize_Project__c = :pId AND Client_Invoice__c = null AND Status__c = 'Approved')
                                                 OR (Client_Invoice__c = :this.invoice.Id))
                                          AND Customer_Billable__c = 'Yes'
                                         ]){
                                             if(er.Client_Invoice__c != null){
                                                 invoicedExpenseLines.add(new ExpenseReport(er));
                                             }else{
                                                 openExpenseLines.add(new ExpenseReport(er));
                                             }
                                             
                                         }
                system.debug('open expenses: ' + openExpenseLines);
                system.debug('invoicedExpenseLines: ' + invoicedExpenseLines);
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This interface only supports Project and Expense invoice types.'));
        }
    }
    public string getWhereClause(Client_Invoice__c inv){
        if(inv.Invoice_Type__c == 'Project'){
            return ' Work_Task__r.Work__r.Hour_Type__c = \'Billable\'';
        }else if(inv.Invoice_Type__c == 'Travel/Expense'){
            return ' Work_Task__r.Work__r.Hour_Type__c = \'Travel - Billable\'';
        }else{return null;}
    }
    public decimal openAmount{
        get{
            openAmount = 0;
            for(WorkTaskLine wtl:openLines){
                openAmount+=wtl.billableAmount;
            }
            for(ExpenseReport er:openExpenseLines){
                openAmount += er.expenseReport.Expense_Report_Total__c;
            }
            return openAmount;
        }set;}
    public decimal invoicedAmount{get;set;}
    
    public list<WorkTaskLine> openLines{get;set;}
    public list<WorkTaskLine> invoicedLines{get;set;}
    public list<ExpenseReport> openExpenseLines{get;set;}
    public list<ExpenseReport> invoicedExpenseLines{get;set;}
    
    public void updateInvoice(){
        try{
            decimal amtDue = 0;
            
            list<Timesheet_Task__c> toAdd = new list<Timesheet_Task__c>();
            for(WorkTaskLine wtl:openLines){
                if(wtl.isSelected){
                    toAdd.add(new Timesheet_Task__c(
                        Id = wtl.timesheetTaskId,
                        Client_Invoice__c  = this.invoice.id,
                        Status__c = WorkTaskLine.STATUS_INVOICED
                    ));
                    this.invoicedLines.add(wtl);
                }
            }
            if(!toAdd.isEmpty()){
                update toAdd;
            }
            
            list<Expense_Report__c> ersToAdd = new list<Expense_Report__c>();
            for(ExpenseReport er:openExpenseLines){
                if(er.isSelected){
                    er.expenseReport.Client_Invoice__c = this.invoice.Id;
                    ersToAdd.add(er.expenseReport);
                }
            }
            if(!ersToAdd.isEmpty()){
                update ersToAdd;
            }
            refreshInvoiceData();
            doSetup();
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }  
    }
    public static final string STATUS_READY_TO_SEND = 'Ready to Send';
    
    public Id removeExpenseId{get;set;}
    public void removeExpenseLine(){
        system.debug('removeExpenseId : ' + removeExpenseId);
        try{
            if(removeExpenseId != null){
                for(integer i = 0;i < invoicedExpenseLines.size(); i++){
                    if(invoicedExpenseLines[i].expenseReport.Id == removeExpenseId){
                        Expense_Report__c er = new Expense_Report__c(
                            Id = invoicedExpenseLines[i].expenseReport.id,
                            Status__c = 'Approved',
                            Client_Invoice__c = null
                        );
                        update er;
                        invoicedExpenseLines.remove(i);
                        break;
                    }
                }
                
                refreshInvoiceData();
                doSetup();
            }
        }catch(Exception e){
            system.debug('removeExpenseId : ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }  
    }
    public Id removeTaskId{get;set;}
    public void removeTaskLine(){
        try{
            if(removeTaskId != null){
                for(integer i = 0;i < invoicedLines.size(); i++){
                    if(invoicedLines[i].timesheetTaskId == removeTaskId){
                        Timesheet_Task__c tt = new Timesheet_Task__c(
                            Id = invoicedLines[i].timesheetTaskId,
                            Status__c = WorkTaskLine.STATUS_APPROVED,
                            Client_Invoice__c = null
                        );
                        update tt;
                        invoicedLines.remove(i);
                        break;
                    }
                }
                
                refreshInvoiceData();
                doSetup();
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }  
    }
    
    public void updateInvoiceOnly(){
        try{
            update this.invoice;
            refreshInvoiceData();
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
    }
    
    public void refreshInvoiceData(){
        Id invoiceId = this.invoice.Id;
        string fields = string.join(addFields,', ');
        this.invoice = (Client_Invoice__c)Database.query('SELECT ' + fields + ' FROM Client_Invoice__c WHERE Id = :invoiceId')[0];
        system.debug('this.invoice after refresh: ' + this.invoice);
    }
    
    public class ExpenseReport{
        public Expense_Report__c expenseReport{get;set;}
        public boolean isSelected{get;set;}
        public ExpenseReport(Expense_Report__c er){
            this.expenseReport = er;
            this.isSelected = false;
        }
    }
    
    public list<string> addFields = new list<String>{'Project__c','Project__r.Name','Project__r.Amount_of_Deposit_Remaining__c',
        'Project__r.Account__r.Name','Invoice_Type__c','Amount_Due__c','Total_Expenses_Due__c','Total_Billable_Hours_Due__c',
        'Payment_Status__c','Deposit_Amount_Applied__c','Gross_Time_Expenses__c'};
}