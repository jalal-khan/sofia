@isTest
public class TestWeeklySurveyEmail {

    @isTest
    public static void testSchedulable(){
        Account a = new Account(Name = 'Test');
        INSERT a;
        
        Project__c p = new Project__c();
        p.Account__c = a.Id;
        p.Project_Manager__c = UserInfo.getUserId();
        p.Name = 'Test Project';
        INSERT p;
        
        Contact c = new Contact(LastName = 'Testerson', Email = 'test@test.com');
        INSERT c;
        
        Project_Contact_Role__c pcr = new Project_Contact_Role__c();
        pcr.Contact__c = c.id;
        pcr.Project__c = p.id;
        pcr.Role__c = 'Survey Recipient';
        INSERT pcr;
        
        Test.startTest();
        WeeklySurveyEmailSchedulable sh1 = new WeeklySurveyEmailSchedulable();      
        String sch = '0  00 1 3 * ?';
        system.schedule('Test', sch, sh1);
        Test.stopTest();
    }
}