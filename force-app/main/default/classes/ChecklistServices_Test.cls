@isTest
private class ChecklistServices_Test {

	private static final string TASK_USER_PROFILE = 'Standard User';
	private static list<string> dealContactRoles = FMZ_SchemaUtils.GetDependentOptions(
		Internal_Role__c.sObjectType.getDescribe().getName(),
		Internal_Role__c.Object__c.getDescribe().getName(),
		Internal_Role__c.Role__c.getDescribe().getName()).get('Opportunity');

	@testSetup
	private static void setup() {
		Opportunity deal = CKLST_TestObjectFactory.getDealWithDates();
		insert deal;

		list<User> users = CKLST_TestObjectFactory.getUsersForProfile(TASK_USER_PROFILE, dealContactRoles.size());
		insert users;

		user nonRoleUser = CKLST_TestObjectFactory.getUserForProfile(TASK_USER_PROFILE);
		nonRoleUser.lastname = 'NonRoleUser';
		insert nonRoleUser;

		list<Internal_Role__c> dcrList = CKLST_TestObjectFactory.getInternalDealContactRoles(deal, users);
		insert dcrList;

		Checklist_Template__c template = CKLST_TestObjectFactory.getChecklistTemplate();
		insert template;

		list<Checklist_Template_Entry__c> entries = CKLST_TestObjectFactory.getChecklistTemplateEntries(template, dealContactRoles.size());
		insert entries;

	}

	@isTest
	private static void validateTestSetup() {
		System.assertEquals(true, dealContactRoles.size() > 1, 'At least two internal deal contact roles must be defined');

		list<Opportunity> deals = [select id, name from Opportunity];
		System.assertEquals(1, deals.size(), 'A single deal should exist');

		list<Checklist_Template__c> templates = [select id, name from Checklist_Template__c];
		System.assertEquals(1, templates.size(), 'A single checklist template should exist');

		list<User> users = [select id from User where alias = 'testuser'];
		System.assertEquals(dealContactRoles.size()+1, users.size(), 'Should have as many test users as there exist Internal Deal Contact Roles, plus 1 Non-Role User');

		list<Checklist_Template_Entry__c> entries = [
			select id, name, Default_Deal_Team_Role__c
				from Checklist_Template_Entry__c
			 where Checklist_Template__c = :templates[0].id
		];
		System.assertEquals(dealContactRoles.size(), entries.size(), 'Checklist Template should have as many entries as there exist Internal Deal Contact Roles');

		set<string> seen = new set<string>();
		for (Checklist_Template_Entry__c entry: entries) {
			System.assertNotEquals(true, seen.contains(entry.Default_Deal_Team_Role__c), 'Checklist Template should have exactly one entry per Internal Deal Contact Role');
			seen.add(entry.Default_Deal_Team_Role__c);
		}

	}

	@isTest
	private static void ChecklistEntryTrigger_insert() {
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		insert entries;

		list<Checklist_Entry__c> resEntries = [select id, Internal_Role__c, Due_Date__c, Task_Id__c from Checklist_Entry__c where id in :entries];
		System.assertEquals(entries.size(), resEntries.size()); // sanity check - found all the entries we inserted
		set<string> taskIds = new set<String>();
		for (Checklist_Entry__c entry : resEntries) {
			System.assertNotEquals(null, entry.task_id__c, 'All checklist entries should have tasks');
			taskIds.add(entry.Task_Id__c);
		}
		map<id, Task> resTasks = new map<id, Task>([select id, ownerId, activityDate, IsClosed from Task where id in :taskIds]);
		System.assertEquals(resTasks.values().size(), resEntries.size(), 'Should have a task for each Checklist Entry');

		map<string, id> dcrMap = getInternalDealContactRoleMap(deal);

		for (Checklist_Entry__c entry : resEntries) {
			Task t = resTasks.get(entry.task_id__c);
			System.assertEquals(dcrMap.get(entry.Internal_Role__c), t.ownerId, 'Task should be assigned to Internal Deal Contact Role user');
			System.assertEquals(entry.Due_Date__c, t.activityDate);
			System.assertEquals(false, t.isClosed);
		}
	}

	@isTest
	private static void ChecklistEntryTrigger_insertSpecificUser() {
		User nonRoleUser = [select id from user where lastname = 'NonRoleUser'];
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		for (Checklist_Entry__c entry : entries) {
			entry.Internal_Role__c = 'User';
			entry.Specific_User__c = nonRoleUser.id;
		}
		insert entries;

		list<Checklist_Entry__c> resEntries = [select id, Internal_Role__c, Due_Date__c, Task_Id__c from Checklist_Entry__c where id in :entries];
		System.assertEquals(entries.size(), resEntries.size()); // sanity check - found all the entries we inserted
		set<string> taskIds = new set<String>();
		for (Checklist_Entry__c entry : resEntries) {
			System.assertNotEquals(null, entry.task_id__c, 'All checklist entries should have tasks');
			taskIds.add(entry.Task_Id__c);
		}
		map<id, Task> resTasks = new map<id, Task>([select id, ownerId, activityDate, IsClosed from Task where id in :taskIds]);
		System.assertEquals(resTasks.values().size(), resEntries.size(), 'Should have a task for each Checklist Entry');

		map<string, id> dcrMap = getInternalDealContactRoleMap(deal);

		for (Checklist_Entry__c entry : resEntries) {
			Task t = resTasks.get(entry.task_id__c);
			System.assertEquals(nonRoleUser.id, t.ownerId, 'Task should be assigned to non-Role user');
			System.assertEquals(entry.Due_Date__c, t.activityDate);
			System.assertEquals(false, t.isClosed);
		}
	}


	@isTest
	private static void ChecklistEntryTrigger_update_A() {
		//   * if status changes from Needed to Not Needed and the CLE has a taskId, remove the task
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		insert entries;

		System.assertEquals(entries.size(), [select id from Task].size(), 'PRECONDITION: all tasks exist');

		for(Checklist_Entry__c entry : entries) {
			entry.Status__c = ChecklistServices.STATUS_NOT_NEEDED;
		}
		update entries;
		System.assertEquals(0, [select id from Task].size(), 'When status changes Needed->Not Needed, task should be removed');

	}

	@isTest
	private static void ChecklistEntryTrigger_update_B() {
		//   * if Status changes from Not Needed to Needed and the Deal has the required Role, create a task
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		for(Checklist_Entry__c entry : entries) {
			entry.Status__c = ChecklistServices.STATUS_NOT_NEEDED;
		}
		insert entries;

		System.assertEquals(0, [select id from Task].size(), 'PRECONDITION: no tasks exist');

		for(Checklist_Entry__c entry : entries) {
			entry.Status__c = ChecklistServices.STATUS_NEEDED;
		}
		update entries;
		System.assertEquals(entries.size(), [select id from Task].size(), 'When status changes Not Needed->Needed, task should be created');
	}

	@isTest
	private static void ChecklistEntryTrigger_update_C() {
		//   * if status is Needed and Role changes, reassign task or create if no prior task
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		insert entries[0];

		Checklist_Entry__c testEntry = [select id, Internal_Role__c, Due_Date__c, Task_id__c from Checklist_Entry__c where id = :entries[0].id];
		System.assertEquals(1, [select id from Task where id = :testEntry.Task_id__c].size(), 'PRECONDITION: one task exists');

		map<string, id> dcrMap = getInternalDealContactRoleMap(deal);
		list<string> roleNames = new list<string>(dcrMap.keySet());
		string newRole = (roleNames[0] == testEntry.Internal_Role__c ? roleNames[1] : roleNames[0]); //change the role
		System.assertNotEquals(newRole, testEntry.Internal_Role__c);
		testEntry.Internal_Role__c = newRole;
		test.startTest();
		update testEntry;
		test.stopTest();
		Task resTask = [select id, ownerId from Task]; // where id = :testEntry.Task_id__c];
		System.assertEquals(dcrMap.get(newRole), resTask.ownerId);
	}

	@isTest
	private static void ChecklistEntryTrigger_update_D() {
		//   * if status is Needed and Date changes, update task
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		insert entries[0];

		Checklist_Entry__c testEntry = [select id, Internal_Role__c, Due_Date__c, Task_id__c from Checklist_Entry__c where id = :entries[0].id];
		System.assertEquals(1, [select id from Task where id = :testEntry.Task_id__c].size(), 'PRECONDITION: one task exists');

		Date newDate = testEntry.Due_Date__c.addDays(7);
		testEntry.Due_Date__c = newDate;
		test.startTest();
		update testEntry;
		test.stopTest();
		Task resTask = [select id, ActivityDate from Task where id = :testEntry.Task_id__c];
		System.assertEquals(newDate, resTask.ActivityDate);

	}

	@isTest
	private static void ChecklistEntryTrigger_update_NonRoleUser() {
		//   * if status is Needed and Role changes to non-role user, reassign task or create if no prior task
		User nonRoleUser = [select id from user where lastname = 'NonRoleUser'];
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		insert entries[0];

		Checklist_Entry__c testEntry = [select id, Internal_Role__c, Due_Date__c, Task_id__c from Checklist_Entry__c where id = :entries[0].id];
		System.assertEquals(1, [select id from Task where id = :testEntry.Task_id__c].size(), 'PRECONDITION: one task exists');

		testEntry.Internal_Role__c = 'User';
		testEntry.Specific_User__c = nonRoleUser.id;
		test.startTest();
		update testEntry;
		test.stopTest();
		Task resTask = [select id, ownerId from Task]; // where id = :testEntry.Task_id__c];
		System.assertEquals(nonRoleUser.id, resTask.ownerId);
	}

	@isTest
	private static void ChecklistEntryTrigger_Delete() {
		//   * if status is Needed and Date changes, update task
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		insert entries;

		System.assertEquals(entries.size(), [select id from Task].size(), 'PRECONDITION: all tasks exist');

		test.startTest();
		delete entries;
		test.stopTest();

		System.assertEquals(0, [select id from Task].size(), 'Tasks should be deleted when Checklist Entry is deleted');

	}

	@isTest
	private static void ChecklistTrigger_Delete() {
		//   * if status is Needed and Date changes, update task
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		insert entries;

		System.assertEquals(entries.size(), [select id from Task].size(), 'PRECONDITION: all tasks exist');

		test.startTest();
		delete checklist;
		test.stopTest();

		System.assertEquals(0, [select id from Task].size(), 'Tasks should be deleted when Checklist is deleted');

	}

	@isTest
	private static void InternalDealContactRoleTrigger_Insert() {
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		Internal_Role__c dcr = [select id, Object__c, Opportunity__c, role__c, Internal_Team_Member__c from Internal_Role__c where Opportunity__c = :deal.id and role__c = :entries[0].Internal_Role__c];
		delete dcr;
		insert entries[0];

		System.assertEquals(0, [select id from Task].size(), 'PRECONDITION: no tasks exists');

		dcr.id = null; // prepare to re-insert
		test.startTest();
		insert dcr;
		test.stopTest();
		Checklist_Entry__c testEntry = [select id, Internal_Role__c, Due_Date__c, Task_id__c from Checklist_Entry__c where id = :entries[0].id];
		System.assertNotEquals(null, testEntry.Task_id__c);
		System.assertEquals(1, [select id, ActivityDate from Task where id = :testEntry.Task_id__c].size());

	}

	@isTest
	private static void InternalDealContactRoleTrigger_Update() {
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		Internal_Role__c dcr = [select id, Object__c, Opportunity__c, role__c, Internal_Team_Member__c from Internal_Role__c where Opportunity__c = :deal.id and role__c = :entries[0].Internal_Role__c];
		insert entries[0];

		System.assertEquals(1, [select id from Task].size(), 'PRECONDITION: one tasks exists');

		User sa = CKLST_TestObjectFactory.getSysAdmin();
		insert sa;
		System.assert(dcr.Internal_Team_Member__c != sa.Id);
		dcr.Internal_Team_Member__c = sa.Id;

		test.startTest();
		update dcr;
		test.stopTest();
		Checklist_Entry__c testEntry = [select id, Internal_Role__c, Due_Date__c, Task_id__c from Checklist_Entry__c where id = :entries[0].id];
		System.assertNotEquals(null, testEntry.Task_id__c);
		List<Task> tasks = [select id, OwnerId , ActivityDate from Task where id = :testEntry.Task_id__c];
		System.assertEquals(1, tasks.size());
		System.assertEquals(sa.Id, tasks[0].OwnerId);

	}

	@isTest
	private static void TaskTrigger_Update() {
		Opportunity deal = [select id from Opportunity];
		Checklist_Template__c template = [select id from Checklist_Template__c];

		Checklist__c checklist = new Checklist__c(Opportunity__c = deal.id, Template__c = template.id);
		insert checklist;

		list<Checklist_Entry__c> entries = CKLST_TestObjectFactory.getChecklistEntries(checklist);
		Internal_Role__c	dcr = [select id, Opportunity__c, role__c, Internal_Team_Member__c from Internal_Role__c where Opportunity__c = :deal.id and role__c = :entries[0].Internal_Role__c];
		insert entries[0];

		Checklist_Entry__c entry = [select task_id__c from Checklist_Entry__c where id = :entries[0].id];

		Task t = [select id, ActivityDate, Status, Description from Task where id = :entry.Task_id__c ];
		t.ActivityDate = t.ActivityDate.addDays(8);
		t.Description = 'Test description';
		t.Status = ChecklistServices.TASK_STATUS_COMPLETE;
		test.startTest();
		update t;
		test.stopTest();

		Checklist_Entry__c resEntry = [select Due_Date__c, Notes__c, Status__c from Checklist_Entry__c where id = :entries[0].id];
		System.assertEquals(t.ActivityDate, resEntry.Due_Date__c);
		System.assertEquals(t.Description, resEntry.Notes__c);
		System.assertEquals(ChecklistServices.STATUS_COMPLETE, resEntry.Status__c);

	}

	// helpers
	private static map<string, id> getInternalDealContactRoleMap(Opportunity deal) {
		map<string, id> res = new map<string, id>();
		for(Internal_Role__c dcr : [select Internal_Team_Member__c, role__c from Internal_Role__c where Opportunity__c = :deal.id]) {
			res.put(dcr.role__c, dcr.Internal_Team_Member__c);
		}
		return res;
	}
}