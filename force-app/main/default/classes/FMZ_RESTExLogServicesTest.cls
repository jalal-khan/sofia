@isTest
public class FMZ_RESTExLogServicesTest {
	static string orgId = 'testorgid';
    
    @testSetup static void makeData(){
        Account a = new Account();
        a.Name = 'test acct';
        insert a;
        
        Client_Link__c cl = new Client_Link__c();
        cl.Account__c = a.Id;
        cl.Active_Monitoring_Enabled__c = true;
        cl.Active_Monitoring_Restarts__c = 0;
        cl.Client_Org_Id__c = orgId;
        cl.Subscription_End__c = Date.today().addYears(1);
        insert cl;
    }
    static integer numOfExes = 400;
    
    @isTest
    static void testExLogService(){
        list<FMZ_RESTExLogServices.Ex> exes = new list<FMZ_RESTExLogServices.Ex>();
        for(integer i=0;i<numOfExes;i++){
            FMZ_RESTExLogServices.Ex e = new FMZ_RESTExLogServices.Ex();
            e.additionalInfo = 'test'+i;
            e.lineNumber = i;
            e.location = 'test'+i;
            e.message = 'test'+i;
            e.stackTrace = 'test'+i;
            e.type = 'test'+i;
            e.user = 'test'+i;
            exes.add(e);
        }
        test.startTest();
        FMZ_RESTExLogServices.addExceptions(exes, orgId);
        test.stopTest();
        /*exc.Additional_Info__c = ex.additionalInfo;
            exc.Line_Number__c = ex.lineNumber;
            exc.Location__c = ex.location;
            exc.Message__c = ex.message;
            exc.Stack_Trace__c = ex.stackTrace;
            exc.Type__c = ex.type;
            exc.Client_Org_Id__c = orgId;*/
        list<Exception__c> exLogs = [SELECT Id,Line_Number__c,Location__c,Message__c,Stack_Trace__c,Type__c,Client_Org_Id__c FROM Exception__c];
        system.assertEquals(numOfExes,exLogs.size());
    }
    
}