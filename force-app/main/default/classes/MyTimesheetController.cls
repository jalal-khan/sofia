global class MyTimesheetController {
    public date startDate{get;set;}
    public Work_Week__c workWeek{get;set;}
    public list<Date> workDays{get;set;}
    public list<String> workDaysString{get;set;}
    public list<SelectOption> tasksAvailableForView{get;set;}
    public map<Date,decimal> summaryLine{get;set;}
    public Timesheet timesheet{get;set;}
    public boolean isPaging{get;set;}
    public boolean isAdmin{get;set;}
    public Id runningAsUser{get;set;}
    public string titleScope{get;set;}
    
    public boolean allowSubmit{
        get{
            boolean isOpen = false;
            integer i = 0;
            for(WorkTaskLine wtl:this.timesheet.tasks.values()){
                if(wtl.status == 'Open' || wtl.status == 'Rejected'){
                    isOpen = true;
                    break;
                }
            }
            return isOpen;
        }set;
    }
    public void ensureTimesheet(){
        if(this.timesheet.ts.Id == null){
            insert this.timesheet.ts;
        }
    }
    public MyTimesheetController(){
        User u = [SELECT Id, Timesheet_Administrator__c FROM User WHERE Id = :UserInfo.getUserId()];
        this.runningAsUser = u.Id;
        this.isAdmin = u.Timesheet_Administrator__c;
        if(ApexPages.currentPage().getParameters().containsKey('adminView')){
            this.isAdmin = true;
        }
        this.startDate = Date.today().toStartOfWeek();
        this.workWeek = [SELECT Id FROM Work_Week__c WHERE Start_Date__c = :this.startDate][0];
        doSetup();
    }
    
    public void doSetup(){
        Id runningUser = this.runningAsUser;
        Date sd = this.startDate;
        this.workWeek = [SELECT Id FROM Work_Week__c WHERE Start_Date__c = :sd][0];
        list<Timesheet__c> sheets = [SELECT Id FROM Timesheet__c WHERE OwnerId = :runningUser AND Work_Week__c = :this.workWeek.Id LIMIT 1];
        
        if(runningUser != UserInfo.getUserId()){
            User u = [SELECT FirstName, LastName FROM User WHERE Id = :runningUser];
            this.titleScope = u.FirstName + ' ' + u.LastName;
        }else{
            this.titleScope = 'My ';
        }
        
        if(!sheets.isEmpty()){
            this.timesheet = new Timesheet(sheets[0]);
        }else{
            Timesheet__c t = new Timesheet__c();
            t.OwnerId = runningUser;
            t.Work_Week__c = workWeek.Id;
            t.Start_Date__c = this.startDate;
            if(this.isPaging != null && this.isPaging){
                insert t;
            }
            this.timesheet = new Timesheet(t);
        }
        this.workDays = new list<Date>();
        this.workDaysString = new list<String>();
        
        for(integer i = 0;i<7;i++){
            this.workDays.add(this.startDate.addDays(i));
            this.workDaysString.add(this.startDate.addDays(i).format());
        }
        
        // get timesheet tasks already created
        list<Id> wtIds = new list<Id>();
        if(this.timesheet.ts.Id != null){
            for(Timesheet_Task__c tsTask : [SELECT Id, Timesheet__c, Work_Task__c, Work_Task__r.Remaining_Hours__c, Work_Task__r.Completed_Hours__c, Status__c,
                                            Hours_Used__c, Rejected_Hours_Comment__c,
				Work_Task__r.Allocated_Hours__c, Work_Task__r.Project_Name__c,
				Work_Task__r.Assigned_to__r.FirstName, Work_Task__r.Assigned_to__r.LastName,
				Work_Task__r.Employee_Assigned_to__r.FirstName, Work_Task__r.Employee_Assigned_to__r.LastName,
				Work_Task__r.Hour_Type__c, Work_Task__r.Reference_Name__c,Work_Task__r.Task_Type__c,
                                            Work_Task__r.Work__r.Fortimize_Project__c, Billable_Amount__c 
                                            FROM Timesheet_Task__c 
                                            WHERE Timesheet__c = :this.timesheet.ts.Id 
                                            ORDER BY Work_Task__r.Name ASC]){
                                                this.timesheet.addTimesheetTask(tsTask, this.isAdmin);
                                                wtIds.add(tsTask.Work_Task__c);
                                            }
        }
        
        this.timesheet = populateTimeLogsForTimesheet(this.workDays, this.timesheet);
        calculateWorkTaskHours();
        //this.workTaskLines = populateTasks(this.workDays);
        //this.workTaskLines.sort();
        system.debug('this.timesheet.tasks.values(): ' + this.timesheet.tasks.values());
        this.tasksAvailableForView =  getPossibleTasks(this.timesheet.tasks.values());
        this.summaryLine = getSummaryLine();
        
    }
    
    public PageReference changePerspective(){
		// the running as user has been changed.
        this.isPaging = true;
        this.isSubmit = false;
        doSetup();
        return null;
    }
    
    public list<SelectOption> allUsers{
        get{
            if(allUsers == null){
                allUsers = new list<SelectOption>();
                for(User u:[SELECT Id, FirstName, LastName FROM User WHERE isActive = true ORDER BY LastName ASC]){
                    allUsers.add(new SelectOption(u.Id, u.FirstName + ' ' + u.LastName));
                }
            }
            return allUsers;
        }set;}
    
    public Timesheet populateTimeLogsForTimesheet(list<Date> dates, Timesheet ts){
        list<Id> taskIds = new list<Id>();
        for(WorkTaskLine wtl:ts.tasks.values()){
            taskIds.add(wtl.timesheetTaskId);
        }
        list<Time_Log__c> logs = [SELECT Id, Hours_Completed__c, Date__c, Work_Week__c, Internal_Comments__c, 
                                  External_Comments__c, Timesheet_Task__c, Work_Task__c, Status__c, Work_Task__r.Hour_Type__c 
                                  FROM Time_Log__c 
                                  WHERE Timesheet_Task__c IN :taskIds
                                  ORDER BY Name ASC];
        
        // this loop populates all the time logs that already exist
        for(Time_Log__c tl:logs){
            ts.tasks.get(tl.Timesheet_Task__c).addTime(tl);
        }
        
        // this loop fills in any missing dates with empty time logs so they have a place on the page.
        for(Date dt:dates){
            for(WorkTaskLine wtl:ts.tasks.values()){
                if(!wtl.timeLogs.containsKey(dt)){
                    Time_Log__c tl = new Time_Log__c();
                    tl.Date__c = dt;
                    tl.Hours_Completed__c = 0;
                    tl.Work_Task__c = wtl.workTaskId;
                    tl.Timesheet_Task__c = wtl.timesheetTaskId;
                    wtl.addTime(tl);
                }
            }
        }
        return ts;
    }
    
    
    public boolean canSave{
        get{
            if(!timesheet.tasks.isEmpty()){
                for(WorkTaskLine wtl:timesheet.tasks.values()){
                    if(wtl.canEdit){
                        return true;
                    }
                }
                return false;
            }
            return true;
        }
    }
    
    public list<SelectOption> getPossibleTasks(list<WorkTaskLine> tasksInView){
        list<Id> tasksInViewIds = new list<Id>();
        tasksInView.sort();
        for(WorkTaskLine wtl:tasksInView){
            // exclude tasks that are already to be displayed in the task list.
            // if a task is Open or Rejected, don't let them add it again.
                system.debug('wtl.status: ' + wtl.status);
            system.debug('comparing to: ' + WorkTaskLine.STATUS_OPEN);
            if(wtl.status == WorkTaskLine.STATUS_OPEN || wtl.status == WorkTaskLine.STATUS_REJECTED){
                system.debug('adding wtl id: : ' + wtl.workTaskId);
                system.debug(wtl.workTaskName);
               tasksInViewIds.add(wtl.workTaskId);
            }
        }
            system.debug('tasks in view: ' + tasksInViewIds);
        list<SelectOption> options = new list<SelectOption>();
        Id runningUser = this.runningAsUser;
        // query all the Work Tasks assigned to the user that have hours available.
        for(Work_Task__c wt:[SELECT Id, Reference_Name__c FROM Work_Task__c 
		WHERE Remaining_Hours__c > 0 AND (Employee_Assigned_To__r.User_Link__c = :runningUser OR Assigned_to__c = :runningUser) AND Id NOT IN :tasksInViewIds
                             And Work__r.Fortimize_Project__r.Current_Sprint__c NOT IN :closedProjectStages 
                            ORDER BY Reference_Name__c ASC]){
            options.add(new SelectOption(wt.Id,wt.Reference_Name__c));
        }
        return options;
    }
        
    public decimal totalHoursForTheWeek{
        get{
            decimal sum = 0;
            for(WorkTaskLine wtl:timesheet.tasks.values()){
                for(Time_Log__c tl:wtl.timeLogs.values()){
                    if(tl.Hours_Completed__c != null){
                        sum += tl.Hours_Completed__c;
                    }
                }
            }
            return sum; 
        }set;
    }
    
    public decimal totalBillableHoursForTheWeek{
        get{
            decimal sum = 0;
            List<WorkTaskLine> billableWTLs = new List<WorkTaskLine>();
            for(WorkTaskLine wtl:timesheet.tasks.values()){
                if(wtl.workTaskHourType == 'Billable' || wtl.workTaskHourType == 'Travel - Billable'){
                    billableWTLs.add(wtl);
                }
            }
            for(WorkTaskLine wtl:billableWTLs){
                for(Time_Log__c tl:wtl.timeLogs.values()){
                    if(tl.Hours_Completed__c != null){
                        sum += tl.Hours_Completed__c;
                    }
                }
            }
            return sum; 
        }set;
    }
    
    public void calculateWorkTaskHours(){
        for(WorkTaskLine wtl:timesheet.tasks.values()){
            wtl.totalHours = 0;
            for(Time_Log__c tl:wtl.timeLogs.values()){
                if(tl.Hours_Completed__c != null){
                    wtl.totalHours += tl.Hours_Completed__c;
                }
            }
        }
    }
    
    public map<date,decimal> getSummaryLine(){
        map<date,decimal> m = new map<date,decimal>();
        for(Date d:this.workDays){
            m.put(d,0);
        }
        for(WorkTaskLine wtl:timesheet.tasks.values()){
            for(Date d:wtl.timeLogs.keySet()){
                if(wtl.timeLogs.get(d).Hours_Completed__c != null && wtl.timeLogs.get(d).Hours_Completed__c > 0){
                    decimal dec = m.get(d);
                    dec += wtl.timeLogs.get(d).Hours_Completed__c;
                    m.put(d,dec);
                }
            }
        }
        return m;
    }
    
    public list<Id> newTaskIds{get;set;}
    
    public void addWorkTaskLines(){
        system.debug('newTaskIds is: ' + newTaskIds);
        list<Timesheet_Task__c> newTasks = new list<Timesheet_Task__c>();
        
        for(Id i:newTaskIds){
            Timesheet_Task__c tst = new Timesheet_Task__c();
            tst.Work_Task__c = i;
            tst.Timesheet__c = this.timesheet.ts.Id;
            tst.Status__c = WorkTaskLine.STATUS_OPEN;
            newTasks.add(tst);
        }
        insert newTasks;
        for(Timesheet_Task__c tt:newTasks){
        	this.timesheet.addTimesheetTask(this.workDays, tt);
        }
        this.tasksAvailableForView =  getPossibleTasks(this.timesheet.tasks.values());
    }
    
    public Id deleteRowId{get;set;}
    
    public void deleteRowFromCurrentView(){
        system.debug('deleteRowId::: ' + deleteRowId);
        integer i = 0;
        for(WorkTaskLine wtl:this.timesheet.tasks.values()){
            if(wtl.timesheetTaskId == deleteRowId){
                list<Time_Log__c> toDelete = new list<Time_Log__c>();
                for(Time_Log__c tl:wtl.timeLogs.values()){
                    if(tl.Id != null){
                        toDelete.add(tl);
                    }
                }
                if(!toDelete.isEmpty()){
                    delete toDelete;
                }
                Timesheet_Task__c deleteMe = new Timesheet_Task__c(Id = wtl.timesheetTaskId);
                delete deleteMe;
                
                this.tasksAvailableForView.add(new SelectOption(wtl.workTaskId, wtl.workTaskName));
                this.timesheet.tasks.remove(wtl.timesheetTaskId);
                break;
            }
            i++;
        }
        this.summaryLine = getSummaryLine();
    }
    
    public boolean isSubmit{get;set;}
    public void saveAndSubmit(){
        this.isSubmit = true;
        saveLines();
        this.isSubmit = false;
    }
    
    public void saveLines(){
        System.SavePoint sp = Database.setSavepoint();
        if(this.timesheet.ts.id == null){
            insert this.timesheet.ts;
        }
        list<Time_Log__c> toUpsert = new list<Time_Log__c>();
        list<Time_Log__c> toDelete = new list<Time_Log__c>();
        system.debug('work task lines: ' + JSON.serialize(timesheet.tasks.values()));
        set<string> decimals = new set<string>
        {
            '0','00','25','5','50','75'
        };
        boolean hasError = false;
        for(WorkTaskLine wtl:this.timesheet.tasks.values()){
            wtl.totalHours = 0;
            for(Date d:wtl.timeLogs.keySet()){
                Time_Log__c tl = wtl.timeLogs.get(d);
                if(tl.Id != null && (tl.Hours_Completed__c==null || tl.Hours_Completed__c <= 0)){
                    // this has been zeroed out and needs to be deleted.
                    toDelete.add(tl);
                }
                else if(tl.Hours_Completed__c > 0){
                	// check the hours, ensure .25 increments.
                    string dec = string.valueOf(tl.Hours_Completed__c);
                    if(dec.contains('.') && !decimals.contains(dec.substringAfter('.'))){
                        tl.Hours_Completed__c.addError('Must use .25 increments');
                        hasError = true;
                    }else{
                        wtl.totalHours += tl.Hours_Completed__c;
                        tl.Timesheet_Task__c = wtl.timesheetTaskId;
                        tl.Date__c = d;
                        toUpsert.add(tl);
                        if((wtl.status == WorkTaskLine.STATUS_OPEN || wtl.status == WorkTaskLine.STATUS_REJECTED) && this.isSubmit != null && this.isSubmit){
                            tl.Status__c = WorkTaskLine.STATUS_SUBMITTED;
                        }
                    }
                }
            }
        }
        set<Id> deletedIds = new set<Id>();
        
        if(!hasError){
            if(!toDelete.isEmpty()){
                try{
                    system.debug('trying to delete hours: ' + JSON.serialize(toDelete));
                    delete toDelete;
                    for(Time_Log__c tl:toDelete){
                        deletedIds.add(tl.Id);
                    }
                }catch(Exception e){
                    Database.rollback(sp);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'There was an error while trying to remove your hours. ' + e.getMessage()));
                }
            }
            if(!toUpsert.isEmpty()){
                try{
                    upsert toUpsert;
                }catch(Exception e){
                    Database.rollback(sp);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'There was an error while saving your time: ' + e.getMessage()));
                }
            }
            
            list<Timesheet_Task__c> taskList = new list<Timesheet_Task__c>();
            list<Timesheet_Task__c> deleteList = new list<Timesheet_Task__c>();
            if(this.isSubmit != null && this.isSubmit){
                // delete any timesheet tasks with no time entered.
                for(WorkTaskLine wtl:this.timesheet.tasks.values()){
                    system.debug('work task being evaluated: ' + JSON.serialize(wtl));
                    boolean hasHours = false;
                    if(((wtl.status == WorkTaskLine.STATUS_SUBMITTED && this.isAdmin) || wtl.status != WorkTaskLine.STATUS_SUBMITTED) && wtl.status != WorkTaskLine.STATUS_APPROVED){
                        if(wtl.totalHours > 0){
                            taskList.add(new Timesheet_Task__c(
                                Id=wtl.timesheetTaskId,
                                Status__c = WorkTaskLine.STATUS_SUBMITTED,
                            	Hours_used__c = wtl.totalHours
                            ));
                            wtl.status = WorkTaskLine.STATUS_SUBMITTED;
                            wtl.canEdit = false;
                        }else{
                            deleteList.add(new Timesheet_Task__c(Id=wtl.timesheetTaskId));
                        }
                    }
                }
            }else{
                for(WorkTaskLine wtl:this.timesheet.tasks.values()){
                    if(((wtl.status == WorkTaskLine.STATUS_SUBMITTED && this.isAdmin) || wtl.status != WorkTaskLine.STATUS_SUBMITTED) && wtl.status != WorkTaskLine.STATUS_APPROVED){
                        if(wtl.totalHours > 0){
                            taskList.add(new Timesheet_Task__c(
                                Id=wtl.timesheetTaskId,
                            	Hours_used__c = wtl.totalHours
                            ));
                        }
                    }
                }
            }
            if(!taskList.isEmpty()){
                try{
                    update taskList;
                    delete deleteList;
                }catch(Exception e){
                    Database.rollback(sp);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'There was an error while saving your time: ' + e.getMessage()));
                }
            }
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM ,'Timesheet Saved.'));
            //recalculateWorkTaskHours(deletedIds);
            //this.summaryLine = getSummaryLine();
            doSetup();
        }
    }
    
    public void nextView(){
        this.isPaging = true;
        this.isSubmit = false;
        this.startDate = this.startDate.addDays(7);
        doSetup();
    }
    
    public void previousView(){
        this.isPaging = true;
        this.isSubmit = false;
        this.startDate = this.startDate.addDays(-7);
        doSetup();
    }
    
    public class Timesheet {
        
        public Timesheet__c ts{get;set;}
        public map<Id,WorkTaskLine> tasks{get;set;}
        public list<WorkTaskLine> taskLines{
            get{
                taskLines = this.tasks.values();
                taskLines.sort();
                
                return taskLines;
            }set;
        }
        
        public Timesheet(Timesheet__c t){
            this.ts = t;
            this.tasks = new map<Id,WorkTaskLine>();
        }
        
        public void addTimesheetTask(Timesheet_Task__c tt, boolean isAdmin){
            this.tasks.put(tt.Id,new WorkTaskLine(tt, isAdmin));
        }
        
        public void addTimesheetTask(list<Date> dates, Timesheet_Task__c tt){
            this.tasks.put(tt.Id,new WorkTaskLine(dates, tt));
        }
    }
    public static final list<string> closedProjectStages = new list<string>{
        'Complete','Cancelled','Complete - Pending Final Payment'
    };
}