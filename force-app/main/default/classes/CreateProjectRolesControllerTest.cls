@IsTest(IsParallel=true)
private class CreateProjectRolesControllerTest {
    
    @IsTest
    static void success() {
        FMZ_Project__c project = new FMZ_Project__c();
        insert project;

        Opportunity opportunity = TestObjectFactory.getOpportunity();
        opportunity.FMZ_Project__c = project.Id;
        insert opportunity;

        Test.startTest();
        Id returnId = CreateProjectRolesController.createProjectRoles(opportunity.Id);
        Test.stopTest();   

        System.assertEquals(project.Id, returnId);
    }

    @IsTest
    static void noProject() {
        Opportunity opportunity = TestObjectFactory.getOpportunity();
        opportunity.FMZ_Project__c = null;
        insert opportunity;

        Test.startTest();
        Boolean exceptionThrown = false;
        try {
            CreateProjectRolesController.createProjectRoles(opportunity.Id);
        } catch (AuraException e) {
            exceptionThrown = true;
        }
        Test.stopTest();

        System.assert(true);
    }
}