@isTest
private class ChecklistTemplateController_Test {

	@isTest
	private static void testChecklistObjects() {
		Checklist_Template__c template = new Checklist_Template__c();
		ApexPages.StandardController sctrl = new ApexPages.StandardController(template);
		ChecklistTemplateController ctrl = new ChecklistTemplateController(sctrl);

		List<String> clObjOptions = FMZ_SchemaUtils.getPickListValues(Checklist_Template__c.sObjectType, Checklist_Template__c.Object__c.getDescribe().getName());
		clObjOptions.sort();
		System.assert(ctrl.checklistObjects.size() > 0);
		System.assertEquals(clObjOptions.size()+1, ctrl.checklistObjects.size()); //one more for --Select-- Option
	}

	@isTest
	private static void testNewChecklist() {
		Checklist_Template__c template = new Checklist_Template__c();
		ApexPages.StandardController sctrl = new ApexPages.StandardController(template);
		ChecklistTemplateController ctrl = new ChecklistTemplateController(sctrl);

		System.assertEquals(null, ctrl.templateId);
		System.assertEquals('New Checklist Template', ctrl.subtitle);
		System.assertEquals(1, ctrl.sections.size());
		System.assertEquals(1, ctrl.sections[0].items.size());
	}

	@isTest
	private static void testNewSection() {
		Checklist_Template__c template = new Checklist_Template__c();
		ApexPages.StandardController sctrl = new ApexPages.StandardController(template);
		ChecklistTemplateController ctrl = new ChecklistTemplateController(sctrl);

		ctrl.sections[0].name = 'First';
		ctrl.newSection();
		System.assertEquals(2, ctrl.sections.size());
		System.assertEquals('First', ctrl.sections[0].name);
		System.assertEquals('Section 2', ctrl.sections[1].name);

	}

	@isTest
	private static void testDeleteSection() {
		Checklist_Template__c template = new Checklist_Template__c();
		ApexPages.StandardController sctrl = new ApexPages.StandardController(template);
		ChecklistTemplateController ctrl = new ChecklistTemplateController(sctrl);

		ctrl.sections[0].name = 'First';
		ctrl.newSection();
		ctrl.actionSectionIndex = 0;
		ctrl.deleteSection();
		System.assertEquals(1, ctrl.sections.size());
		System.assertEquals('Section 2', ctrl.sections[0].name);

	}

	@isTest
	private static void testNewItem() {
		Checklist_Template__c template = new Checklist_Template__c();
		ApexPages.StandardController sctrl = new ApexPages.StandardController(template);
		ChecklistTemplateController ctrl = new ChecklistTemplateController(sctrl);

		ctrl.sections[0].items[0].description = 'First';
		ctrl.actionSectionIndex = 0;
		ctrl.newItem();
		System.assertEquals(2, ctrl.sections[0].items.size());
		System.assertEquals('First', ctrl.sections[0].items[0].description);
		System.assertEquals(null, ctrl.sections[0].items[1].description);

	}

	@isTest
	private static void testDeleteItem() {
		Checklist_Template__c template = new Checklist_Template__c();
		ApexPages.StandardController sctrl = new ApexPages.StandardController(template);
		ChecklistTemplateController ctrl = new ChecklistTemplateController(sctrl);

		ctrl.sections[0].items[0].description = 'First';
		ctrl.actionSectionIndex = 0;
		ctrl.newItem();
		ctrl.actionItemIndex = 0;
		ctrl.deleteItem();
		System.assertEquals(1, ctrl.sections[0].items.size());
		System.assertEquals(null, ctrl.sections[0].items[0].description);

	}

	@isTest
	private static void testSave() {
		Checklist_Template__c template = new Checklist_Template__c();
		ApexPages.StandardController sctrl = new ApexPages.StandardController(template);
		ChecklistTemplateController ctrl = new ChecklistTemplateController(sctrl);

		ctrl.template.Name = 'TestTemplate';
		ctrl.template.Object__c = 'Opportunity';
		ctrl.sections[0].items[0].description = 'First';
		ctrl.actionSectionIndex = 0;
		ctrl.newItem();
		ctrl.newItem();
		for (integer i=0; i<3; i++) {
			ChecklistTemplateController.ChecklistTemplateEntry item = ctrl.sections[0].items[i];
			item.description = 'Item ' + (i+1);
			item.dueDateBasedOn = ctrl.dealDateFields[0].getValue();
			item.dueDateOffset = i;
			item.defaultDealContactRole = ctrl.dealTeamRoles[0].getValue();
			item.saveTaskNotes = true;
		}

		ctrl.newSection();
		ctrl.actionSectionIndex = 1;
		ctrl.newItem();
		ctrl.newItem();
		for (integer i=0; i<3; i++) {
			ChecklistTemplateController.ChecklistTemplateEntry item = ctrl.sections[1].items[i];
			item.description = 'S2 Item ' + (i+1);
			item.dueDateBasedOn = ctrl.dealDateFields[0].getValue();
			item.dueDateOffset = i;
			item.defaultDealContactRole = ctrl.dealTeamRoles[0].getValue();
			item.saveTaskNotes = true;
		}

		PageReference ref = ctrl.saveTemplate();
		System.assertNotEquals(null, ref);

		list<Checklist_Template__c> res = [
			select id, name,
						 (select id, name, Checklist_Template__c, Section__c, Sort_Order__c, Due_Date_Based_On__c,
						 				 Due_Date_Offset__c, Save_Task_Notes__c, Default_Deal_Team_Role__c
								from Checklist_Template_Entries__r
						order by Sort_Order__c, name)
				from Checklist_Template__c
		];
		System.assertEquals(1, res.size());
		System.assertEquals('TestTemplate', res[0].name);
		System.assertEquals(6, res[0].Checklist_Template_Entries__r.size());
		list<Checklist_Template_Entry__c> entries = res[0].Checklist_Template_Entries__r;
		System.assertEquals('Item 1', entries[0].name);
		System.assertEquals('S2 Item 1', entries[1].name);
		System.assertEquals('Item 2', entries[2].name);
		System.assertEquals('S2 Item 2', entries[3].name);
		System.assertEquals('Item 3', entries[4].name);
		System.assertEquals('S2 Item 3', entries[5].name);
	}

	// @isTest
	private static void testEditExisting() {
		string TEMPLATE_NAME = 'Template 1';

		list<Checklist_Template__c> templates = CKLST_TestObjectFactory.getChecklistTemplates(3);
		templates[0].name = TEMPLATE_NAME;
		insert templates;

		list<Checklist_Template_Entry__c> entries = CKLST_TestObjectFactory.getChecklistTemplateEntries(templates, 3);
		insert entries;

		ApexPages.StandardController sctrl = new ApexPages.StandardController(templates[0]);
		ChecklistTemplateController ctrl = new ChecklistTemplateController(sctrl);

		System.assertEquals(1, ctrl.sections.size());
		System.assertEquals(3, ctrl.sections[0].items.size());
	}
}