public without sharing class OpportunityTriggerHandler {

    public static void afterUpdate(Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap) {
        handleSalesProjectedStartDate(newMap, oldMap);
    }

    // JIRA FOR-101 - push forecast out based on change in sales projected start week
    private static void handleSalesProjectedStartDate(Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap) {

        // find opportunities where the start date has changed
        Map<Id, Opportunity> changedOpportunities = new Map<Id, Opportunity>();
        for (Opportunity o : newMap.values()) {
            if (o.Sales_Projected_Start_Date__c != oldMap.get(o.Id).Sales_Projected_Start_Date__c
            && o.Sales_Projected_Start_Date__c != null) {
                changedOpportunities.put(o.Id, o);
            }
        }

        if (changedOpportunities.isEmpty()) {
            return;
        }

        // work weeks start on sunday, end on saturday
        // ASSUMPTION: work weeks exist into the future and are always setup correctly
        Map<Id, Work_Week__c> workWeekMap = new Map<Id, Work_Week__c>(
            [SELECT Id, Start_Date__c
            FROM Work_Week__c
            WHERE Start_Date__c <> NULL]
        );

        // map work weeks by date to make them easier to find
        Map<Date, Id> workWeekByDateMap = new Map<Date, Id>();
        for (Work_Week__c ww : workWeekMap.values()) {
            workWeekByDateMap.put(ww.Start_Date__c, ww.Id);
        }

        // get all the forecasts for the project roles related to the opportunity
        List<Forecast__c> forecasts =
            [SELECT Id, Work_Week__c, Work_Week__r.Id, Work_Week__r.Start_Date__c, Project_Role__r.Opportunity__c
            FROM Forecast__c
            WHERE Work_Week__c <> NULL
            AND Work_Week__r.Start_Date__c <> NULL
            AND Project_Role__r.Opportunity__c IN :changedOpportunities.keySet()
            AND Project_Role__r.Project__c = NULL
            ORDER BY Project_Role__r.Opportunity__c,
            Work_Week__r.Start_Date__c];

        Work_Week__c oldWorkWeek;
        Work_Week__c newWorkWeek;
        Id currentOpportunityId;
        Integer shiftDays;

        for (Forecast__c f : forecasts) {
            if (f.Project_Role__r.Opportunity__c != currentOpportunityId) {
                currentOpportunityId = f.Project_Role__r.Opportunity__c;
                Id wwId = oldMap.get(currentOpportunityId).Sales_Projected_Start_Date__c;
                oldWorkWeek = wwId != null ? workWeekMap.get(wwId) : null;
                // if the was no old work week, use the first forecast work week for this opportunity
                if (oldWorkWeek == null) {
                    oldWorkWeek = f.Work_Week__r;
                }
                wwId = newMap.get(currentOpportunityId).Sales_Projected_Start_Date__c;
                newWorkWeek = workWeekMap.get(newMap.get(currentOpportunityId).Sales_Projected_Start_Date__c);
                shiftDays = oldWorkWeek.Start_Date__c.daysBetween(newWorkWeek.Start_Date__c);
            }
            // shift the work week on the forecast based on the difference between work weeks
            Date newStartDate = f.Work_Week__r.Start_Date__c.addDays(shiftDays);
            f.Work_Week__c = workWeekByDateMap.get(newStartDate);
        }

        update forecasts;

    }

}