public class InternalRoleNewController {

	public Internal_Role__c internalRole {
		get {
			return (Internal_Role__c)ctrl.getRecord();
		}
	}

	public list<SelectOption> dealTeamRoles {
		get {
			if (this.dealTeamRoles == null) {
				list<SelectOption> options = new list<SelectOption>();
				options.add(new SelectOption('', '--Select--'));
				map<string, string> dcrMap = FMZ_SchemaUtils.getPickListValueMap(Internal_Role__c.sObjectType, Internal_Role__c.Role__c.getDescribe().getName());
				map<string, List<string>> dcrDependentValuesMap = FMZ_SchemaUtils.GetDependentOptions(
						Internal_Role__c.sObjectType.getDescribe().getName(),
						Internal_Role__c.Object__c.getDescribe().getName(),
						Internal_Role__c.Role__c.getDescribe().getName()
				);
				list<string> dcrList = dcrDependentValuesMap.get(this.clObject);
				if(dcrList == null || dcrList.size() == 0){
					dcrList = FMZ_SchemaUtils.getPickListValues(Internal_Role__c.sObjectType, Internal_Role__c.Role__c.getDescribe().getName());
				}
				dcrList.sort();
				for (string dcr : dcrList) {
					options.add(new SelectOption(dcr, dcrMap.get(dcr)));
				}
				dealTeamRoles = options;
			}
			return dealTeamRoles;
		}
		private set;
	}

	private String clObject {get; set;}

	public pageReference saveRecord(){
		this.internalRole.Object__c = this.clObject;
		return this.ctrl.save();
	}

	private ApexPages.StandardController ctrl {get; set;}
	public InternalRoleNewController(ApexPages.StandardController ctrl) {
		this.ctrl = ctrl;
		if (Test.isRunningTest() == false) {
			this.ctrl.addFields(new List<String>{FMZ_SchemaUtils.currentNamespacePrefix+'Object__c'});
		}
		Map<String,Schema.SObjectField> fields = Internal_Role__c.sObjectType.getDescribe().fields.getMap();
		Schema.FieldSet fieldset = Internal_Role__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		List<DescribeFieldResult> dfrs = new List<DescribeFieldResult>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			DescribeFieldResult dfr = fields.get(fsm.getFieldPath()).getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
			}
		}
		if(dfrs.size() > 0){
			Map<String, String> objOptsMap = FMZ_SchemaUtils.getPickListValueMap(
					Internal_Role__c.sObjectType,
					Internal_Role__c.Object__c.getDescribe().getName()
			);
			for(DescribeFieldResult dfr : dfrs){
				if(this.internalRole.get(dfr.getName()) != null && objOptsMap.keySet().contains(dfr.getLabel())){
					//If this Internal Role is assigned to more than one of these lookup fields,
					//logic not defined, all bets are off. This should not happen for a new record
					this.clObject = objOptsMap.get(dfr.getLabel());
					break;
				}
			}
		}
	}
}