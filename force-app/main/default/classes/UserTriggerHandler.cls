public class UserTriggerHandler {

    public void afterInsert(List<User> newUsers){
        List<Id> userIds = new List<Id>();
        
        for(User u : newUsers){
            userIds.add(u.Id);
        }
        
        CashFlowDataSharing.userSharing(userIds, false);
        setNewUserMetadata(userIds);
    }
    
    public void afterUpdate(List<User> newUsers, Map<Id,User> oldUsersMap){
        List<Id> userIds = new List<Id>(oldUsersMap.keySet());
        CashFlowDataSharing.userSharing(userIds, true);
    }
    
    private static void setNewUserMetadata(List<Id> userIds){
        /*List<User> newUsers = [SELECT Id, FirstName, LastName FROM User WHERE Id In: UserIds];
        List<CashFlowSharing__c> metadata = [SELECT Id, Name, BAT__c, Executive__c, Finance__c,
                                                 HR__c, Legal__c, Professional_Services__c, Revenue__c,
                                                 User_Id__c FROM CashFlowSharing__c];
        
        for(CashFlowSharing__c cfs : metadata){
            for(User u : newUsers){
                if(cfs.Name == u.FirstName + ' ' + u.LastName && cfs.User_Id__c == null){
                    cfs.User_Id__c = u.Id;
                }
            }
        }
        UPDATE metadata; Broken, needs to be redone somehow.*/
    }
}