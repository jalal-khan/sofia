@RestResource(urlMapping='/ClientLink/')
global class FMZ_RESTClientLink {
    /**
    * @description: the method that receives the POST from the client orgs on a daily basis.  Responds with client service details.
    * @param a map<string,string> of key/value pairs.  this keeps things extensible.
    * @response: a map<string,string> of possible values to send back to client.  Map used to keep things extensible and not break deserialization.
    * subscriptionEnd: if the date sent from client is different than what's in our org, respond with the new date.
    * activeMonitoringStatus: if the status sent is different than what's in our org, respond with the status
    * restartActiveMonitoring: tells the client org to reschedule the exception diagnostics batch immediately
    */
    @HttpPost
    global static ResponseObject phoneHome(PostObj postObject){
        
        map<string,string> pMap = postObject.postMap;
        ResponseObject resp = new ResponseObject();
        if(pMap.containsKey('orgId')){
            string clientOrg = pMap.get('orgId');
            boolean kickstartActiveMonitoring = false;
            Date d;
            
            if(pMap.containsKey('subscriptionEnd')){
            	d = Date.valueOf(pMap.get('subscriptionEnd'));
            }
            
            boolean activeMonitoringStatus = false;
            if(pMap.containsKey('activeMonitoringStatus')){
                activeMonitoringStatus = pMap.get('activeMonitoringStatus').containsIgnoreCase('true') ? true : false;
            }
            
            DateTime lastExceptionTime;
            if(pMap.containsKey('lastExceptionTime')){
                lastExceptionTime = DateTime.valueOf(pMap.get('lastExceptionTime'));
            }           
            
            
            // find the current client org link
            list<Client_Link__c> clinks = [SELECT Id, Client_Org_Id__c, Subscription_End__c, Active_Monitoring_Enabled__c, Active_Monitoring_Restarts__c FROM Client_Link__c WHERE Client_Org_Id__c = :clientOrg];
            Client_Link__c cl;
                        
            if(!clinks.isEmpty()){
                cl = clinks[0];
                if(cl.Subscription_End__c != d){
                    // the subscription end date has changed and the client org needs to be updated.
                    resp.resMap.put('subscriptionEnd',string.valueOf(cl.Subscription_End__c));
                }
                if(cl.Active_Monitoring_Enabled__c != activeMonitoringStatus){
                    resp.resMap.put('activeMonitoringStatus',string.valueOf(cl.Active_Monitoring_Enabled__c));
                    if(cl.Active_Monitoring_Enabled__c){
                    	kickstartActiveMonitoring = true;
                    }
                }
            }else{
                // if for some reason we don't already have the client org in our data, then we create a new instance record.
                cl = new Client_Link__c();
                cl.Client_Org_Id__c = clientOrg;
            }
            DateTime lastExceptionRuntime;
            // check if the last exception job run time is more than 3 hours older than the current time
            if(cl.Active_Monitoring_Enabled__c && cl.Subscription_End__c >= Date.today() ){
                lastExceptionRuntime = pMap.get('lastExceptionMonitoringRuntime')== null ? null : DateTime.valueOf(pMap.get('lastExceptionMonitoringRuntime'));
                DateTime thisClientLinkRuntime = DateTime.valueOf(pMap.get('runTime'));
                DateTime validCheck = thisClientLinkRunTime.addHours(-3);
                system.debug('lastExceptionRunTime: ' + lastExceptionRuntime);
                if(lastExceptionRuntime == null || lastExceptionRuntime < validCheck){
                    kickstartActiveMonitoring = true;
                    
                }
            }
            
            if(kickstartActiveMonitoring){
                DateTime kickstartTime;
                if(lastExceptionRuntime != null){
                    kickstartTime = lastExceptionRunTime;
                }else{
                    kickstartTime = DateTime.now().addHours(-12);
                }
                    
                resp.resMap.put('restartActiveMonitoring',string.valueOf(kickstartTime));
            }
            Schema.SObjectField f = Client_Link__c.Fields.Client_Org_Id__c;
            
            cl.Last_Org_Communication__c = DateTime.now();
            
            Database.upsert(cl,f,false);
            
            
            
        }
        return resp;
    }
    
    global class PostObj{
        public map<string,string> postMap{get;set;}
    }
    
    global class ResponseObject{
        public ResponseObject(){resMap = new map<string,string>();}
        public map<string,string> resMap{get;set;}
    }
}