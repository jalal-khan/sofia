@isTest
public class FMZ_RESTClientLinkTest {
	static string orgId = 'testorgid';
    
    @testSetup static void makeData(){
        Account a = new Account();
        a.Name = 'test acct';
        insert a;
        
        Client_Link__c cl = new Client_Link__c();
        cl.Account__c = a.Id;
        cl.Active_Monitoring_Enabled__c = true;
        cl.Active_Monitoring_Restarts__c = 0;
        cl.Client_Org_Id__c = orgId;
        cl.Subscription_End__c = Date.today().addYears(1);
        insert cl;
    }
    
    @isTest
    static void testPhoneHome(){
        system.assertEquals(orgId,[SELECT Client_Org_Id__c FROm Client_link__c][0].Client_Org_Id__c);
        FMZ_RESTClientLink.PostObj po = new FMZ_RESTClientLink.PostObj();
        po.postMap = new map<string,string>();
        po.postMap.put('orgId',orgId);
        po.postMap.put('subscriptionEnd',string.valueOf(Date.today()));
        po.postMap.put('activeMonitoringStatus',string.valueOf(false));
        po.postMap.put('runTime',string.valueOf(DateTime.now()));
        test.startTest();
        FMZ_RESTClientLink.ResponseObject r = FMZ_RESTClientLink.phoneHome(po);
        test.stopTest();
        
        // query the client link and ensure that the communication timestamp was updated.
        Client_Link__c cl = [SELECT Id, Last_Org_Communication__c FROM Client_Link__c][0];
        system.assertEquals(Date.today(), Date.newInstance(cl.Last_Org_Communication__c.year(), cl.Last_Org_Communication__c.month(), cl.Last_Org_Communication__c.day()));
    }
}