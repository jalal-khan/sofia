// ==================================================================================
//  Company : Force Optimized
//  Author : Mahesh S
//  Comments : To generate test data for test classes
// ==================================================================================
//  Changes: 2013-07-16 Initial version.
// ==================================================================================
public with sharing class TestDataFactory_AC {
    
    /* Account creation */
    public static List<Account> createAccounts( Integer numberOfAccounts) 
    {
        List<Account> accounts = new List<Account>();
        
        for(Integer i = 0 ; i < numberOfAccounts ; i++)
        {
             Account account = new Account( Name = 'Test Account' + Math.random());
             accounts.add(account);
        }
        
        return  accounts;
    }
    
    /* Contact creation */
    public static List<Contact> createContacts( Integer numberOfContacts) 
    {
        List<Contact> contacts = new List<Contact>();
        
        for(Integer i = 0 ; i < numberOfContacts ; i++)
        {
             Contact contct = new Contact( LastName = 'Test Contact' + Math.random());
             contacts.add(contct);
        }
        
        return  contacts;
    }
    
    /* Opportunity Creation */
    public static List<Opportunity> createOpportunities(Integer numberOfOpps)
    {
        List<Opportunity> opportunities = new List<Opportunity>();
        
        for(Integer i = 0 ; i < numberOfOpps ; i++)
        {
             Opportunity opp = new Opportunity( Name = 'Test Opportunity' + Math.random(),
                                                Type = 'SFDC Fix-it Project',
                                                Eligible_for_Reward__c = 'No',
                                                CloseDate = System.today(),
                                                StageName = 'SOW Preparation',
                                                Number_of_Payments__c = '1',
                                                Payment_1_Percentage__c = 100.00);
             opportunities.add(opp);
        }
        
        return  opportunities;
    }
    
    /* Project Creation */
  /* 14-08-14 - Commented to remove milestone package  
  public static List<Milestone1_Project__c> createProjects(Integer numberOfOpps)
    {
        List<Milestone1_Project__c> projects = new List<Milestone1_Project__c>();
        
        for(Integer i = 0 ; i < numberOfOpps ; i++)
        {
             Milestone1_Project__c project = new Milestone1_Project__c( Name = 'Test Project' + Math.random(),
                                                                        Status__c = 'Active' );
             projects.add(project);
        }
        
        return  projects;
    } */
    
     /* SOW Creation */

  public static List<SOW__c> createSOWs(Integer numberOfOpps)
    {
        List<SOW__c> projects = new List<SOW__c>();
        
        for(Integer i = 0 ; i < numberOfOpps ; i++)
        {
             SOW__c sow= new SOW__c( Name = 'Test Project' + Math.random() );
             projects.add(sow);
        }
        
        return  projects;
    } 
    
    /* Resource Assignment Creation */
    public static List<Projects_ForceOptimized__c> createRAs(Integer numberOfOpps)
    {
        List<Projects_ForceOptimized__c> RAs = new List<Projects_ForceOptimized__c>();
        
        for(Integer i = 0 ; i < numberOfOpps ; i++)
        {
             Projects_ForceOptimized__c RA = new Projects_ForceOptimized__c(Resource__c = UserInfo.getUserId(),
                                                                            Role__c = 'Business Analyst',
                                                                            Project_Responsibilities__c = 'Test' );
             RAs.add(RA);
        }
        
        return  RAs;
    }
    
}