public with sharing class ganttChart {
    @AuraEnabled
    public static Map<String, Object> getChartData(
        String recordId,
        String startTime,
        String endTime,
        Integer slotSize,
        List<String> filterProjects,
        List<String> filterRoles,
        List<String> filterEmployeeDepartments,
        List<String> filterResources
    ) {
        Map<String, Object> data = new Map<String, Object>();
        List<String> fields = new List<String>{
            'Id',
            'Weekly_Hours_Allocation__c',
            'Gantt_Chart_Color__c',
            'FMZ_Resource__c',
            'FMZ_Resource__r.Name',
            'FMZ_Resource__r.Termination_Date__c',
            'FMZ_Project_Role__r.FMZ_Role__c',
            'FMZ_Project_Role__r.FMZ_Role__r.Name',
            'FMZ_Project_Role__r.FMZ_Project__c',
            'FMZ_Project_Role__r.FMZ_Project__r.Name',
            'FMZ_Project_Role__r.FMZ_Project__r.Stage__c',
            'Assignment_Start_Date__c',
            'Assignment_End_Date__c',
            'Status__c'
        };
        String query =
            'SELECT ' +
            String.join(fields, ',') +
            '  FROM FMZ_Resource_Assignment__c WHERE Assignment_Start_Date__c <= :endDate AND Assignment_End_Date__c >= :startDate AND FMZ_Project_Role__r.FMZ_Project__r.Display_for_Resource_Management__c = true AND FMZ_Project_Role__r.FMZ_Project__r.Stage__c != \'Cancelled\'';

        List<FMZ_Resource_Assignment__C> assignments = new List<FMZ_Resource_Assignment__c>();
        Map<String, Object> projectById = new Map<String, Object>();
        Map<String, Object> resourceById = new Map<String, Object>();
        Set<String> roles = new Set<String>();

        if (!filterProjects.isEmpty()) {
            query += ' AND FMZ_Project_Role__r.FMZ_Project__c IN :filterProjects';
        }
        if (!filterRoles.isEmpty()) {
            query += ' AND FMZ_Project_Role__r.FMZ_Role__r.Name IN :filterRoles';
        }
        if (!filterEmployeeDepartments.isEmpty()) {
            query += ' AND FMZ_Resource__r.Employee_Department__c IN :filterEmployeeDepartments';
        }
        if (!filterResources.isEmpty()) {
            query += ' AND FMZ_Resource__c IN :filterResources';
        }

        if (String.isNotEmpty(startTime) && String.isNotEmpty(endTime)) {
            Date startDate = DateTime.newInstance(Long.valueOf(startTime)).date();
            Date endDate = DateTime.newInstance(Long.valueOf(endTime)).date();
            Integer days = startDate.daysBetween(endDate) + 1;
            slotSize = Integer.valueOf(slotSize);
            Decimal slots = days / slotSize;

            // Overview View
            if (String.isEmpty(recordId)) {
                query += ' ORDER BY FMZ_Resource__r.Name, FMZ_Project_Role__r.FMZ_Project__r.Name NULLS FIRST, Assignment_Start_Date__c';
                System.debug(query);
                System.debug(startDate);
                System.debug(endDate);
                System.debug(slotSize);
                assignments = Database.query(query);

                // display all active resources
                for (FMZ_Resource_Assignment__c resource : assignments) {
                    resourceById.put(
                        resource.FMZ_Resource__c,
                        new Map<String, Object>{
                            'Id' => resource.FMZ_Resource__c,
                            'Name' => resource.FMZ_Resource__r.Name,
                            'Default_Role__c' => resource.FMZ_Project_Role__r.FMZ_Role__r.Name,
                            'allocationsByProject' => new Map<String, Object>()
                        }
                    );
                }
            } else {
                if (Id.valueOf(recordId).getSobjectType().getDescribe().getName().endsWith('Project__c')) {
                    data.put('projectId', recordId);
                }

                query += ' AND (FMZ_Project_Role__r.FMZ_Project__c = :recordId OR FMZ_Resource__c = :recordId)';
                query += ' ORDER BY FMZ_Resource__r.Name, FMZ_Project_Role__r.FMZ_Project__r.Name NULLS FIRST, Assignment_Start_Date__c';

                assignments = Database.query(query);

                // empty state on resource page
                if (
                    assignments.isEmpty() &&
                    Id.valueOf(recordId).getSobjectType().getDescribe().getName().endsWith('Contact')
                ) {
                    FMZ_Resource_Assignment__c assignment = [
                        SELECT Id, FMZ_Resource__r.Name, FMZ_Project_Role__r.FMZ_Role__c
                        FROM FMZ_Resource_Assignment__c
                        WHERE FMZ_Resource__c = :recordId
                    ];

                    resourceById.put(
                        assignment.FMZ_Resource__c,
                        new Map<String, Object>{
                            'Id' => assignment.FMZ_Resource__c,
                            'Name' => assignment.FMZ_Resource__r.Name,
                            'Default_Role__c' => assignment.FMZ_Project_Role__r.FMZ_Role__c,
                            'allocationsByProject' => new Map<String, Object>()
                        }
                    );
                }
            }

            // organize allocations by resource and project
            for (FMZ_Resource_Assignment__c assignment : assignments) {
                if (!resourceById.containsKey(assignment.FMZ_Resource__c)) {
                    resourceById.put(
                        assignment.FMZ_Resource__c,
                        new Map<String, Object>{
                            'Id' => assignment.FMZ_Resource__c,
                            'Name' => assignment.FMZ_Resource__r.Name,
                            'Default_Role__c' => assignment.FMZ_Project_Role__r.FMZ_Role__c,
                            'allocationsByProject' => new Map<String, Object>()
                        }
                    );
                }

                Map<String, Object> resource = (Map<String, Object>) resourceById.get(assignment.FMZ_Resource__c);
                Map<String, Object> allocationsByProject = (Map<String, Object>) resource.get('allocationsByProject');

                if (!allocationsByProject.containsKey(assignment.FMZ_Project_Role__r.FMZ_Project__c)) {
                    allocationsByProject.put(assignment.FMZ_Project_Role__r.FMZ_Project__c, new List<Object>());
                }

                projectById.put(
                    assignment.FMZ_Project_Role__r.FMZ_Project__c,
                    new Map<String, Object>{
                        'Id' => assignment.FMZ_Project_Role__r.FMZ_Project__c,
                        'Name' => assignment.FMZ_Project_Role__r.FMZ_Project__r.Name
                    }
                );

                List<Object> projectAllocation = (List<Object>) allocationsByProject.get(
                    assignment.FMZ_Project_Role__r.FMZ_Project__c
                );

                Decimal left = Decimal.valueOf(startDate.daysBetween(assignment.Assignment_Start_Date__c)) / slotSize;
                left = left.round(System.RoundingMode.FLOOR);

                Decimal right = Decimal.valueOf(startDate.daysBetween(assignment.Assignment_End_Date__c)) / slotSize;
                right = right.round(System.RoundingMode.FLOOR);

                projectAllocation.add(
                    new Map<String, Object>{
                        'Id' => assignment.Id,
                        'Start_Date__c' => assignment.Assignment_Start_Date__c,
                        'End_Date__c' => assignment.Assignment_End_Date__c,
                        'Status__c' => assignment.Status__c,
                        'Weekly_Hours_Allocation__c' => assignment.Weekly_Hours_Allocation__c,
                        'projectName' => assignment.FMZ_Project_Role__r.FMZ_Project__r.Name,
                        'color' => '#' + assignment.Gantt_Chart_Color__c,
                        'left' => left,
                        'right' => right
                    }
                );

                roles.add(assignment.FMZ_Project_Role__r.FMZ_Role__r.Name);
            }
        }

        List<String> employeeDepartments = new List<String>();
        for (Schema.PicklistEntry pickListVal : Contact.Employee_Department__c.getDescribe().getPicklistValues()) {
            employeeDepartments.add(pickListVal.getLabel());
        }

        List<Contact> filterableResources = getFilterableResources();
        List<Forecast> forecasts = getForecastForDateRange(startTime, endTime);

        data.put('projects', projectById.values());
        data.put('resources', resourceById.values());
        data.put('allResources', filterableResources);
        data.put('roles', roles);
        data.put('employeeDepartments', employeeDepartments);
        data.put('forecasts', forecasts);

        return data;
    }

    @AuraEnabled
    public static List<Object> getResources() {
        List<Object> resources = new List<Object>();

        for (FMZ_Resource_Assignment__c r : [
            SELECT Id, FMZ_Resource__r.Name, FMZ_Project_Role__r.FMZ_Role__r.Name
            FROM FMZ_Resource_Assignment__c
            WHERE FMZ_Resource__c != null
            ORDER BY Name
        ]) {
            resources.add(
                new Map<String, Object>{
                    'Id' => r.FMZ_Resource__c,
                    'Name' => r.FMZ_Resource__r.Name,
                    'Default_Role__c' => r.FMZ_Project_Role__r.FMZ_Role__r.Name
                }
            );
        }

        return resources;
    }

    @AuraEnabled
    public static List<FMZ_Project__c> getProjects() {
        return [
            SELECT Id, Name, (SELECT Id, Name FROM FMZ_Project_Roles__r)
            FROM FMZ_Project__c
            WHERE Stage__c != 'Cancelled' AND Display_for_Resource_Management__c = true
            ORDER BY Name
        ];
    }

    @AuraEnabled
    public static void saveAllocation(
        Id allocationId,
        Id projectId,
        Id roleId,
        Id resourceId,
        String hours,
        String status,
        String startDate,
        String endDate,
        List<FMZ_Resource_Forecast__c> forecasts
    ) {
        FMZ_Resource_Assignment__c assignment = new FMZ_Resource_Assignment__c(
            Assignment_Start_Date__c = DateTime.newInstance(Long.valueOf(startDate)).date(),
            Assignment_End_Date__c = Datetime.newInstance(Long.valueOf(endDate)).date()
        );

        // update allocation
        if (allocationId != null) {
            assignment.Id = allocationId;
        } else {
            if (projectId != null && roleId != null) {
                assignment.FMZ_Project_Role__c = roleId;
                assignment.FMZ_Project_Role__r = new FMZ_Project_Role__c(Id = roleId, FMZ_Project__c = projectId);
            }
            assignment.FMZ_Resource__c = resourceId;
        }

        if (String.isNotEmpty(status)) {
            assignment.Status__c = status;
        }

        if (!String.isBlank(hours)) {
            assignment.Weekly_Hours_Allocation__c = Decimal.valueOf(hours);
        }

        try {
            upsert assignment;
            if (forecasts != null) {
                update forecasts;
            }
        } catch (DMLException e) {
            throw new AuraHandledException(e.getDmlMessage(0));
        } catch (Exception e) {
            System.debug(e);
            throw new AuraHandledException('An error occurred saving the record');
        }
    }

    @AuraEnabled
    public static void deleteAllocation(Id allocationId) {
        delete new FMZ_Resource_Assignment__c(Id = allocationId);
    }

    @AuraEnabled
    public static List<FMZ_Resource_Forecast__c> getForecast(Id assignmentId) {
        if (String.isBlank(assignmentId)) {
            return new List<FMZ_Resource_Forecast__c>();
        }

        return [
            SELECT Id, Name, Forecast_Hours__c, Week_Start_Date__c
            FROM FMZ_Resource_Forecast__c
            WHERE FMZ_Resource_Assignment__c = :assignmentId
            ORDER BY Name
        ];
    }

    @AuraEnabled
    public static FMZ_Timesheet__c getTimesheetForDate(String dateWorked) {
        Date dateWorkedDate;
        if (String.isEmpty(dateWorked)) {
            dateWorkedDate = Date.today();
        } else {
            dateWorkedDate = DateTime.newInstance(Long.valueOf(dateWorked)).date();
        }

        List<FMZ_Timesheet__c> timesheets = [
            SELECT Id, Total_Hours__c, Week_Start_Date__c, Week_End_Date__c
            FROM FMZ_Timesheet__c
            WHERE Is_Me__c = true AND Week_Start_Date__c < :dateWorkedDate AND Week_End_Date__c <= :dateWorkedDate
        ];
        if (timesheets.isEmpty()) {
            throw new AuraHandledException('Date is out of bounds');
        }
        return timesheets[0];
    }

    @AuraEnabled
    public static List<Forecast> getForecastForDateRange(String startDate, String endDate) {
        if (String.isEmpty(startDate) || String.isEmpty(endDate)) {
            throw new AuraHandledException('Date range is invalid');
        }

        Date sd = DateTime.newInstance(Long.valueOf(startDate)).date();
        Date ed = DateTime.newInstance(Long.valueOf(endDate)).date();

        List<FMZ_Timesheet__c> timesheets = [
            SELECT
                Id,
                Resource__c,
                Resource_Util_Target__c,
                Week_Start_Date__c,
                (
                    SELECT Id, Forecast_Hours__c
                    FROM FMZ_Resource_Forecasts__r
                    WHERE FMZ_Resource_Assignment__r.Status__c = 'Confirmed'
                )
            FROM FMZ_Timesheet__c
            WHERE Week_Start_date__c >= :sd AND Week_Start_Date__c <= :ed
            ORDER BY Week_Start_Date__c
        ];

        if (timesheets.isEmpty()) {
            throw new AuraHandledException('Date is out of bounds');
        }

        List<Forecast> forecasts = new List<Forecast>();
        FMZ_Timesheet_Configuration__mdt config = getTimesheetConfiguration('Default');

        for (FMZ_Timesheet__c timesheet : timesheets) {
            Double sumHours = 0.0;
            if (!timesheet.FMZ_Resource_Forecasts__r.isEmpty()) {
                for (FMZ_Resource_Forecast__c f : timesheet.FMZ_Resource_Forecasts__r) {
                    sumHours += f.Forecast_Hours__c == null ? 0 : f.Forecast_Hours__c;
                }
            }

            Double target = timesheet.Resource_Util_Target__c;
            Double util = target == 0 || sumHours == 0 ? 0.0 : sumHours / target;

            Forecast forecastItem = new Forecast();
            if (util * 100 <= config.Low_To_Mid_Threshold__c) {
                forecastItem.background = '#' + config.No_Forecast_Color__c;
                forecastItem.foreground = '#' + config.No_Forecast_Text_Color__c;
            } else if (util * 100 <= config.Mid_to_High_Threshold__c) {
                forecastItem.background = '#' + config.Weak_Forecast_Color__c;
                forecastItem.foreground = '#' + config.Weak_Forecast_Text_Color__c;
            } else {
                forecastItem.background = '#' + config.Strong_Forecast_Color__c;
                forecastItem.foreground = '#' + config.Strong_Forecast_Text_Color__c;
            }

            forecastItem.hours = sumHours;
            forecastItem.target = target;
            forecastItem.resourceId = timesheet.Resource__c;
            forecastItem.startDate = timesheet.Week_Start_Date__c;
            forecasts.add(forecastItem);
        }

        return forecasts;
    }

    public static FMZ_Timesheet_Configuration__mdt getTimesheetConfiguration(String label) {
        return [
            SELECT
                Id,
                MasterLabel,
                Future_Weeks_to_Create_Timesheets__c,
                Time_Off_Color__c,
                Other_Projects_Color__c,
                Standard_Project_Color__c,
                No_Forecast_Color__c,
                No_Forecast_Text_Color__c,
                Weak_Forecast_Color__c,
                Weak_Forecast_Text_Color__c,
                Strong_Forecast_Color__c,
                Strong_Forecast_Text_Color__c,
                Low_To_Mid_Threshold__c,
                Mid_to_High_Threshold__c
            FROM FMZ_Timesheet_Configuration__mdt
            WHERE MasterLabel = :label
        ];
    }

    public static List<Contact> getFilterableResources() {
        return [SELECT Id, Name FROM Contact WHERE FO_Employment_Status__c = 'Active' ORDER BY Name ASC];
    }

    private Date getDateFromTimestamp(String s) {
        Long timestamp = Long.valueOf(s);
        return DateTime.newInstance(timestamp).date();
    }

    class Forecast {
        @AuraEnabled
        public Double hours { get; set; }
        @AuraEnabled
        public Double target { get; set; }
        @AuraEnabled
        public String background { get; set; }
        @AuraEnabled
        public String foreground { get; set; }
        @AuraEnabled
        public Double lowToMidThreshold { get; set; }
        @AuraEnabled
        public Double midToHighThreshold { get; set; }
        @AuraEnabled
        public Id resourceId { get; set; }
        @AuraEnabled
        public Date startDate { get; set; }
    }
}
