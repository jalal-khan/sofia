@isTest
public class MyForecast2ControllerTest {
	static list<Work_Week__c> weeks;
	static Account account;
	static Project__c project;
	static Project__c internalProject;
	static Work__c work;
	//static Work_Task__c workTask;
	static Project_Role__c projectRole;
	static Project_Role__c internalProjectRole;
	static Opportunity opportunity;
	static Opportunity closedOpportunity;
	static Project_Role__c opportunityRole;
	static Project_Role__c closedOpportunityRole;
	static List<Contact> usersToTestWith;

	static integer numWeeks = 3;
	static{
		weeks = new list<Work_Week__c>();
		date startDate = date.today().toStartOfWeek();
		for(integer i=0;i<numWeeks;i++){
			Work_Week__c ww = new Work_Week__c();
			ww.Start_Date__c = startDate;
			ww.End_Date__c = startDate.addDays(6);
			weeks.add(ww);
			startDate = startDate.addDays(7);
		}
		insert weeks;

		Account fortimize = new Account (Name = MyForecast2Controller.FORTIMIZE_ACCOUNT);
		insert fortimize;

		RecordType employee = [SELECT Id FROM RecordType WHERE DeveloperName = :MyForecast2Controller.EMPLOYEE_RECORD_TYPE AND SobjectType = 'Contact'];

		usersToTestWith = new List<Contact> {
			new Contact ( FirstName = 'User1', LastName = 'Test1', RecordTypeId = employee.Id, AccountId = fortimize.Id, FO_Employment_Status__c = 'Active' ),
			new Contact ( FirstName = 'User2', LastName = 'Test2', RecordTypeId = employee.Id, AccountId = fortimize.Id, FO_Employment_Status__c = 'Active'),
			new Contact ( FirstName = 'User3', LastName = 'Test3', RecordTypeId = employee.Id, AccountId = fortimize.Id, FO_Employment_Status__c = 'Active'),
			new Contact ( FirstName = 'User4', LastName = 'Test4', RecordTypeId = employee.Id, AccountId = fortimize.Id, FO_Employment_Status__c = 'Active'),
			new Contact ( FirstName = 'User5', LastName = 'Test5', RecordTypeId = employee.Id, AccountId = fortimize.Id, FO_Employment_Status__c = 'Active',
				User_Link__c = UserInfo.getUserId())
		};
		insert usersToTestWith;

		account = new Account();
		account.name = 'test';
		insert account;

		project = new Project__c();
		project.name = 'Test project';
		project.account__c = account.Id;
		project.Project_Start_Date__c = Date.today().addDays(-100);
		project.Project_Close_Date__c = Date.today().addDays(100);
		insert project;

		// Create an internal project that is visible to everyone for PTO etc.
		internalProject = new Project__c();
		internalProject.name = 'Test Internal Project';
		internalProject.account__c = account.Id;
		internalProject.Project_Start_Date__c = Date.today().addDays(-100);
		internalProject.Project_Close_Date__c = Date.today().addDays(100);
		internalProject.Public_Forecast_Visibility__c=true;
		insert internalProject;

		// Create an opportunity with a role
		opportunity = new Opportunity();
		opportunity.Name = 'Test Open Opportunity';
		opportunity.CloseDate = Date.today().addDays(30);
		opportunity.StageName='Prospect/ Qualifying';
		insert opportunity;

		// Create an opportunity with a role
		closedOpportunity = new Opportunity();
		closedOpportunity.Name = 'Test Closed Opportunity';
		closedOpportunity.CloseDate = Date.today().addDays(30);
		closedOpportunity.StageName='Closed Lost';
		// Set the flag that makes the Opp not show up
		closedOpportunity.Hide_Opportunity_Forecasting__c=true;
		insert closedOpportunity;

		work = new Work__c();
		work.Fortimize_Project__c = project.Id;
		insert work;

		projectRole = new Project_Role__c();
		projectRole.Project__c=project.Id;
		projectRole.Hours_Needed__c = 100;
		projectRole.Resource_Contact__c = usersToTestWith[4].Id;
		projectRole.Role__c = 'Senior Developer (SD)';
		projectRole.Hour_Type__c = 'Billable';
		insert projectRole;

		opportunityRole = new Project_Role__c();
		opportunityRole.Opportunity__c=opportunity.Id;
		opportunityRole.Hours_Needed__c = 100;
		opportunityRole.Resource_Contact__c = usersToTestWith[4].Id;
		opportunityRole.Role__c = 'Senior Developer (SD)';
		opportunityRole.Hour_Type__c = 'Billable';
		insert opportunityRole;

		closedOpportunityRole = new Project_Role__c();
		closedOpportunityRole.Opportunity__c=closedOpportunity.Id;
		closedOpportunityRole.Hours_Needed__c = 100;
		closedOpportunityRole.Resource_Contact__c = usersToTestWith[4].Id;
		closedOpportunityRole.Role__c = 'Senior Developer (SD)';
		closedOpportunityRole.Hour_Type__c = 'Billable';
		insert closedOpportunityRole;

		internalProjectRole = new Project_Role__c();
		internalProjectRole.Project__c = internalProject.Id;
		internalProjectRole.Hours_Needed__c = 100;
		internalProjectRole.Resource_Contact__c = usersToTestWith[4].Id;
		internalProjectRole.Role__c = 'Senior Developer (SD)';
		internalProjectRole.Hour_Type__c = 'Billable';
		insert internalProjectRole;

	}

	@isTest
	public static void initializeController(){
		Forecast__c f = new Forecast__c();
		f.Work_Week__c = weeks[0].Id;
		f.Hours__c = 4;
		f.Project_Role__c = projectRole.id;
		insert f;

		MyForecast2Controller fc = new MyForecast2Controller();

		fc.setManagerView();
		system.assertEquals(true, fc.managerView);

		fc.unsetManagerView();
		system.assertEquals(false, fc.managerView);

		fc.nextView();
		system.assertEquals(Date.today().toStartOfWeek().addDays(myForecastController.NUMBER_OF_WEEKS_TO_SHOW * 7),fc.startDate);

		fc.previousView();
		system.assertEquals(Date.today().toStartOfWeek(),fc.startDate);

	}

	@isTest
	public static void addTaskLineTest(){
		// Need to use the Internal Project Role for this test as it is not run as an admin
		MyForecast2Controller fc = new MyForecast2Controller();
		system.assertEquals(4,fc.tasksAvailableForView.size());

		fc.newTaskId = internalProjectRole.id;

		fc.addWorkTaskLine();
		system.assertEquals(3,fc.tasksAvailableForView.size());

		system.assertEquals(1,fc.tasks.size());

		fc.tasks[0].forecastMap.get(Date.today().toStartOfWeek()).Hours__c = 8;
		fc.setHours();

		fc.deleteRowId = internalProjectRole.id;
		fc.deleteRowFromCurrentView();
		system.assertEquals(0,fc.tasks.size());
		system.assertEquals(4,fc.tasksAvailableForView.size());

	}

	@isTest
	public static void testGetAllProjects() {
		PageReference pr = Page.MyForecast;
		Test.setCurrentPage(pr);

		Test.startTest();
		MyForecast2Controller fc = new MyForecast2Controller();
		Test.stopTest();
		System.assert(fc.allProjects.size() > 0, 'We should have found at least one project');
	}

	@isTest
	public static void testGetAllOpportunities() {
		PageReference pr = Page.MyForecast;
		Test.setCurrentPage(pr);

		Test.startTest();
		MyForecast2Controller fc = new MyForecast2Controller();
		Test.stopTest();
		System.Debug('RCH Here:'+[Select Id, Name, CloseDate, StageName, ForecastCategoryName from Opportunity]);
		System.Debug('RCH Here2:'+[SELECT Opportunity__c, Opportunity__r.Name, Opportunity__r.ForecastCategoryName FROM Project_Role__c WHERE Opportunity__c != NULL AND Project__c = NULL]);
		System.assertEquals(1, fc.allOpportunities.size(), 'We should have exactly 1 opportunity, the closed lost opportunity should be ignored');
	}

	@isTest
	public static void testWithProjectId(){
		PageReference pr = Page.MyForecast;
		pr.getParameters().put('projectId',project.Id);
		Test.setCurrentPage(pr);

		Test.startTest();
		MyForecast2Controller fc = new MyForecast2Controller();
		Test.stopTest();
		System.assertEquals(fc.projectId, project.Id, 'Make sure the project Id is set');
		System.assertEquals(fc.viewBy, 'Project', 'Make sure the view By is set properly');
	}

	@isTest
	public static void testWithOpportunityId(){
		PageReference pr = Page.MyForecast;
		pr.getParameters().put('opportunityId',opportunity.Id);
		Test.setCurrentPage(pr);

		MyForecast2Controller fc = new MyForecast2Controller();
		System.assertEquals(fc.opportunityId, opportunity.Id, 'Make sure the opportunity Id is set');
		System.assertEquals(fc.viewBy, 'Opportunity', 'Make sure the view By is set properly');
	}

	@isTest
	public static void testViewByOptions(){
		PageReference pr = Page.MyForecast;
		Test.setCurrentPage(pr);

		MyForecast2Controller fc = new MyForecast2Controller();
		System.assertEquals(3, fc.viewByOptions.size(), 'Check the View By Options');
	}

	@isTest
	public static void testChangeViewBy(){
		PageReference pr = Page.MyForecast;
		Test.setCurrentPage(pr);

		MyForecast2Controller fc = new MyForecast2Controller();
		fc.viewBy = 'Project';
		fc.changeViewBy();
		System.assertNotEquals(null, fc.projectId, 'Make sure the project Id was set');
		fc.viewBy = 'Opportunity';
		fc.changeViewBy();
		System.assertNotEquals(null, fc.opportunityId, 'Make sure the opportunity Id was set');
		fc.viewBy = 'Person';
		fc.changeViewBy();
		System.assertNotEquals(null, fc.runningAsUser, 'Make sure the running as user was set');
	}

	//@isTest
	public static void testOverForecasting(){
		MyForecast2Controller fc = new MyForecast2Controller();
		fc.newTaskId = projectRole.id;

		fc.addWorkTaskLine();
		system.assertEquals(0,fc.tasksAvailableForView.size());

		system.assertEquals(1,fc.tasks.size());

		fc.tasks[0].forecastMap.get(Date.today().toStartOfWeek()).Hours__c = 500;
		fc.setHours();
		system.assertEquals(true,ApexPages.hasMessages());
		system.assertEquals(true,ApexPages.getMessages()[0].getDetail().containsIgnoreCase(Label.Over_Forecasted_Error));
	}
}