@isTest
public class InvoicingExt_Tests {

    static list<Work_Week__c> weeks;
    static Account account;
    static integer numWeeks = 3;
    static{
        weeks = new list<Work_Week__c>();
        date startDate = date.today().toStartOfWeek();
        for(integer i=0;i<numWeeks;i++){
            Work_Week__c ww = new Work_Week__c();
            ww.Start_Date__c = startDate;
            ww.End_Date__c = startDate.addDays(6);
            weeks.add(ww);
            startDate = startDate.addDays(7);
        }
        // make one for last week.
        Work_Week__c wwPrev = new Work_Week__c();
        Date oldDate = Date.today().addDays(-7).toStartOfWeek();
        wwPrev.Start_Date__c = oldDate;
        wwPrev.End_Date__c = oldDate.addDays(6);
        weeks.add(wwPrev);
        insert weeks;
        
        account = new Account();
        account.name = 'test';
        insert account;
        
        // make 5 projects.
        list<Project__c> projects = new list<Project__c>();
        integer i = 0;
        while(i<5){
            Project__c project = new Project__c();
            project.name = 'Test project ' + i;
            project.account__c = account.Id;
            project.Project_Manager__c = UserInfo.getUserId();
            projects.add(project);
            i+=1;
        }
        insert projects;
        
        list<Work__c> works = new list<Work__c>();
        for(Project__c p:projects){
            Work__c work = new Work__c();
            work.Fortimize_Project__c = p.Id;
            work.Hour_Type__c = 'Billable';
            works.add(work);
        }
        insert works;
        
        list<Work_Task__c> workTasks = new list<Work_Task__c>();
        
        for(Work__c w:works){
            Work_Task__c workTask = new Work_Task__c();
            workTask.Allocated_Hours__c = 100;
            workTask.Assigned_to__c = UserInfo.getUserId();
            workTask.Task_Type__c = 'Other';
            workTask.Work__c = w.Id;
            workTask.Bill_Rate__c = 100;
            workTask.Task_Subject__c = 'Test Subject';
            workTasks.add(workTask);
        }
        insert workTasks;
        
        // make a timesheet task that's not been submitted
        Timesheet__c ts = new Timesheet__c();
        ts.Work_Week__c = [SELECT Id FROM Work_Week__c WHERE Start_Date__c = :Date.today().toStartOfWeek() LIMIT 1][0].Id;
        ts.Start_Date__c = Date.today().toStartOfWeek();
        ts.OwnerId = UserInfo.getUserId();
        insert ts;
        
        list<Timesheet_Task__c> tasks = new list<Timesheet_Task__c>();
        
        for(Work_Task__c wt: [SELECT Id FROM Work_Task__c]){
            Timesheet_Task__c tst = new Timesheet_Task__c();
            tst.Timesheet__c = ts.Id;
            tst.Work_Task__c = wt.Id;
            tst.Status__c = WorkTaskLine.STATUS_APPROVED;
            tst.Hours_Used__c = 4;
            tasks.add(tst);
        }
        insert tasks;
        
        list<Time_Log__c> logs = new list<Time_Log__c>();

        for(Timesheet_Task__c tst:tasks){
            Time_Log__c tl = new Time_Log__c();
            tl.Timesheet_Task__c = tst.Id;
            tl.External_Comments__c = 'Test';
            tl.Hours_Completed__c = 4;
            tl.Work_Task__c = tst.Work_Task__c;
            logs.add(tl);
        }
        insert logs;
        
        // make expense reports.
        list<Expense_Report__c> ers = new list<Expense_Report__c>();
        for(Project__c p:projects){
            Expense_Report__c er = new Expense_Report__c();
            er.Customer_Billable__c = 'Yes';
            er.Status__c = 'Approved';
            er.Fortimize_Project__c = p.Id;
            er.Pay_Period_Ending__c = Date.today().addMonths(1).toStartOfMonth().addDays(-1);
            er.Name = string.valueOf(er.Pay_Period_Ending__c);
            er.Customer_Billing_Status__c = 'Open';
            ers.add(er);
        }
        insert ers;
        
        list<Expense_Detail__c> details = new list<Expense_Detail__c>();
        for(Expense_Report__c er:ers){
            Expense_Detail__c d = new Expense_Detail__c();
            d.Amount__c = 100.00;
            d.RecordTypeId = Schema.SObjectType.Expense_Detail__c.getRecordTypeInfosByName().get('Other').getRecordTypeId();
            d.Expense_Report__c = er.Id;
            d.Date__c = Date.today();
            details.add(d);
        }
        insert details;
    }
    
    public static string INVOICE_RT_TYPE_EXPENSE = 'Travel/Other';
    public static string INVOICETYPE_EXPENSE = 'Travel/Expense';
    public static string INVOICETYPE_PROJECT = 'Project';
    public static string STATUS_PROJECTED = 'Projected';
    
    @isTest
    static void loadProjectInvoice(){
        Client_Invoice__c inv = new Client_Invoice__c();
        
        Project__c pr = [SELECT id, Name FROM Project__c LIMIT 1];
        
        inv.Project__c = pr.Id;
        //inv.RecordTypeId = Schema.SObjectType.Client_Invoice__c.getRecordTypeInfosByName().get(INVOICETYPE_PROJECT).getRecordTypeId();
        inv.Invoice_Type__c = INVOICETYPE_PROJECT;
        inv.Payment_Status__c = STATUS_PROJECTED;
        inv.External_Comments__c = 'Test';
        inv.Projected_Percent__c = 100;
        inv.Projected_Invoice_Date__c = Date.today().addDays(7);
        insert inv;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(inv);
        InvoicingExt ext = new InvoicingExt(sc);
        
        system.assertEquals(true, ext.canEdit);
        system.assertEquals(1, ext.openLines.size());
        system.assertEquals(100*4, ext.openAmount);
    }
    
    @isTest
    static void loadExpenseInvoice(){
        Client_Invoice__c inv = new Client_Invoice__c();
        
        Project__c pr = [SELECT id, Name FROM Project__c LIMIT 1];
        
        inv.Project__c = pr.Id;
        //inv.RecordTypeId = Schema.SObjectType.Client_Invoice__c.getRecordTypeInfosByName().get(INVOICE_RT_TYPE_EXPENSE).getRecordTypeId();
        inv.Invoice_Type__c = INVOICETYPE_EXPENSE;
        inv.Payment_Status__c = STATUS_PROJECTED;
        inv.External_Comments__c = 'Test';
        inv.Projected_Percent__c = 100;
        inv.Projected_Invoice_Date__c = Date.today().addDays(7);
        insert inv;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(inv);
        InvoicingExt ext = new InvoicingExt(sc);
        
        system.assertEquals(true, ext.canEdit);
        system.assertEquals(1, ext.openExpenseLines.size());
        
        system.assertEquals(100, ext.openAmount);
    }
    
    @isTest
    static void testAddRemoveProjectTime(){
        Client_Invoice__c inv = new Client_Invoice__c();
        
        Project__c pr = [SELECT id, Name FROM Project__c LIMIT 1];
        
        inv.Project__c = pr.Id;
        //inv.RecordTypeId = Schema.SObjectType.Client_Invoice__c.getRecordTypeInfosByName().get(INVOICETYPE_PROJECT).getRecordTypeId();
        inv.Invoice_Type__c = INVOICETYPE_PROJECT;
        inv.Payment_Status__c = STATUS_PROJECTED;
        inv.External_Comments__c = 'Test';
        inv.Projected_Percent__c = 100;
        inv.Projected_Invoice_Date__c = Date.today().addDays(7);
        insert inv;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(inv);
        InvoicingExt ext = new InvoicingExt(sc);
        
        system.assertEquals(true, ext.canEdit);
        system.assertEquals(1, ext.openLines.size());
        system.assertEquals(100*4, ext.openAmount);
        
        // simulate adding the work task to the invoice.
        ext.openLines[0].isSelected = true;
        Id ttId = ext.openLines[0].timesheetTaskId;
        
        ext.updateInvoice();
        
        system.assertEquals(0, ext.openLines.size());
        system.assertEquals(1, ext.invoicedLines.size());
        system.assertEquals(ttId, ext.invoicedLines[0].timesheetTaskId);
        
        ext.removeTaskId = ttId;
        ext.removeTaskLine();
        
        system.assertEquals(true, ext.canEdit);
        system.assertEquals(1, ext.openLines.size());
        system.assertEquals(100*4, ext.openAmount);
    }
    
    @isTest
    static void testAddRemoveExpenses(){
        Client_Invoice__c inv = new Client_Invoice__c();
        
        Project__c pr = [SELECT id, Name FROM Project__c LIMIT 1];
        
        inv.Project__c = pr.Id;
        //inv.RecordTypeId = Schema.SObjectType.Client_Invoice__c.getRecordTypeInfosByName().get(INVOICE_RT_TYPE_EXPENSE).getRecordTypeId();
        inv.Invoice_Type__c = INVOICETYPE_EXPENSE;
        inv.Payment_Status__c = STATUS_PROJECTED;
        inv.External_Comments__c = 'Test';
        inv.Projected_Percent__c = 100;
        inv.Projected_Invoice_Date__c = Date.today().addDays(7);
        insert inv;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(inv);
        InvoicingExt ext = new InvoicingExt(sc);
        
        system.assertEquals(true, ext.canEdit);
        system.assertEquals(1, ext.openExpenseLines.size());
        
        system.assertEquals(100, ext.openAmount);
        
        // simulate adding the expense report to the invoice.
        ext.openExpenseLines[0].isSelected = true;
        Id erId = ext.openExpenseLines[0].expenseReport.Id;
        ext.updateInvoice();
        
        system.assertEquals(0, ext.openExpenseLines.size());
        system.assertEquals(1, ext.invoicedExpenseLines.size());
        system.assertEquals(erId, ext.invoicedExpenseLines[0].expenseReport.Id);
        
        ext.removeExpenseId = erId;
        ext.removeExpenseLine();
        
        system.assertEquals(true, ext.canEdit);
        system.assertEquals(1, ext.openExpenseLines.size());
        system.assertEquals(100, ext.openAmount);
    }
}