global class TimesheetMaintenanceBatch implements Database.Batchable<sObject>, Schedulable {

    global Database.QueryLocator start(Database.BatchableContext bc){
        System.debug('starting batch');
        return Database.getQueryLocator(
            'SELECT Id FROM Contact WHERE Contact.RecordType.DeveloperName = \'Employee\' AND FO_Employment_Status__c = \'Active\' AND Start_Date__c != NULL');
    }

    global void execute(Database.BatchableContext bc, List<Contact> contacts) {
        System.debug('executing batch ' + JSON.serialize(contacts));
        TimesheetService.createFutureTimesheets(new Map<Id,Contact>(contacts).keySet());
    }

    global void finish(Database.BatchableContext bc){
    }

    public static String CRON_EXP = '0 0 6 ? * 1 *';
   
    global void execute(SchedulableContext ctx) {
        System.debug('executing schedule');
        Database.executeBatch(new TimesheetMaintenanceBatch());
    }
}