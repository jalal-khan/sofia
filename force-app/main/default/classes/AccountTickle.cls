// ==================================================================================
//  Class Name : Account Tickle 
//  Company : Force Optimized
//  Author :  Jan Matteson
// 	Description: Scheduled tickle for Account record 
// ==================================================================================
//  Changes: 2014-04-01 Initial version.
// ==================================================================================
global class AccountTickle implements Schedulable{
    //mandatory funciton called by the Apex Scheduler
    global void execute(SchedulableContext SC){
        tickleAccountRecords();
        }//end execute()
                        
//get all accounts named Force Optimized LLC
     public void tickleAccountRecords(){
         List<Account> AcctList = new List<Account>();
         
         for(Account a : [SELECT Id, Name, Period_End_Tickle__c FROM Account WHERE Name = : 'Force Optimized LLC'])
         {
             a.Period_End_Tickle__c = true;
             AcctList.add(a);
         }
          if (AcctList.size()>0)
              update AcctList;
     }
                       
}