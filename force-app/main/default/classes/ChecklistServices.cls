public class ChecklistServices {

	public static final string STATUS_COMPLETE = 'Complete';
	public static final string STATUS_NEEDED = 'Needed';
	public static final string STATUS_NOT_NEEDED = 'Not Needed';
	public static final string TASK_STATUS_COMPLETE = 'Completed';

	public static boolean isRunning {get; set;}
	static {
		isRunning = false;
	}


	// ******************************************************** //
	// ****      Checklist_Entry__c Trigger Handlers       **** //
	// ******************************************************** //

	// when new Checklist Entries are created, if Status is Needed and the Deal has the required Role, create a task
	public static void createTasks(list<Checklist_Entry__c> newList) {
		system.debug('### CALLED: createTasks(ChecklistEntryTrigger)');
		set<id> dealIds = new set<id>();
		Map<Id, Id> dealIdByChecklistId = getDealIdByChecklistId(newList);
		System.debug('### dealIdByChecklistId: '+dealIdByChecklistId);
		list<ChecklistEntry> entriesNeedingTask = new list<ChecklistEntry>();
		for (Checklist_Entry__c cle : newList) {
			if (cle.Status__c == STATUS_NEEDED) {
				dealIds.add(dealIdByChecklistId.get(cle.Checklist__c));
				entriesNeedingTask.add(new ChecklistEntry(cle));
			}
		}
		System.debug('### entriesNeedingTask: '+entriesNeedingTask);
		if (entriesNeedingTask.size() == 0) return;
		map<id, Deal> dealMap = getDealMap(dealIds);
		System.debug('### dealMap: '+dealMap);
		list<Task> tasksForInsert = getTasksForInsert(entriesNeedingTask, dealMap);
		System.debug('### tasksForInsert: '+tasksForInsert);
		insert tasksForInsert;
		// update the taskId on the CLE
		for (ChecklistEntry cle : entriesNeedingTask) {
			cle.updateTaskId();
		}
	}

	// when Checklist Entries are updated,
	//   * if Status changes from Not Needed to Needed and the Deal has the required Role, create a task
	//   * if status changes from Needed to Not Needed and the CLE has a taskId, remove the task
	//   * if status is Needed and Role changes, reassign task or create if no prior task
	//   * if status is Needed and Date changes, update task
	public static void updateTasks(list<Checklist_Entry__c> newList, map<id, Checklist_Entry__c> oldMap) {
		system.debug('CALLED: updateTasks(ChecklistEntryTrigger)');
		isRunning = true;
		set<id> dealIds = new set<id>();
		Map<Id, Id> dealIdByChecklistId = getDealIdByChecklistId(newList);
		list<ChecklistEntry> entriesNeedingTask = new list<ChecklistEntry>();
		list<ChecklistEntry> entriesNeedingTaskUpdate = new list<ChecklistEntry>();
		set<id> taskIdsForDelete = new set<id>();
		set<id> taskIdsForUpdate = new set<id>();
		for (Checklist_Entry__c cle : newList) {
			Checklist_Entry__c oldCle = oldMap.get(cle.id);
			if (cle.Status__c == STATUS_NEEDED && oldCle.Status__c == STATUS_NOT_NEEDED) {
				dealIds.add(dealIdByChecklistId.get(cle.Checklist__c));
				entriesNeedingTask.add(new ChecklistEntry(cle));
			}
			else if (cle.status__c == STATUS_NOT_NEEDED && oldCle.status__c == STATUS_NEEDED && cle.task_id__c != null) {
				taskIdsForDelete.add(cle.task_id__c);
			}
			else if (cle.status__c == STATUS_NEEDED) {
				dealIds.add(dealIdByChecklistId.get(cle.Checklist__c));
				if (cle.task_id__c == null) {
					entriesNeedingTask.add(new ChecklistEntry(cle));
				}
				else {
					system.debug('TaskId: #' + cle.task_id__c + '#');
					entriesNeedingTaskUpdate.add(new ChecklistEntry(cle));
					taskIdsForUpdate.add(cle.task_id__c);
				}
			}

		}

		map<id, Deal> dealMap = getDealMap(dealIds);
		map<id, Task> taskMap = getTaskMap(taskIdsForUpdate);
		list<Task> tasksForInsert = getTasksForInsert(entriesNeedingTask, dealMap);
		list<Task> tasksForUpdate = getTasksForUpdate(entriesNeedingTaskUpdate, dealMap, taskMap);
		taskIdsForDelete.addAll(getTaskIdsForDelete(entriesNeedingTaskUpdate, dealMap, taskMap));
		Savepoint sp = Database.setSavepoint();

		try {
			insert tasksForInsert;
			update tasksForUpdate;
			delete [select id from Task where id in :taskIdsForDelete];
			for (ChecklistEntry cle : entriesNeedingTask) {
				cle.updateTaskId();
			}

		}
		catch (Exception e) {
			Database.rollback(sp);
			throw e;
		}

	}

	// When Checklist Entries are Removed, if the entry in not Completed, remove task.
	public static void removeTasks(list<Checklist_Entry__c> oldList) {
		system.debug('CALLED: removeTasks(ChecklistEntryTrigger)');
		list<id> taskIdsForDelete = new list<id>();
		for (Checklist_Entry__c cle : oldList) {
			if (cle.task_id__c != null) {
				try {
					id xId = cle.task_id__c;
					taskIdsForDelete.add(xId);
				}
				catch (Exception e) {
					system.debug('Could not convert task_id__c to valid id: ' + cle);
					// do nothing.
				}
			}
		}
		if (taskIdsForDelete.size() > 0) {
			delete [select id from task where id in :taskIdsForDelete];
		}
 	}


	// ******************************************************** //
	// ****         Checklist__c Trigger Handlers          **** //
	// ******************************************************** //

	// cascaded delete of child records won't fire child delete trigger, so manually invoke
	public static void deleteChecklistEntries(set<id> checklistIds) {
		system.debug('CALLED: deleteChecklistEntries(ChecklistTrigger)');
		delete [select id from Checklist_Entry__c where Checklist__c in :checklistIds];
	}


	// ******************************************************** //
	// ****             Task Trigger Handlers              **** //
	// ******************************************************** //

	// When a Task is updated,
	//   * if the notes have changed, and if ChecklistEntry.Template.SaveTaskNotes == True, copy Notes to CLE
	//   * if the status changes to or from Completed, update the CLE's status
	//   * if the date has change, update the CLE's Due Date
	public static void updateCompletedTasks(map<id, Task> newMap, map<id, Task> oldMap) {
		system.debug('CALLED: updateCompletedTasks(TaskTrigger)');
		if(isRunning) return; //avoid infinite recursion
		set<id> taskIds = new set<id>();
		for (Task newTask : newMap.values()) {
			Task oldTask = oldMap.get(newTask.id);
			if (newTask.isClosed == true && oldTask.isClosed == false
			 || newTask.Description != oldTask.Description
			 || newTask.ActivityDate != oldTask.ActivityDate) {
				taskIds.add(newTask.id);
			}
		}

		if (taskIds.size() == 0) return;

		list<Checklist_Entry__c> entriesNeedingUpdate = new list<Checklist_Entry__c>();
		for(Checklist_Entry__c cle : [
				select id, Status__c, Notes__c, Task_id__c, Due_Date__c, Checklist__c
				  from Checklist_Entry__c
				 where task_id__c in :taskIds
		]) {
			ChecklistEntry entry = new ChecklistEntry(cle);
			if (entry.updateFromTask(newMap.get(entry.taskId))) {
				entriesNeedingUpdate.add(entry.obj);
			}
		}
		update entriesNeedingUpdate;

	}


	// ******************************************************** //
	// **** Internal_Role__c Trigger Handlers **** //
	// ******************************************************** //

	// When a new InternalDealContactRole is inserted,
	//   * If any of Deal's Checklists' Entries need the new role (and don't have a taskId), create the task
	public static void createTasks(list<Internal_Role__c> newList) {
		system.debug('### CALLED: createTasks(InternalDealContactRoleTrigger)');
		set<id> dealIds = new set<id>();

		Map<String,Schema.SObjectField> fields = Internal_Role__c.sObjectType.getDescribe().fields.getMap();
		Schema.FieldSet fieldset = Internal_Role__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		List<DescribeFieldResult> dfrs = new List<DescribeFieldResult>();
		List<String> lookupFields = new List<String>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			DescribeFieldResult dfr = fields.get(fsm.getFieldPath()).getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
				lookupFields.add(dfr.getName());
			}
		}

		for (Internal_Role__c newDcr : newList) {
			for(String field : lookupFields) {
				if (String.isNotBlank((String) newDcr.get(field))) {
					string dId = (String) newDcr.get(field);
					dealIds.add(dId);
				}
			}
		}

		System.debug('### dealIds: '+dealIds);

		list<ChecklistEntry> entriesNeedingTask = new list<ChecklistEntry>();

		fields = Checklist__c.sObjectType.getDescribe().fields.getMap();
		fieldset = Checklist__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		dfrs = new List<DescribeFieldResult>();
		lookupFields = new List<String>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			DescribeFieldResult dfr = fields.get(fsm.getFieldPath()).getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
				lookupFields.add('Checklist__r.'+dfr.getName()+' IN: dealIds');
			}
		}

		String conditionString = String.join(lookupFields, ' OR ');

		String query = 'select id, Status__c, Notes__c, Task_id__c, Checklist__c, Due_Date__c, Internal_Role__c, Name, Specific_User__c ' +
				'from Checklist_Entry__c ' +
				'where ('+ conditionString +') ' +	// for any checklist on the deal
				'and Status__c = :STATUS_NEEDED	' +	// which is needed (and not complete)
				'and Due_Date__c != null ' +
				'and Task_id__c = null';
		System.debug('### CLE Query: '+query);
		List<Checklist_Entry__c> entries = (List<Checklist_Entry__c>) Database.query(query);
		System.debug('### entries: '+entries);
		for (Checklist_Entry__c cle : entries) {
			entriesNeedingTask.add(new ChecklistEntry(cle));
		}
		System.debug('### entriesNeedingTask: '+entriesNeedingTask);
		if (entriesNeedingTask.size() == 0) return;
		map<id, Deal> dealMap = getDealMap(dealIds);
		list<Task> tasksForInsert = getTasksForInsert(entriesNeedingTask, dealMap);
		System.debug('### tasksForInsert: '+tasksForInsert);
		insert tasksForInsert;
		// update the taskId on the CLE
		list<Checklist_Entry__c> entriesNeedingUpdate = new list<Checklist_Entry__c>();
		for (ChecklistEntry cle : entriesNeedingTask) {
			if (cle.updateTaskId()) {
				entriesNeedingUpdate.add(cle.obj);
			}
		}
		System.debug('### entriesNeedingUpdate'+entriesNeedingUpdate);
		update entriesNeedingUpdate;

	}

	// When an InternalDealContactRole is updated:
	//   * IF user has changed, need to check for (not-completed) tasks to be reassigned
	// Validation rule will disallow changes to Role field
	public static void updateTasks(list<Internal_Role__c> newList, map<id, Internal_Role__c> oldMap) {
		system.debug('CALLED: updateTasks(InternalDealContactRoleTrigger)');
		set<string> dealIds = new set<string>();
		map<id, map<string, id>> dealToUpdateRolesMap = new map<id, map<string, id>>();

		Map<String,Schema.SObjectField> fields = Internal_Role__c.sObjectType.getDescribe().fields.getMap();
		Schema.FieldSet fieldset = Internal_Role__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		List<DescribeFieldResult> dfrs = new List<DescribeFieldResult>();
		List<String> lookupFields = new List<String>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			DescribeFieldResult dfr = fields.get(fsm.getFieldPath()).getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
				lookupFields.add(dfr.getName());
			}
		}

		for (Internal_Role__c newDcr : newList) {
			for(String field : lookupFields) {
				if (String.isNotBlank((String) newDcr.get(field))) {
					Internal_Role__c oldDcr = oldMap.get(newDcr.id);
					if (newDcr.Internal_Team_Member__c != oldDcr.Internal_Team_Member__c) {
						string dId = (String) newDcr.get(field);
						dealIds.add(dId);
					}
					if (dealToUpdateRolesMap.containsKey((Id) newDcr.get(field)) == false) {
						dealToUpdateRolesMap.put((Id) newDcr.get(field), new map<string, id>());
					}
					dealToUpdateRolesMap.get((Id) newDcr.get(field)).put(newDcr.Role__c, newDcr.Internal_Team_Member__c);
				}
			}
		}

		//need to check for (not-completed) tasks to be reassigned
		list<ChecklistEntry> entriesNeedingTaskUpdate = new list<ChecklistEntry>();
		set<id> taskIds = new set<id>();

		fields = Checklist__c.sObjectType.getDescribe().fields.getMap();
		fieldset = Checklist__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		dfrs = new List<DescribeFieldResult>();
		lookupFields = new List<String>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			DescribeFieldResult dfr = fields.get(fsm.getFieldPath()).getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
				lookupFields.add('Checklist__r.'+dfr.getName()+' IN: dealIds');
			}
		}
		String conditionString = String.join(lookupFields, ' OR ');

		String query = 'select id, Status__c, Notes__c, Task_id__c, Checklist__c, Due_Date__c, Internal_Role__c, Name, Specific_User__c ' +
				'from Checklist_Entry__c ' +
				'where ('+ conditionString +') ' +	// for any checklist on the deal
				'and Status__c = :STATUS_NEEDED	' +	// which is needed (and not complete)
				'and Task_id__c != null';

		List<Checklist_Entry__c> entries = (List<Checklist_Entry__c>) Database.query(query);
		Map<Id, Id> dealIdByChecklistId = getDealIdByChecklistId(entries);
		for (Checklist_Entry__c cle : entries) {
			if (dealToUpdateRolesMap.get(dealIdByChecklistId.get(cle.Checklist__c)).containsKey(cle.Internal_Role__c)) {
				entriesNeedingTaskUpdate.add(new ChecklistEntry(cle));
				taskIds.add(cle.Task_id__c);
			}
		}
		if (entriesNeedingTaskUpdate.size() == 0) return;

		map<id, Task> taskMap = getTaskMap(taskIds);
		list<Task> tasksForUpdate = getTasksForUpdate(entriesNeedingTaskUpdate, dealToUpdateRolesMap, taskMap);
		update tasksForUpdate;
	}


	// ******************************************************** //
	// ****            Public Static Utilities             **** //
	// ******************************************************** //

	public static SObject getDealWithDates(id dealId) {

		Map<String, Schema.DescribeFieldResult> fieldMap = FMZ_SchemaUtils.getObjectFieldDescribes(dealId.getSObjectType());
		list<string> dateFields = new list<string>();
		for (Schema.DescribeFieldResult dfr : fieldMap.values()) {
			if (dfr.getType() == Schema.DisplayType.Date) {
				dateFields.add(dfr.getName());
			}
		}
		String dateString = String.join(dateFields, ',');
		if(String.isNotBlank(dateString)){
			dateString = ', '+dateString;
		}

		string query = 'select id ' + dateString + ' from '+ dealId.getSObjectType().getDescribe().getName() +' where id = :dealId';
		list<SObject> deals = (list<SObject>)Database.query(query);
		return deals[0];
	}


	// **** Wrapper Classes ****//

	// Wrapper class for a Checklist_Entry__c
	class ChecklistEntry {
		public Checklist_Entry__c obj {get; set;}
		private Task taskObj {get; set;}

		public id dealId {get; set;}
		public id taskId {
			get {
				id tId;
				try {
					tId = obj.task_id__c;
				}
				catch (Exception e) {
					system.debug('Could not convert task_id__c to valid id: ' + obj );
					// no other action needed; the prop will just return null
				}
				return tId;
			}
		}

		public ChecklistEntry(Checklist_Entry__c cle) {
			this.obj = cle;
			Map<Id, Id> dealIdByChecklistId = getDealIdByChecklistId(new List<Checklist_Entry__c>{cle});
			this.dealId = dealIdByChecklistId.get(cle.Checklist__c);

		}

		// get a task for the given checklist entry, if Deal has someone assigned to the appropriate role (and CLE has a due date); otherwise return null;
		public Task getTask(Deal d) {
			id userId;
			if (this.obj.Internal_Role__c == 'User') {
				userId = this.obj.Specific_User__c;
			}
			else {
				userId = d.roleToUserMap.get(this.obj.Internal_Role__c);
			}
			if (userId == null || this.obj.Due_Date__c == null) {
				return null;
			}
			this.taskObj = new Task(
				OwnerId = userId,
				Subject = this.obj.name,
				ActivityDate = this.obj.Due_Date__c,
				Description = this.obj.Notes__c,
				WhatId = d.id
			);
			return this.taskObj;
		}

		// update the task if needed, and return true if task needs update
		// note that we don't have a copy of the old version of the CLE; we can just compare to the task
		//   * if status is Needed and Role changes, reassign task
		//   * if status is Needed and Date changes, update task
		//   * if status is Needed and Notes change, update task
		public boolean updateTask(Task t, Deal d) {
			boolean changed = false;
			if (t.ActivityDate != this.obj.Due_Date__c) {
				t.ActivityDate = this.obj.Due_Date__c;
				changed = true;
			}
			if (t.Description != this.obj.Notes__c) {
				t.Description = this.obj.Notes__c;
				changed = true;
			}
			id userId;
			if (this.obj.Internal_Role__c == 'User') {
				userId = this.obj.Specific_User__c;
			}
			else {
				userId = d.roleToUserMap.get(this.obj.Internal_Role__c);
			}
			if (t.ownerId != userId) {
				if (userId == null) return false; // if the new role doesn't exist on the deal, must delete, so don't mark for update
				t.OwnerId = userId;
				changed = true;
			}
			return changed;
		}

		// the Internal_Role__c's user has changed; update from provided map
		public Task updateTaskUser(map<string, id> roleToUserMap, map<id, Task> taskMap) {
			system.debug('taskObj: ' + this.taskObj);
			system.debug('obj: ' + this.obj);
			system.debug('roleToUserMap: ' + roleToUserMap);
			Task t = taskMap.get(this.taskId);
			t.OwnerId = roleToUserMap.get(this.obj.Internal_Role__c);
			return t;
		}

		// if the new role doesn't exist on the deal, must delete existing task
		public boolean shouldDeleteTask(Task t, Deal d) {
			if (this.obj.Internal_Role__c == 'User') { // if "Specific User..." role is selected...
				return (this.obj.Specific_User__c == null);						// should delete if Specific_User__c is null
			}
			id userId = d.roleToUserMap.get(this.obj.Internal_Role__c);
			if ((t.ownerId != userId) && userId == null) {
				this.obj.task_id__c = null;
				return true;
			}
			return false;
		}

		// after new task has been inserted, store the task ID on the CLE
		public boolean updateTaskId() {
			if (this.taskObj != null && this.taskObj.id != null && this.obj.task_id__c != this.taskObj.id) {
				this.obj.task_id__c = this.taskObj.id;
				return true;
			}
			return false;
		}

		// update the Checklist_Entry__c obj from the task; return true if any updates made
		public boolean updateFromTask(Task t) {
			boolean changed = false;
			if (t.status == TASK_STATUS_COMPLETE && this.obj.Status__c == STATUS_NEEDED) {
				this.obj.Status__c = STATUS_COMPLETE;
				changed = true;
			}
			if (t.description != this.obj.notes__c) {
				this.obj.Notes__c = t.Description;
				changed = true;
			}
			if (t.ActivityDate != this.obj.Due_Date__c) {
				this.obj.Due_Date__c = t.ActivityDate;
				changed = true;
			}

			return changed;
		}

	}

	// Wrapper class for deal
	class Deal {
		public List<Internal_Role__c> internalRoles {get; set;}
		public map<string, id> roleToUserMap {get; set;}
		public id id {get; set;}

		public Deal(Id d, List<Internal_Role__c> irs) {
			this.Id = d;
			if(irs == null){
				this.internalRoles = new List<Internal_Role__c>();
			}else{
				this.internalRoles = irs;
			}
			this.roleToUserMap = new map<string, id>();
			if(this.internalRoles.size() > 0){
				for (Internal_Role__c dcr : this.internalRoles) {
					this.roleToUserMap.put(dcr.Role__c, dcr.Internal_Team_Member__c); // if a deal has >1 user assigned to a role, behavior is undefined (all bets are off)
				}
			}
		}

	}


	// ******************************************************** //
	// ****                 PRIVATE HELPERS                 ****//
	// ******************************************************** //
	private static map<id, Deal> getDealMap(set<id> dealIds) {
		map<id, Deal> res = new map<id, Deal>();
		Map<String,Schema.SObjectField> fields = Internal_Role__c.sObjectType.getDescribe().fields.getMap();
		Schema.FieldSet fieldset = Internal_Role__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		List<DescribeFieldResult> dfrs = new List<DescribeFieldResult>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			DescribeFieldResult dfr = fields.get(fsm.getFieldPath()).getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
			}
		}
		System.assert(dfrs.size() > 0); //This should always be the case
		List<String> lookupFields = new List<String>();
		List<String> lookupFieldSearch = new List<String>();
		for(DescribeFieldResult dfr : dfrs){
			lookupFields.add(dfr.getName());
			lookupFieldSearch.add(dfr.getName()+' IN: dealIds');
		}



		String conditionsString = String.join(lookupFieldSearch, ' OR ');
		String soql = 'SELECT Id, Internal_Team_Member__c, Object__c, Role__c, '+ String.join(lookupFields, ', ') +' FROM Internal_Role__c WHERE ' + conditionsString;
		List<Internal_Role__c> internalRoles = Database.query(soql);
		System.debug('### internalRoles: '+internalRoles);
		Map<String, List<Internal_Role__c>> irsByObject = (Map<String, List<Internal_Role__c>>) FMZ_SchemaUtils.groupListByField(internalRoles, FMZ_SchemaUtils.currentNamespacePrefix+'Object__c');
		System.debug('### irsByObject: '+irsByObject);
		Map<String, List<Internal_Role__c>> irsByDealId = new Map<String, List<Internal_Role__c>>();
		for(List<Internal_Role__c> irs : irsByObject.values()){
			for(String field : lookupFields){
				if(String.isNotBlank((String) irs[0].get(field))){
					irsByDealId.putAll((Map<String, List<Internal_Role__c>>) FMZ_SchemaUtils.groupListByField((List<Internal_Role__c>)irs, field));
				}
			}
		}
		System.debug('### irsByDealId: '+irsByDealId);
		for (String dealId : irsByDealId.keySet()) {
			List<Internal_Role__c> irs = irsByDealId.get(dealId);
			res.put((Id) dealId, new Deal((Id) dealId, irs));
		}
		return res;
	}


	public static Map<Id, Id> getDealIdByChecklistId(List<Checklist_Entry__c> entries) {
		Map<String,Schema.SObjectField> fields = Checklist__c.sObjectType.getDescribe().fields.getMap();
		Schema.FieldSet fieldset = Checklist__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		List<DescribeFieldResult> dfrs = new List<DescribeFieldResult>();
		List<String> lookupFields = new List<String>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			DescribeFieldResult dfr = fields.get(fsm.getFieldPath()).getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
				lookupFields.add(dfr.getName());
			}
		}

		Set<String> checklistIds = new Set<String>();
		for(Checklist_Entry__c cle : entries){
			checklistIds.add(cle.Checklist__c);
			System.debug('### Checklist__c: '+ cle.Checklist__c);
		}
		System.debug('### checklistIds'+checklistIds);
		String query = 'SELECT Id, '+ String.join(lookupFields, ', ') +' FROM Checklist__c WHERE Id IN: checklistIds';
		System.debug('### Query for CL: '+query);
		List<Checklist__c> checklists = (List<Checklist__c>)Database.query(query);
		System.debug('### Checklists: '+checklists);

		Map<Id, Id> dealIdByChecklistId = new Map<Id, Id>();
		if(checklists.size() > 0){
			for(Checklist__c cl : checklists){
				for(String field : lookupFields){
					if(String.isNotBlank((String) cl.get(field))){
						dealIdByChecklistId.put((Id) cl.get('Id'), (Id) cl.get(field));
					}
				}
			}
		}
		System.debug('### dealIdByChecklistId: '+dealIdByChecklistId);
		return dealIdByChecklistId;
	}

	private static map<id, Task> getTaskMap(set<id> taskIds) {
		map<id, Task> res = new map<id, Task>();
		for (Task t : [select id, OwnerId, ActivityDate, Status, Subject, Description, WhatId, isClosed from Task where id in :taskIds]) {
			res.put(t.id, t);
		}
		return res;
	}

	private static list<Task> getTasksForInsert(list<ChecklistEntry> entriesNeedingTask, map<id, Deal> dealMap) {
		list<Task> tasksForInsert = new list<Task>();
		for (ChecklistEntry cle : entriesNeedingTask) {
			Deal d = dealMap.get(cle.dealId);
			if (d != null) {
				Task t = cle.getTask(d);
				if (t != null) {
					tasksForInsert.add(t);
				}
			}
		}
		return tasksForInsert;

	}

	private static list<Task> getTasksForUpdate(list<ChecklistEntry> entriesNeedingTaskUpdate, map<id, Deal>dealMap, map<id, Task> taskMap) {
		list<Task> tasksForUpdate = new list<Task>();
		for (ChecklistEntry cle : entriesNeedingTaskUpdate) {
			Deal d = dealMap.get(cle.dealId);
			Task t = taskMap.get(cle.taskId);
			if (d != null && t != null) {
				if (cle.updateTask(t, d)) {
					tasksForUpdate.add(t);
				}
			}
		}
		return tasksForUpdate;
	}

	private static list<Task> getTasksForUpdate( list<ChecklistEntry>entries, map<id, map<string, id>> dealToUpdateRolesMap, map<id, Task> taskMap) {
		list<Task> tasksForUpdate = new list<Task>();
		for (ChecklistEntry cle : entries) {
			tasksForUpdate.add(cle.updateTaskUser(dealToUpdateRolesMap.get(cle.dealId), taskMap));
		}
		return tasksForUpdate;
	}

	private static set<id> getTaskIdsForDelete(list<ChecklistEntry> entriesNeedingTaskUpdate, map<id, Deal>dealMap, map<id, Task> taskMap) {
		set<id> taskIdsForDelete = new set<id>();
		for (ChecklistEntry cle : entriesNeedingTaskUpdate) {
			Deal d = dealMap.get(cle.dealId);
			Task t = taskMap.get(cle.taskId);
			if (d != null && t != null) {
				if (cle.shouldDeleteTask(t, d)) {
					taskIdsForDelete.add(t.id);
				}
			}
		}
		return taskIdsForDelete;
	}
}