public class FMZ_SchemaUtils {
	public static string currentNamespace = '';
	public static string currentNamespacePrefix = '';

	private static Map<String, Schema.SObjectType> typeMap;
	private static Map<Schema.SObjectType, Map<String, Schema.FieldSet>> sObjectFieldSetMap =
		new map<Schema.SObjectType, Map<String, Schema.FieldSet>>();
	private static Map<Schema.SObjectType, Schema.DescribeSObjectResult> sObjectDescribeMap =
		new map<Schema.SObjectType, Schema.DescribeSObjectResult>();
	private static Map<Schema.SObjectType, Map<String, Schema.DescribeFieldResult>> sObjectFieldMap =
		new Map<Schema.SObjectType, Map<String, Schema.DescribeFieldResult>>();

	static {
		currentNamespace = FMZ_SchemaUtils.class.getName().substringBefore('FMZ_SchemaUtils').removeEnd('.').toLowerCase();
		if (String.isNotBlank(currentNamespace)) {
			currentNamespacePrefix = currentNamespace + '__';
		}
	}

	// given an SObject name, get the Describe info for the object
	public static Schema.DescribeSObjectResult getObjectDescribe(string SObjectName) {
		Schema.SObjectType sObjType = getSObjectType(sObjectName);
		if (sObjType == null) return null;
		return getObjectDescribe(sObjType);
	}

	// given an SObjectType, get the Describe info for the object
	public static Schema.DescribeSObjectResult getObjectDescribe(Schema.SObjectType sObjType) {
		if (sObjectDescribeMap.containsKey(sObjType) == false) {
			sObjectDescribeMap.put(sObjType, sObjType.getDescribe());
		}
		return sObjectDescribeMap.get(sObjType);
	}

	// Returns a map of FieldName(string) -> Schema.DescribeFieldResult.
	// FieldName is derived useing DescribeFieldResult.getName, so fields in this class's namespace
	// will not have an NS prefix; fields in other (non-empty) Namespaces will have a prefix.
	public static Map<String, Schema.DescribeFieldResult> getObjectFieldDescribes(Schema.SObjectType sObjType) {
		if(sObjectFieldMap.containsKey(sObjType) == false) {
			Map<String, Schema.DescribeFieldResult> describeMap = new Map<String, Schema.DescribeFieldResult>();
			for (Schema.SObjectField fldToken : getObjectDescribe(sObjType).fields.getMap().values()) {
				Schema.DescribeFieldResult dfr = fldToken.getDescribe();
				describeMap.put(dfr.getName().toLowerCase(), dfr);
			}
			sObjectFieldMap.put(sObjType, describeMap);
		}
		return sObjectFieldMap.get(sObjType);
	}

	// given an SObject name as a string, return the Schema.SObjectType token for the sobject
	public static Schema.SObjectType getSObjectType(string sObjName) {
		return getSObjectType(sObjName, true);
	}

	public static Schema.SObjectType getSObjectType(string sObjName, boolean implyNamespace) {
		if (typeMap == null) {
			typeMap = Schema.getGlobalDescribe();
		}

		string preferredName = (implyNamespace ? currentNamespacePrefix + sObjName : sObjName).toLowerCase();
		if(typeMap.containsKey(preferredName)) {
			return typeMap.get(preferredName);
		}
		else if(implyNamespace) {
			return typeMap.get(sObjName.toLowerCase());
		}
		else {
			return null;
		}

		return typeMap.get(sObjName);
	}

	public static Map<String, List<SObject>> groupListByField(List<SObject> objects, String field){
		Map<String, List<SObject>> objsByField = new Map<String, List<SObject>>();
		Boolean fieldExists = false;
		if(objects != null && objects.size() > 0) {
			fieldExists = objects[0].getSObjectType().getDescribe().fields.getMap().get(field) != null;
		}
		if(fieldExists){
			for(SObject obj : objects){
				List<SObject> objs = objsByField.get((String) obj.get(field));
				if(objs == null){
					objs = new List<SObject>();
				}
				objs.add(obj);
				objsByField.put((String) obj.get(field), objs);
			}
		}
		return objsByField;
	}

	// convenience method to get a field from an sObject.  This is useful when your fieldset
	// contains related object fields.  For example, a fieldset on Case that contains
	// 'contact.name'.  If you need to iterate the fieldset pulling out values (for example,
	// constructing a param string) you can't use sObj.get(fieldpath) for fieldpath='contact.name'
	public static String getSObjectField(sObject sObj, String fieldpath) {
		String[] pathParts = fieldpath.split('[.]', 2);
		if (pathParts.size() == 1) return String.valueOf(sObj.get(fieldpath));
		return getSObjectField(sObj.getSObject(pathParts[0]), pathParts[1]);
	}

	// return a list of picklist Values (not Labels) in the order defined by the Picklist
	public static List<String> getPicklistValues(Schema.SObjectType sobject_type, String Field_name){
		List<String> lstPickvals=new List<String>();
		Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
		Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
		List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
		for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
  			lstPickvals.add(a.getValue());//add the value  to our final list
 		}
		return lstPickvals;
	}

	// return a map of PicklistValue->PicklistLabel
	public static map<string, string> getPicklistValueMap(Schema.SObjectType sobject_type, String Field_name){
		map<String, String> pickvals=new map<String, String>();
		Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
		Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
		List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
		for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
  			pickvals.put(a.getValue(), a.getLabel());//add the value  to our final map
 		}
		return pickvals;
	}

	public static Map<String,List<String>> getDependentOptions(String pObjName, String pControllingFieldName, String pDependentFieldName){
		Map<String,List<String>> objResults = new Map<String,List<String>>();
		//get the string to sobject global map
		Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
		if (!objGlobalMap.containsKey(pObjName)) {
			return objResults;
		}
		//get the type being dealt with
		Schema.SObjectType pType = objGlobalMap.get(pObjName);
		Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
		//verify field names
		if (!objFieldMap.containsKey(pControllingFieldName) || !objFieldMap.containsKey(pDependentFieldName)) {
			return objResults;
		}
		//get the control values
		List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(pControllingFieldName).getDescribe().getPicklistValues();
		//get the dependent values
		List<Schema.PicklistEntry> dep_ple = objFieldMap.get(pDependentFieldName).getDescribe().getPicklistValues();
		//iterate through the values and get the ones valid for the controlling field name
		Bitset objBitSet = new Bitset();
		//set up the results
		for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){
			//get the pointer to the entry
			Schema.PicklistEntry ctrl_entry = ctrl_ple[pControllingIndex];
			//get the label
			String pControllingLabel = ctrl_entry.getLabel();
			//create the entry with the label
			objResults.put(pControllingLabel,new List<String>());
		}
		//cater for null and empty
		objResults.put('',new List<String>());
		objResults.put(null,new List<String>());
		//check the dependent values
		for(Integer pDependentIndex=0; pDependentIndex<dep_ple.size(); pDependentIndex++){
			//get the pointer to the dependent index
			Schema.PicklistEntry dep_entry = dep_ple[pDependentIndex];
			//get the valid for
			String pEntryStructure = JSON.serialize(dep_entry);
			TPicklistEntry objDepPLE = (TPicklistEntry)JSON.deserialize(pEntryStructure, TPicklistEntry.class);
			//if valid for is empty, skip
			if (objDepPLE.validFor==null || objDepPLE.validFor==''){
				continue;
			}
			//iterate through the controlling values
			for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){
				if (objBitSet.testBit(objDepPLE.validFor,pControllingIndex)){
					//get the label
					String pControllingLabel = ctrl_ple[pControllingIndex].getLabel();
					objResults.get(pControllingLabel).add(objDepPLE.label);
				}
			}
		}
		return objResults;
	}

	public class Bitset{
		public Map<String,Integer> AlphaNumCharCodes {get;set;}
		public Map<String, Integer> Base64CharCodes { get; set; }
		public Bitset(){
			LoadCharCodes();
		}
		//Method loads the char codes
		private void LoadCharCodes(){
			AlphaNumCharCodes = new Map<String,Integer>{
					'A'=>65,'B'=>66,'C'=>67,'D'=>68,'E'=>69,'F'=>70,'G'=>71,'H'=>72,'I'=>73,'J'=>74,
					'K'=>75,'L'=>76,'M'=>77,'N'=>78,'O'=>79,'P'=>80,'Q'=>81,'R'=>82,'S'=>83,'T'=>84,
					'U'=>85,'V'=> 86,'W'=>87,'X'=>88,'Y'=>89,'Z'=>90
			};
			Base64CharCodes = new Map<String, Integer>();
			//lower case
			Set<String> pUpperCase = AlphaNumCharCodes.keySet();
			for(String pKey : pUpperCase){
				//the difference between upper case and lower case is 32
				AlphaNumCharCodes.put(pKey.toLowerCase(),AlphaNumCharCodes.get(pKey)+32);
				//Base 64 alpha starts from 0 (The ascii charcodes started from 65)
				Base64CharCodes.put(pKey,AlphaNumCharCodes.get(pKey) - 65);
				Base64CharCodes.put(pKey.toLowerCase(),AlphaNumCharCodes.get(pKey) - (65) + 26);
			}
			//numerics
			for (Integer i=0; i<=9; i++){
				AlphaNumCharCodes.put(string.valueOf(i),i+48);
				//base 64 numeric starts from 52
				Base64CharCodes.put(string.valueOf(i), i + 52);
			}
		}
		public Boolean testBit(String pValidFor,Integer n){
			//the list of bytes
			List<Integer> pBytes = new List<Integer>();
			//multiply by 6 since base 64 uses 6 bits
			Integer bytesBeingUsed = (pValidFor.length() * 6)/8;
			//will be used to hold the full decimal value
			Integer pFullValue = 0;
			//must be more than 1 byte
			if (bytesBeingUsed <= 1)
				return false;
			//calculate the target bit for comparison
			Integer bit = 7 - (Math.mod(n,8));
			//calculate the octet that has in the target bit
			Integer targetOctet = (bytesBeingUsed - 1) - (n >> bytesBeingUsed);
			//the number of bits to shift by until we find the bit to compare for true or false
			Integer shiftBits = (targetOctet * 8) + bit;
			//get the base64bytes
			for(Integer i=0;i<pValidFor.length();i++){
				//get current character value
				pBytes.Add((Base64CharCodes.get((pValidFor.Substring(i, i+1)))));
			}
			//calculate the full decimal value
			for (Integer i = 0; i < pBytes.size(); i++)
			{
				Integer pShiftAmount = (pBytes.size()-(i+1))*6;//used to shift by a factor 6 bits to get the value
				pFullValue = pFullValue + (pBytes[i] << (pShiftAmount));
			}
			//& is to set the same set of bits for testing
			//shift to the bit which will dictate true or false
			Integer tBitVal = ((Integer)(Math.Pow(2, shiftBits)) & pFullValue) >> shiftBits;
			return  tBitVal == 1;
		}
	}

	/*
    * @Summary: Entity to represent a json version of a picklist entry
    * so that the validFor property becomes exposed
	*/
	public class TPicklistEntry{
		public string active {get;set;}
		public string defaultValue {get;set;}
		public string label {get;set;}
		public string value {get;set;}
		public string validFor {get;set;}
		public TPicklistEntry(){

		}
	}
}