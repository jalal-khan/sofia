public with sharing class CreateProjectRolesController {
    
    @AuraEnabled
    public static Id createProjectRoles(Id opportunityId) {
        Opportunity opportunity = [SELECT Id, FMZ_Project__c FROM Opportunity WHERE Id = :opportunityId];
        if (opportunity.FMZ_Project__c == null) {
            throw new AuraException('You cannot create project roles on an opportunity with no project.');
        }      
        ProjectService.createProjectRoles(new Set<Id> { opportunityId });
        return opportunity.FMZ_Project__c;  
    }
}