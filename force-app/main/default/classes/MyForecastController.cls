public class MyForecastController {
    public Date startDate{get;set;}
    public static final integer NUMBER_OF_WEEKS_TO_SHOW = 6;
    public list<Date> startDates{get;set;}
    public list<WorkWeek> weeks{get;set;}
    public list<Id> userIds{get;set;}
    public list<SelectOption> tasksAvailableForView{get;set;}
    public map<Date,Decimal> summaryLine{get;set;}
    public Id projectId{get;set;}
    public Id opportunityId{get;set;}
    public string titleScope{get;set;}
    public decimal totalProjectForecast{get;set;}
    public boolean isForecastAdmin{get;set;}
    public string viewBy{get;set;} // person or project.
    private static String forecastOnlyProjectRole='Potential';

    public MyForecastController(){
        this.viewBy = 'Person';
        this.startDate = Date.today().toStartOfWeek();
        this.projectId = ApexPages.currentPage().getParameters().get('projectid');
        this.opportunityId = ApexPages.currentPage().getParameters().get('opportunityid');
        System.Debug('Project Id is:'+projectId);
        System.Debug('Opportunity Id is:'+opportunityId);
        if(projectId != null){
            this.viewBy = 'Project';
        } else if(opportunityId != null) {
            this.viewBy = 'Opportunity';
        }

        // determine whether the user can see the list of other users to choose from
        list<User> users = [SELECT Id, Manage_Forecasts__c FROM User WHERE Id = :UserInfo.getUserId()];
        isForecastAdmin = users[0].Manage_Forecasts__c;
        if(ApexPages.currentPage().getParameters().get('adminView')!=null){
            isForecastAdmin = true;
        }
        system.debug('initial start date is: ' + this.startDate);
        doSetup(this.startDate);

    }
    
    public boolean managerView{get;set;}
    public void setManagerView(){
        this.managerView = true;
        doSetup(this.startDate);
    }
    public void unsetManagerView(){
        this.managerView = false;
        doSetup(this.startDate);
    }
    public void previousView(){
        this.startDate = this.startDate.addDays(-NUMBER_OF_WEEKS_TO_SHOW * 7);
        system.debug('initial start date is: ' + this.startDate);
        doSetup(this.startDate);
    }
    
    public void nextView(){
        this.startDate = this.startDate.addDays(NUMBER_OF_WEEKS_TO_SHOW * 7);
        system.debug('initial start date is: ' + this.startDate);
        doSetup(this.startDate);
    }
    
    public void doSetup(Date startDate){
        Boolean canEdit = false;
        startDates = new list<Date>();
        Date addDate = this.startDate;
        while(startDates.size() < 6){
            startDates.add(addDate);
            addDate = addDate.addDays(7);
        }
        startDates.sort();
        
        system.debug('in doSetup, runningAsUser = : ' + this.runningAsUser);
        
        this.weeks = getWeeks(startDates);
        System.debug('weeks: ' + this.weeks);
        if(this.viewBy == 'Project'){
            //Make sure Opporutnity Id is null so it doesn't get used
            this.opportunityId=null;
            if(this.projectId == null && allProjects.size() > 0){
                // populate the project Id.
                this.projectId = allProjects[0].getValue();
                this.titleScope = allProjects[0].getLabel();
                this.userIds = getAllUsers();
            }else if(this.projectId != null) {
                // Read the project so we can set the name
                List<Project__c> projects = [Select Id, Name from Project__c where Id=:this.projectId];
                if(projects.size() > 0) {
                    this.titleScope = projects.get(0).Name;
                }
            }
            if(isForecastAdmin) canEdit=true;
        }else if(this.viewBy == 'Opportunity') {
            //Make sure Project Id is null so it doesn't get used
            this.projectId=null;
            if(this.opportunityId == null && allOpportunities.size() > 0){
                // populate the opporutnity Id.
                this.opportunityId = allOpportunities[0].getValue();
                this.titleScope = allOpportunities[0].getLabel();
                this.userIds = getAllUsers();
            }else if(this.opportunityId != null) {
                // Read the project so we can set the name
                List<Opportunity> opps = [Select Id, Name from Opportunity where Id=:this.opportunityId];
                if(opps.size() > 0) {
                    this.titleScope = opps.get(0).Name;
                }
            }
            if(isForecastAdmin) canEdit=true;
        } else if(this.managerView != null && this.managerView){
            this.titleScope = 'All Fortimize ';
            if(isForecastAdmin) canEdit=true;
        }else if(this.runningAsUser != null && this.runningAsUser != UserInfo.getUserId()){//
            this.userIds = new list<Id>{runningAsUser};
                list<User> users = [SELECT FirstName, LastName FROM User WHERE Id = :runningAsUser];
            this.titleScope = users[0].FirstName + ' ' + users[0].LastName + ' ';
            if(isForecastAdmin) canEdit=true;
        }else {
            this.runningAsUser = UserInfo.getUserId();
            this.userIds = new list<Id>{this.runningAsUser};
            this.titleScope = 'My ';
            if(isForecastAdmin) canEdit=true;
        }
        this.tasks = populateTasks(weeks,userIds, canEdit);
        this.tasksAvailableForView =  getPossibleTasks(this.tasks);
        system.debug('tasksAvailableForView: ' + tasksAvailableForView);
        this.summaryLine = makeSummaryLine();
        
        if(this.projectId != null){
            setViewTotal();
        }
    }
    
    public Id runningAsUser{get;set;}
    
    public void changePerspective(){
        doSetup(Date.today().toStartOfWeek());
    }
    
    public void changeProject(){
        doSetup(Date.today().toStartOfWeek());
    }

    public void changeOpportunity(){
        doSetup(Date.today().toStartOfWeek());
    }

    public void changeViewBy(){
        // Clear the Project and Opportunity - this is true even if changing to project or Opportunity
        this.projectId = null;
        this.opportunityId = null;
        if(this.viewBy == 'Person'){
            this.runningAsUser = UserInfo.getUserId();
        }else {
            // For either view by project or opporutnity the running User needs to be cleared.
            this.runningAsUser = null;
        }
        doSetup(Date.today().toStartOfWeek());
    }
    
    public void setViewTotal(){
        /*list<AggregateResult> arList = [SELECT SUM(Hours__c) hours FROM Forecast__c WHERE r.Work__r.Fortimize_Project__c = :this.projectId GROUP BY Work_Task__r.Work__r.Fortimize_Project__c];
        if(!arList.isEmpty()){
            this.totalProjectForecast = (Decimal)arList[0].get('hours');
        }else{
            this.totalProjectForecast = 0;
        }*/
    }
    
    public map<Date,Decimal> makeSummaryLine(){
        map<Date,Decimal> summaryLine = new map<Date,Decimal>();
        for(WorkTaskForecast wtf:this.tasks){
            for(Date d:wtf.forecastMap.keySet()){
                if(!summaryLine.containsKey(d)){
                    system.debug('adding startDate: ' + d );
                    summaryLine.put(d,0);
                }
                Decimal dnum = summaryLine.get(d);
                if(wtf.forecastMap.get(d).Hours__c != null){
                    dnum += wtf.forecastMap.get(d).Hours__c;
                    summaryLine.put(d,dnum);
                    system.debug('adding startDate: ' + d + ', record: '+JSON.serialize(dnum));
                }
            }
        }
        return summaryLine;
    }
    public list<WorkWeek> getWeeks(list<Date> dates){
        list<WorkWeek> wws = new list<WorkWeek>();
        for(Work_Week__c ww:[SELECT Id, Name, Start_Date__c, End_Date__c FROM Work_Week__c WHERE Start_Date__c IN :dates ORDER BY Start_Date__c ASC]){
            wws.add(new WorkWeek(ww));
        }
        return wws;
    }
    
    public list<Id> getProjectUsers(Id projectId){
        set<Id> userSet = new set<Id>();
        for(Work_Task__c wt : [SELECT Id, Assigned_To__c FROM Work_Task__c WHERE Work__r.Fortimize_Project__c = :projectId]){
            userSet.add(wt.Assigned_To__c);
        }
        list<Id> userIds = new list<Id>();
        userIds.addAll(userSet);
        System.Debug('getProjectUsers:'+userIds);
        return userIds;
    }

    public list<Id> getAllUsers() {
        set<Id> userSet = new set<Id>();
        for(User u:[SELECT Id, FirstName, LastName FROM User WHERE isActive = true ORDER BY LastName ASC]){
            userSet.add(u.Id);
        }
        list<Id> userIds = new list<Id>();
        userIds.addAll(userSet);
        System.Debug('getAllUsers:'+userIds);
        return userIds;
    }
    
    public list<SelectOption> allUsers{
        get{
            if(allUsers == null){
                allUsers = new list<SelectOption>();
                for(User u:[SELECT Id, FirstName, LastName FROM User WHERE isActive = TRUE ORDER BY LastName ASC]){
                    allUsers.add(new SelectOption(u.Id, u.LastName + ', ' + u.FirstName));
                }
            }
            return allUsers;
        }set;}
    
    public list<SelectOption> allProjects{
        get{
            if(allProjects == null){
                allProjects = new list<SelectOption>();
                for(Project__c p:[SELECT Id, Name FROM Project__c WHERE Public_Forecast_Visibility_Now__c = FALSE and Is_Active__c=TRUE ORDER BY Name ASC]){
                    allProjects.add(new SelectOption(p.Id, p.Name));
                }
            }
            return allProjects;
        }set;}

    public list<SelectOption> allOpportunities {
        get{
            if(allOpportunities == null){
                allOpportunities = new list<SelectOption>();
                List<Opportunity> opportunities = [Select Id, Name from Opportunity where Id in (Select Opportunity__c from Project_Role__c where Project__c = null) and  Hide_Opportunity_Forecasting__c=FALSE and ForecastCategoryName != 'Closed' and ForecastCategoryName != 'Omitted' order by Name ASC];
                  for(Opportunity opp: opportunities) {
                      allOpportunities.add(new SelectOption(opp.Id, opp.Name));
                }
            }
            return allOpportunities;
        }set;}

    public list<SelectOption> viewByOptions{
        get{
            if(viewByOptions == null){
                viewByOptions = new list<SelectOption>();
                viewByOptions.add(new SelectOption('Person','Person'));
                viewByOptions.add(new SelectOption('Project','Project'));
                viewByOptions.add(new SelectOption('Opportunity','Opportunity'));
            }
            return viewByOptions;
        }set;
    }
    
    public list<WorkTaskForecast> tasks{get;set;}
    
    public list<WorkTaskForecast>populateTasks(list<WorkWeek> wws, list<Id> userIds, Boolean canEdit){
        list<Id> wwIds = new list<Id>();
        for(WorkWeek ww:wws){
            wwIds.add(ww.id);
        }
        list<WorkTaskForecast> wtfs = new list<WorkTaskForecast>();
        set<Id> taskIds = new set<Id>();
        map<Id,WorkTaskForecast> tMap = new map<Id,WorkTaskForecast>();
        Id pId = this.projectId;
        Id opptyId = this.opportunityId;
        string qry = 'SELECT Id, Hours__c, Project_Role__c, Project_Role__r.Resource__r.FirstName, Project_Role__r.Resource__r.LastName,';
        qry += ' Project_Role__r.Role__c, Work_Week__r.Start_Date__c, Project_Role__r.Hours_Needed__c, Comment__c, Project_Role__r.Project_name__c,';
        qry += ' Project_Role__r.Project__r.Public_Forecast_Visibility_Now__c, Project_Role__r.Status__c, Resource__c';
        qry += ' FROM Forecast__c';
        qry += ' WHERE Work_Week__c IN :wwIds ';
        if(pId != null){
            qry += ' AND Project_Role__r.Project__c = :pId';
        } else if(opptyId != null){
            qry += ' AND Project_Role__r.Opportunity__c = :opptyId';
        } else if(isForecastAdmin) {
            qry += ' AND (Project_Role__r.Resource__c IN :userIds OR Resource__c in :userIds)';
        } else if(opptyId == null && pId == null){
            // A Non Admin has certain items hidden
            qry += ' AND ((Project_Role__r.Resource__c IN :userIds AND Project_Role__r.Status__c != :forecastOnlyProjectRole) OR (Resource__c IN :userIds AND Project_Role__r.Project__r.Public_Forecast_Visibility_Now__c=true)) ';
        }
        system.debug('query: ' + qry);
        for(Forecast__c f:database.query(qry)){
            if(!tMap.containsKey(f.Project_Role__c)){
                // Row can be edited if canEdit is true OR Public Forecast Visibility Now is true
                WorkTaskForecast wtf = new WorkTaskForecast(wws, f, canEdit || f.Project_Role__r.Project__r.Public_Forecast_Visibility_Now__c); // initialize
                tMap.put(f.Project_Role__c,wtf);
            }else{
                tMap.get(f.Project_Role__c).addForecast(f);
            }
        }
        list<WorkTaskForecast> sortedList = tMap.values();
        sortedList.sort();
        System.Debug('Sorted List:'+sortedList);
        return sortedList;
    }
    
    public class WorkTaskForecast implements comparable{
        public Id workTaskId{get;set;}
        public string name{get;set;}
        public string projectName{get;set;}
        public string taskType{get;set;}
        public decimal totalHours{get;set;}
        public decimal forecastedHours{get;set;}
        public decimal hoursAvailable{get;set;}
        public decimal deltaHours{get;set;}
        public decimal hoursWorked{get;set;}
        public map<Date,Forecast__c> forecastMap{get;set;}
        public Id assignedTo{get;set;}
        public string assignedToName{get;set;}
        public Project_Role__c role{get;set;}
        public boolean canEdit{get;set;}
        public boolean showTotalHours{get;set;}
        public boolean forecastingOnly {get; set;}

        public WorkTaskForecast(list<WorkWeek> wws, Forecast__c f, Boolean canEdit){
            this.showTotalHours=true;
            this.forecastMap = new map<Date,Forecast__c>();
            for(WorkWeek ww:wws){
                this.forecastMap.put(ww.startDate,new Forecast__c(Work_Week__c = ww.id, Project_Role__c = f.Project_Role__c, Resource__c = f.Resource__c));
            }
            this.workTaskId = f.Project_Role__c;
            this.name = f.Project_Role__r.Role__c;
            this.assignedTo = f.Project_Role__r.Resource__c;
            this.assignedToname = f.Project_Role__r.Resource__r.FirstName + ' ' + f.Project_Role__r.Resource__r.LastName;
            this.forecastMap.put(f.Work_Week__r.Start_Date__c, f);
            this.forecastedHours= f.Hours__c;
            this.totalHours = f.Project_Role__r.Hours_Needed__c;
            this.role = f.Project_Role__r;
            this.projectName = f.Project_Role__r.Project_Name__c;
            this.canEdit = canEdit;
            this.forecastingOnly = false;
            if(f.Project_role__r.Status__c == forecastOnlyProjectRole)
            {
                this.forecastingOnly=true;
            }
            if(f.Project_Role__r.Project__r.Public_Forecast_Visibility_Now__c) {
                this.showTotalHours=false;
            }
        }
        
        public WorkTaskForecast(list<WorkWeek> wws, Id workTaskId, Id runningAsUser){
            this.showTotalHours=true;
            this.forecastMap = new map<Date,Forecast__c>();
            Project_Role__c pr = [SELECT Id, Role__c , Project_Name__c, Hours_Needed__c, Resource__c, Status__c, Resource__r.FirstName, Resource__r.Lastname, Project__r.Public_Forecast_Visibility_Now__c FROM Project_Role__c WHERE Id = :workTaskId];
            Id runningUserToSet=null;
            if(pr.Project__r.Public_Forecast_Visibility_Now__c) {
                runningUserToSet = runningAsUser;
                this.showTotalHours=false;
            }
            this.forecastingOnly=false;
            if(pr.Status__c== forecastOnlyProjectRole) {
                this.forecastingOnly=true;
            }
            for(WorkWeek ww:wws){
                this.forecastMap.put(ww.startDate,new Forecast__c(Work_Week__c = ww.id, Project_Role__c = workTaskId, Resource__c=runningUserToSet));
            }
            this.workTaskId = pr.Id;
            this.name = pr.Role__c;
            this.totalHours = pr.Hours_Needed__c;
            this.role = pr;
            this.projectName = pr.Project_Name__c;
            this.canEdit = true;
            /*this.forecastedHours = wt.Forecasted_Hours__c;
            this.hoursWorked = wt.Completed_Hours__c;
            this.hoursAvailable = wt.Remaining_Forecast_Hours__c < wt.Remaining_Hours__c ? wt.Remaining_Forecast_Hours__c : wt.Remaining_Hours__c;*/
        }
        
        public void addForecast(Forecast__c f){
            this.forecastedHours += f.Hours__c;
            this.forecastMap.put(f.Work_Week__r.Start_Date__c,f);
        }
        
        public Integer compareTo(Object compareTo) {
            WorkTaskForecast compareToWTF = (WorkTaskForecast)compareTo;
            string compareToName = compareToWTF.projectName + compareToWTF.name;
            string thisWTFName = this.projectName + this.name;
            if (thisWTFName == compareToName) return 0;
            if (thisWTFName > compareToName) return 1;
            return -1;        
        }
    }
    
    public list<SelectOption> getPossibleTasks(list<WorkTaskForecast> tasksInView){
        // Clear the tasks
        this.newTaskId=null;
        list<Id> tasksInViewIds = new list<Id>();
        for(WorkTaskForecast wtf:tasksInView){
            // exclude tasks that are already to be displayed in the task list.
            tasksInViewIds.add(wtf.workTaskId);
        }
        system.debug('tasksInViewIds: ' + tasksInViewIds);
        list<SelectOption> options = new list<SelectOption>();
        // query all the Work Tasks assigned to the user that have hours available.
        string qry = 'SELECT Id, Name, Role__c, Project_Name__c FROM Project_Role__c';
        
        Id pId = this.projectId;
        Id opptyId = this.opportunityId;
        System.debug('isForecastAdmin: ' + isForecastAdmin);
        if(isForecastAdmin && pId != null){
            qry += ' WHERE Project__c = :pId';
        } else if(isForecastAdmin && opptyId != null){
            qry += ' WHERE Opportunity__c = :opptyId and Opportunity__r.Hide_Opportunity_Forecasting__c=FALSE ';
        } else {
            qry += ' WHERE Project__r.Public_Forecast_Visibility_Now__c=TRUE ';
        }
        
        qry += ' AND Id NOT IN :tasksInViewIds ORDER BY Project_Name__c ASC';

            for(Project_Role__c pr:Database.query(qry)){
                system.debug('pr: ' + pr);
                options.add(new SelectOption(pr.Id,pr.Project_Name__c + ' - ' + pr.Role__c));
                if(this.newTaskId == null) this.newTaskId=pr.Id;
            }
        return options;
    }
    
    public void addWorkTaskLine(){
        system.debug('this tasks before add is: ' + JSON.serialize(this.tasks));
        WorkTaskForecast wtf = new WorkTaskForecast(this.weeks, newTaskId, this.runningAsUser);
        this.tasks.add(wtf);
        system.debug('this tasks AFTER add is: ' + JSON.serialize(this.tasks));
        
        integer i = 0;
        for(SelectOption so: this.tasksAvailableForView){
            if(so.getValue() == newTaskId){
                tasksAvailableForView.remove(i);
                break;
            }
            i = i+1;
        }
    }
    
    public void addAllWorkTaskLines() {
        for(SelectOption task : tasksAvailableForView) {
            this.tasks.add(new WorkTaskForecast(this.weeks, task.getValue(), this.runningAsUser));
        }
        this.tasksAvailableForView.clear();
    }
    
    public Id newTaskId{get;set;}
    public Id deleteRowId{get;set;}
    
    public void deleteRowFromCurrentView(){
        system.debug('deleteRowId: ' + deleteRowId);
        integer i = 0;
        integer removeIndex;
        list<Forecast__c> toDelete = new list<Forecast__c>();
        for(WorkTaskForecast wtf:tasks){
            if(wtf.workTaskId == deleteRowId){
                removeIndex = i;
                // delete all the forecasts in the current view.
                for(Forecast__c f : wtf.forecastMap.values()){
                    if(f.Id != null){
                        toDelete.add(f);
                    }
                }
                break;
            }
            i = i+1;
        } 
        if(!toDelete.isEmpty()){
            delete toDelete;
        }
        
        this.tasksAvailableForView.add(new SelectOption(tasks[removeIndex].workTaskId,tasks[removeIndex].name));
        
        system.debug('removing index: ' + tasks[removeIndex]);
        
        tasks.remove(removeIndex);
        
        // recalculate the summary
        this.summaryLine = makeSummaryLine();
        if(this.projectId != null){
            // calculate the total hours for a project.
            setViewTotal();
        }
    }
    
    public void setHours(){
        list<Forecast__c> toDelete = new list<Forecast__c>();
        list<Forecast__c> toUpsert = new list<Forecast__c>();
        list<Project_Role__c> rolesToUpsert = new list<Project_Role__c>();
        
        for(WorkTaskForecast wtf:this.tasks){
            rolesToUpsert.add(wtf.role);
            for(Forecast__c f:wtf.forecastMap.values()){
                // first, find any that need to be deleted.
                if(f.Id != null && (f.Hours__c == null || f.Hours__c == 0)){
                    toDelete.add(f);
                }else if(f.Hours__c != null && f.Hours__c > 0){
                        // See if public visibility item and needs resource
                    System.Debug('Set Hours: Forecast: '+JSON.serialize(f));
                    System.Debug('Public Visibility:'+f.Project_Role__r.Project__r.Public_Forecast_Visibility_Now__c);
                    System.Debug('Resource:'+f.Resource__c);
                    if(f.Project_Role__r.Project__r.Public_Forecast_Visibility_Now__c && f.Resource__c==null) {
                        f.Resource__c=UserInfo.getUserId();
                    }
                    System.debug('Adding to update:'+JSON.serialize(f));
                    // if there are hours, upsert them
                    toUpsert.add(f);
                }
            }
        }
        
        SavePoint sp = Database.setSavepoint();
        try{
            if(!rolesToUpsert.isEmpty()){
                upsert rolesToUpsert;
            }
            if(!toDelete.isEmpty()){
                delete toDelete;
            }
            if(!toUpsert.isEmpty()){
                upsert toUpsert;
            }
            this.summaryLine = makeSummaryLine();
            if(this.projectId != null){
                setViewTotal();
            }
            doSetup(this.startDate);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM ,'Your forecast has been updated.'));
        }catch(Exception e){
            if(e.getMessage().containsIgnoreCase('validation')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,Label.Over_Forecasted_Error));
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,'Could not update your forecast.  ' + e.getMessage()));
            }
        }
    }
    
    public class WorkWeek {
        public Id id{get;set;}
        public string name{get;set;}
        public Date startDate{get;set;}
        public Date endDate{get;set;}
        public WorkWeek(Work_Week__c ww){
            this.id = ww.Id;
            this.name = ww.Name;
            this.startDate = ww.Start_Date__c;
            this.endDate = ww.End_Date__c;
        }
    }
}