public class ContactTriggerHandler {

    public void afterInsert(List<Contact> newContacts) {
        onEmployeeStatusChange(newContacts, new Map<Id,Contact>());
    }

    public void afterUpdate(List<Contact> newContacts, Map<Id,Contact> oldContactsMap){
        CashFlowDataSharing.updateContactSharing(newContacts, oldContactsMap);
        onEmployeeStatusChange(newContacts, oldContactsMap);
    }

    private void onEmployeeStatusChange(List<Contact> newContacts, Map<Id,Contact> oldContactsMap) {
        Id employeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Employee').getRecordTypeId();
        
        Set<Id> activeEmployees = new Set<Id>();
        Set<Id> formerEmployees = new Set<Id>();
        for (Contact contact : newContacts) {
            if (contact.RecordTypeId == employeeRecordTypeId) {
                Contact oldContact = oldContactsMap.get(contact.Id);
                if (contact.FO_Employment_Status__c == 'Active' && contact.Start_Date__c != null
                        && (oldContact == null || oldContact.FO_Employment_Status__c != 'Active' || oldContact.Start_Date__c == null))
                    activeEmployees.add(contact.Id);
                else if ((contact.FO_Employment_Status__c == 'Former' || contact.FO_Employment_Status__c == 'Furlough') && (oldContact == null || (oldContact.FO_Employment_Status__c != 'Former' && oldContact.FO_Employment_Status__c != 'Furlough')))
                    formerEmployees.add(contact.Id);
            }
        }

        if (!activeEmployees.isEmpty())
            TimesheetService.createFutureTimesheets(activeEmployees);
        if (!formerEmployees.isEmpty())
            TimesheetService.deleteFutureTimesheets(formerEmployees);
    }
}