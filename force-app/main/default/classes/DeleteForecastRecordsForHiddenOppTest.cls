/**********************************************************
@class            DeleteForecastRecordsForHiddenOppTest
@brief            Test Class for DeleteForecastRecordsForHiddenOpp
@bug              None
@todo             None
@version          V1.0
@author           Developer Name (rchapple)
@date             6/4/18
@copyright        (c)2018 Fortimize.  All Rights Reserved. Unauthorized use is prohibited.
***********************************************************/
@isTest
private class DeleteForecastRecordsForHiddenOppTest {
    static list<Work_Week__c> weeks;
    static Account account;
    static Opportunity opportunity;
    static Project_Role__c opportunityRole;
    static List<User> usersToTestWith;

    static integer numWeeks = 3;
    static{
        weeks = new list<Work_Week__c>();
        date startDate = date.today().toStartOfWeek();
        for(integer i=0;i<numWeeks;i++){
            Work_Week__c ww = new Work_Week__c();
            ww.Start_Date__c = startDate;
            ww.End_Date__c = startDate.addDays(6);
            weeks.add(ww);
            startDate = startDate.addDays(7);
        }
        insert weeks;

        account = new Account();
        account.name = 'test';
        insert account;

        // Create an opportunity with a role
        opportunity = new Opportunity();
        opportunity.Name = 'Test Open Opportunity';
        opportunity.CloseDate = Date.today().addDays(30);
        opportunity.StageName='Prospect/ Qualifying';
        insert opportunity;

        opportunityRole = new Project_Role__c();
        opportunityRole.Opportunity__c=opportunity.Id;
        opportunityRole.Hours_Needed__c = 100;
        opportunityRole.Resource__c = UserInfo.getUserId();
        opportunityRole.Role__c = 'Senior Developer (SD)';
        opportunityRole.Hour_Type__c = 'Billable';
        insert opportunityRole;

        usersToTestWith = [Select Id, Name from User LIMIT 5];
    }

    @isTest
    public static void testDeleteMethodWhenNoRecords(){
        Forecast__c f = new Forecast__c();
        f.Work_Week__c = weeks[0].Id;
        f.Hours__c = 4;
        f.Project_Role__c = opportunityRole.id;
        insert f;

        //Call before test to ensure there are no records
        DeleteForecastRecordsForHiddenOpp.ForecastDelete(new List<Id>{opportunity.Id});
        List<Forecast__c> forecastsBefore = [Select Id from Forecast__c];
        System.assertEquals(0, forecastsBefore.size(), 'We should not have any records before we ran');

        Test.startTest();
        DeleteForecastRecordsForHiddenOpp.ForecastDelete(new List<Id>{opportunity.Id});
        Test.stopTest();

        //No validation needed we just need the call to not have failed or thrown any errors
    }
}