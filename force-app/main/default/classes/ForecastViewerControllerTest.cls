@IsTest
private class ForecastViewerControllerTest {
    
    @TestSetup
    static void makeData(){
		
        Contact contact = TestObjectFactory.getContact();
        contact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Employee').getRecordTypeId();
        contact.FO_Employment_Status__c = 'Active';
        contact.Start_Date__c = Date.today();
        contact.User_Link__c = UserInfo.getUserId();
        insert contact;

        FMZ_Project__c project = new FMZ_Project__c();
        insert project;

        FMZ_Role__c role = new FMZ_Role__c(Name='Test',Role_Abbreviation__c='TST',Effective_Start_Date__c=system.today(),List_Price__c=200,Active__c=true);
        insert role;
        
        FMZ_Project_Role__c projectRole = new FMZ_Project_Role__c(
            FMZ_Project__c = project.Id,
            FMZ_Role__c = role.Id,
            SOW_Allocated_Hours__c = 1
        );
        insert projectRole;

        FMZ_Resource_Assignment__c assignment = new FMZ_Resource_Assignment__c(
            FMZ_Project_Role__c = projectRole.Id,
            FMZ_Resource__c = contact.Id,
            Assignment_Start_Date__c = Date.today(),
            Assignment_End_Date__c = Date.today().addMonths(1),
            Status__c = 'Confirmed'
        );
        insert assignment;
    }

    @IsTest
    static void getResourceAssignments() {
        Date weekStartDate = Date.today().toStartOfWeek();

        Test.startTest();
        List<FMZ_Resource_Assignment__c> assignments = ForecastViewerController.getResourceAssignments(weekStartDate);
        Test.stopTest();

        System.assertEquals(1, assignments.size());
    }

    @IsTest
    static void submitTimesheetDetails() {
        Date weekStartDate = Date.today().toStartOfWeek();

        FMZ_Resource_Assignment__c assignment = [SELECT Id FROM FMZ_Resource_Assignment__c];
        FMZ_Timesheet__c timesheet = [SELECT Id FROM FMZ_Timesheet__c WHERE Week_Start_Date__c = :weekStartDate];

        FMZ_Timesheet_Detail__c detail = new FMZ_Timesheet_Detail__c(
            FMZ_Timesheet__c = timesheet.Id,
            FMZ_Resource_Assignment__c = assignment.Id,
            Hours_Worked__c = 5,
            Date_Worked__c = Date.today(),
            Status__c = 'Open');
        insert detail;

        Test.startTest();
        ForecastViewerController.submitTimesheetDetails(weekStartDate);
        Test.stopTest();

        detail = [SELECT Id, Status__c FROM FMZ_Timesheet_Detail__c];
        System.assertEquals('Submitted', detail.Status__c);
    }
}