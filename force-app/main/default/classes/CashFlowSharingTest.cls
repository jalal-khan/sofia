@isTest (seeAllData = true)
public without sharing class CashFlowSharingTest {
    
    @isTest
    public static void testUserSharing(){
        Profile p = [SELECT Id from Profile WHERE Name = 'Executive'];
        User testUser = [SELECT Id FROM User WHERE isActive = true AND Id !=: UserInfo.getUserId() AND ProfileId =: p.Id LIMIT 1];
        Account a = new Account();
        a.Name = 'Fortimize';
        INSERT a;
        
        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'Testerson';
        c.AccountId = a.Id;
        c.Employee_Department__c = 'Professional Services';
        c.Employee_Role__c = 'Developer (D)';
        //c.RecordTypeId = '0120m000000kkBX';
        INSERT c;
        
        CashFlowSharing__c cfs = new CashFlowSharing__c();
        cfs.Name = 'Test Testerson';
        cfs.BAT__c = false;
        cfs.Professional_Services__c = true;
        //cfs.User_Id__c = testUser.Id;
        INSERT cfs;
        
        User u = new User();
        u.FirstName = 'Test';
        u.LastName = 'Testerson';
        u.ProfileId = p.Id;
        u.Username = 'testtesterson@fortimize.com';
        u.Email = 'test@test.com';
        u.Alias = 'test';
        u.TimeZoneSidKey = 'America/New_York';
        u.LocaleSidKey = UserInfo.getLocale();
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        INSERT u;
        
        Test.startTest();
        
        CashFlowSharing__c cfsTest = [SELECT Name, User_Id__c FROM CashFlowSharing__c WHERE Name =: 'Test Testerson'];
        //system.assertEquals(u.Id, cfsTest.User_Id__c);
        
        Cash_Flow_Data__c cfd = new Cash_Flow_Data__c();
        cfd.Employee_Contractor__c = c.Id;
        cfd.Date__c = Date.today();
        INSERT cfd;
        
        List<Cash_Flow_Data__Share> cfdShare = [SELECT Id FROM Cash_Flow_Data__Share WHERE ParentId =: cfd.Id 
                                                AND RowCause =: Schema.Cash_Flow_Data__Share.RowCause.Executive__c
                                               AND UserorGroupId =: testUser.Id];
        
        //system.assertEquals(1, cfdShare.size());
        
        DELETE cfs;
        
        u.LastName = 'Testerson Jr';
        UPDATE u;
        
        List<Cash_Flow_Data__Share> cfdShare2 = [SELECT Id FROM Cash_Flow_Data__Share WHERE ParentId =: cfd.Id 
                                                 AND RowCause =: Schema.Cash_Flow_Data__Share.RowCause.Executive__c
                                                AND UserorGroupId =: testUser.Id];
        
        //system.assertEquals(0, cfdShare2.size());
        
        Test.stopTest();
    }
    
    @isTest
    public static void testContactSharing(){
        Profile p = [SELECT Id from Profile WHERE Name = 'Executive'];
        User testUser = [SELECT Id FROM User WHERE isActive = true AND Id !=: UserInfo.getUserId() AND ProfileId =: p.Id LIMIT 1];
        
        Account a = new Account();
        a.Name = 'Fortimize';
        INSERT a;
        
        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'Testerson';
        c.AccountId = a.Id;
        c.Employee_Department__c = 'Professional Services';
        c.Employee_Role__c = 'Developer (D)';
        //c.RecordTypeId = '0120m000000kkBX';
        c.Email = 'test@test.com';
        INSERT c;
        
        CashFlowSharing__c cfs = new CashFlowSharing__c();
        cfs.Name = 'Test Testerson';
        cfs.BAT__c = false;
        cfs.Professional_Services__c = true;
        cfs.User_Id__c = testUser.Id;
        INSERT cfs;
        
        Cash_Flow_Data__c cfd = new Cash_Flow_Data__c();
        cfd.Employee_Contractor__c = c.Id;
        cfd.Date__c = Date.today();
        INSERT cfd;
        
        Test.startTest();
        
        c.Employee_Department__c = 'BAT';
        //c.Employee_Role__c = 'Admin';
        UPDATE c;
        
        List<Cash_Flow_Data__Share> cfdShare = [SELECT Id FROM Cash_Flow_Data__Share WHERE ParentId =: cfd.Id 
                                                AND RowCause =: Schema.Cash_Flow_Data__Share.RowCause.Executive__c
                                               AND UserorGroupId =: testUser.Id];
        
        system.assertEquals(1, cfdShare.size());
        
        Test.stopTest();
    }
    
    @isTest
    public static void testCashFlowSharing(){
        Profile p = [SELECT Id from Profile WHERE Name = 'Executive'];
        User testUser = [SELECT Id FROM User WHERE isActive = true AND Id !=: UserInfo.getUserId() AND ProfileId =: p.Id LIMIT 1];
        
        Account a = new Account();
        a.Name = 'Fortimize';
        INSERT a;
        
        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'Testerson';
        c.AccountId = a.Id;
        c.Employee_Department__c = 'Professional Services';
        c.Employee_Role__c = 'Developer (D)';
        //c.RecordTypeId = '0120m000000kkBX';
        c.Email = 'test@test.com';
        INSERT c;
        
        CashFlowSharing__c cfs = new CashFlowSharing__c();
        cfs.Name = 'Test Testerson';
        cfs.BAT__c = true;
        cfs.Professional_Services__c = true;
        cfs.Executive__c = true;
        cfs.Finance__c = true;
        cfs.HR__c = true;
        cfs.Legal__c = true;
        cfs.Revenue__c = true;
        cfs.User_Id__c = testUser.Id;
        INSERT cfs;
        
        Cash_Flow_Data__c cfd = new Cash_Flow_Data__c();
        cfd.Employee_Contractor__c = c.Id;
        cfd.Date__c = Date.today();
        INSERT cfd;
        
        Test.startTest();
        
        Cash_Flow_Data__c cfdTest = [SELECT Department__c FROM Cash_Flow_Data__c WHERE Id =: cfd.Id];
        
        system.assertEquals(c.Employee_Department__c, cfdTest.Department__c);
        
        List<Cash_Flow_Data__Share> cfdShare = [SELECT Id FROM Cash_Flow_Data__Share WHERE ParentId =: cfd.Id 
                                                AND RowCause =: Schema.Cash_Flow_Data__Share.RowCause.Executive__c
                                               AND UserorGroupId =: testUser.Id];
        
        system.assertEquals(1, cfdShare.size());
        
        cfd.Department__c = 'BAT';
        UPDATE cfd;
        
        Test.stopTest();
    }
}