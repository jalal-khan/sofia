@isTest
global class CKLST_TestObjectFactory {

	public static final string SMALL_BLANK_JPG_BASE64 = '/9j/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/yQALCAABAAEBAREA/8wABgAQEAX/2gAIAQEAAD8A0s8g/9k=';

	private static integer counter = 0;
		public static string getUniqueName(string prefix) {
		return prefix + counter++;
	}

	public static string getRepeatName(string prefix) {
		return prefix + (counter - 1);
	}

	public static Account getAccount() {
		return new Account(
			name = getUniqueName('Acct')
		);
	}

	public static list<Account> getAccounts(integer num) {
		list<Account> accounts = new list<Account>();
		for(integer i=0;i<num;i++) {
			accounts.add(getAccount());
		}
		return accounts;
	}

	public static Contact getContact(Account acct, string lastName, string email, Date birthdate) {
		return new Contact(
			AccountId = acct == null ? null : acct.id,
			lastName = lastName,
			email = email,
			birthdate = birthdate
		);
	}

	public static Contact getContact(Account acct) {
		return getContact(Acct, getUniqueName('last'), getRepeatName('name') + '@example.com', Date.Today().addYears(-25) );
	}

	public static List<Contact> getContacts(Account acct, integer count) {
		List<Contact> contacts = new List<Contact>();
		for (integer i=0; i<count; i++) {
			contacts.add(getContact(acct));
		}
		return contacts;
	}

	public static Opportunity getDeal() {
		return new Opportunity(
			name = getUniqueName('deal'),
			stagename = 'Diligence'
		);
	}

	public static Opportunity getDealWithDates() {
		Opportunity deal = getDeal();
		integer n = 0;
		for (string fld : getOpptyDateFields()) {
			deal.put(fld, Date.today().addDays(n));
		}
		return deal;
	}

	public static list<Opportunity> getDeals(integer num) {
		list<Opportunity> deals = new list<Opportunity>();
		for(integer i=0;i<num;i++) {
			deals.add(getDeal());
		}
		return deals;
	}

	public static list<Internal_Role__c> getInternalDealContactRoles(Opportunity deal, list<User> users) {
		list<Internal_Role__c> res = new list<Internal_Role__c>();
		map<string, List<string>> dcrDependentValuesMap = FMZ_SchemaUtils.GetDependentOptions(
				Internal_Role__c.sObjectType.getDescribe().getName(),
				Internal_Role__c.Object__c.getDescribe().getName(),
				Internal_Role__c.Role__c.getDescribe().getName()
		);
		list<string> roles = dcrDependentValuesMap.get('Opportunity');
		for (integer i=0; i<math.min(roles.size(), users.size()); i++) {
			res.add(
				new Internal_Role__c(
					Object__c = 'Opportunity',
					Opportunity__c = deal.id,
					Internal_Team_Member__c = users[i].id,
					Role__c = roles[i]
				)
			);
		}
		return res;
	}

	public static User getSysAdmin() {
		return getUserForProfile('System Administrator');
	}


	public static User getUserForProfile(string profileName) {
		Profile prof = [Select id from Profile where Name = :profileName limit 1];
		return getUserForProfileId(prof.id);
	}

	public static User getUserForProfileId(id profileId) {
		String name = getUniqueName('User');
		String email = name + '@jamestown.com';
		User u = new User(
		Username = email,
		LastName = name,
		CommunityNickname = getUniqueName('nick'),
		alias = 'testuser', //max length 8
		Email = email,
		ProfileId = profileId,
		TimeZoneSidKey = 'America/New_York',
		EmailEncodingKey='UTF-8',
		LanguageLocaleKey = 'en_US',
		LocaleSidKey = 'en_US');
		return u;
	}

	public static list<User> getUsersForProfile(string profileName, integer num) {
		Profile prof = [Select id from Profile where Name = :profileName limit 1];
		list<user> users = new list<user>();
		for(integer i=0;i<num;i++) {
			users.add(getUserForProfileId(prof.id));
		}
		return users;
	}

	public static void makeOrgDefaults(){
		//FMZ_Active_Monitoring__c am = new FMZ_Active_Monitoring__c();
		//am.Active__c = false;
		//am.Last_Exception_Job_Time__c = null;
		//am.Subscription_End__c = Date.today();
		//insert am;
	}

	public static Opportunity getOpportunity() {
		return new Opportunity(
			name=getUniqueName('opp'),
			stageName='In Progress',
			closeDate=Date.Today().addMonths(1)
		);
	}
	public static Attachment getAttachment(id parentId) {
		return new Attachment(
			name = 'blank.jpg',
			parentId = parentId,
			IsPrivate = false,
			body = EncodingUtil.base64Decode(SMALL_BLANK_JPG_BASE64)
		);
	}

	public static Checklist_Template__c getChecklistTemplate() {
		return new Checklist_Template__c(name = getUniqueName('Templ'), Object__c = 'Opportunity');
	}

	public static list<Checklist_Template__c> getChecklistTemplates(integer n) {
		list<Checklist_Template__c> res = new list<Checklist_Template__c>();
		for (integer i=0; i<n; i++) {
			res.add(getChecklistTemplate());
		}
		return res;
	}

	public static list<Checklist_Template_Entry__c> getChecklistTemplateEntries(Checklist_Template__c template, integer n) {
		list<Checklist_Template_Entry__c> entries = new list<Checklist_Template_Entry__c>();
		string section = getUniqueName('Section ');
		for (integer i=0; i<n; i++) {
			entries.add( 
				new Checklist_Template_Entry__c (
					Checklist_Template__c = template.id,
					name = getUniqueName('Task '),
					Default_Deal_Team_Role__c = getDealContactRole(i),
					Due_Date_Based_On__c = getOpptyDateField(i),
					Due_Date_Offset__c = i,
					Section__c = section,
					Sort_Order__c = i
				)
			);
		}
		return entries;
	}

	public static list<Checklist_Template_Entry__c> getChecklistTemplateEntries(list<Checklist_Template__c> templates, integer n) {
		list<Checklist_Template_Entry__c> entries = new list<Checklist_Template_Entry__c>();
		for (Checklist_Template__c templ : templates) {
			entries.addAll(getChecklistTemplateEntries(templ, n));
		}
		return entries;
	}

	public static list<Checklist_Entry__c> getChecklistEntries(Checklist__c checklist) {
		list<Checklist_Entry__c> res = new list<Checklist_Entry__c>();
		Opportunity deal = (Opportunity) ChecklistServices.getDealWithDates(checklist.Opportunity__c);
		for (Checklist_Template_Entry__c entryTemplate : [
				select id, Name, Default_Deal_Team_Role__c, Due_Date_Based_On__c, Due_Date_Offset__c, Section__c
				from Checklist_Template_Entry__c
				where Checklist_Template__c = :checklist.Template__c
				order by Sort_Order__c
				]) {
			Date dueDate = null;
			if (entryTemplate.Due_Date_Based_On__c != null) {
				dueDate = (Date)deal.get(entryTemplate.Due_Date_Based_On__c);
			}
			if (dueDate != null && entryTemplate.Due_Date_Offset__c != null) {
				dueDate = dueDate.addDays((integer)entryTemplate.Due_Date_Offset__c);
			}
			res.add(new Checklist_Entry__c(
				Checklist__c = checklist.id,
				name = entryTemplate.name,
				template__c = entryTemplate.id,
				Internal_Role__c = entryTemplate.Default_Deal_Team_Role__c,
				Status__c = ChecklistServices.STATUS_NEEDED,
				Due_Date__c = dueDate
			));
		}
		return res;
	}

	private static list<string> dealContactRoles;
	private static string getDealContactRole(integer n) {
		if (dealContactRoles == null) {
			map<string, List<string>> dcrDependentValuesMap = FMZ_SchemaUtils.GetDependentOptions(
					Internal_Role__c.sObjectType.getDescribe().getName(),
					Internal_Role__c.Object__c.getDescribe().getName(),
					Internal_Role__c.Role__c.getDescribe().getName()
			);
			dealContactRoles = dcrDependentValuesMap.get('Opportunity');
		}
		return dealContactRoles[ math.mod(n, dealContactRoles.size()) ];
	}

	private static list<string> opptyDateFields;
	private static list<string> getOpptyDateFields() {
		if (opptyDateFields == null) {
			Map<String, Schema.DescribeFieldResult> fieldMap = FMZ_SchemaUtils.getObjectFieldDescribes(Opportunity.SObjectType);
			opptyDateFields = new list<string>();
			for (Schema.DescribeFieldResult dfr : fieldMap.values()) {
				if (dfr.getType() == Schema.DisplayType.Date && dfr.isUpdateable()) {
					opptyDateFields.add(dfr.getName());
				}
			}
		}
		return opptyDateFields;
	}

	private static string getOpptyDateField(integer n) {
		return getOpptyDateFields()[ math.mod(n, opptyDateFields.size()) ];
	}
}