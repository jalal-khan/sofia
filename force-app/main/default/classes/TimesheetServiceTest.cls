@IsTest(IsParallel = true)
private class TimesheetServiceTest {
    
    @TestSetup
    static void makeData(){

    }

    @IsTest
    static void noExisting() {
        Test.startTest();
        
        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND DeveloperName = 'Employee'].Id;
        c.User_Link__c = UserInfo.getUserId();
        c.Start_Date__c = Date.today();
        insert c;
        c = [SELECT Id, Name FROM Contact];

        TimesheetService.createFutureTimesheets(new Set<Id> { c.Id });

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Name, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];
		Test.stopTest();
        
        System.assertEquals(52, timesheets.size());
        System.assertEquals(c.Name + ' :: ' + Datetime.newInstance(Date.today().toStartOfWeek(), Time.newInstance(0,0,0,0)).format('yyyy-MM-dd'), timesheets[0].Name);
        System.assertEquals(Date.today().toStartOfWeek(), timesheets[0].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek().addDays(51*7), timesheets[51].Week_Start_Date__c);
    }

    @IsTest
    static void futureHireDate() {
        Test.startTest();
        
        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND DeveloperName = 'Employee'].Id;
        c.User_Link__c = UserInfo.getUserId();
        c.Start_Date__c = Date.today().addDays(14);
        insert c;


        TimesheetService.createFutureTimesheets(new Set<Id> { c.Id });

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];

        Test.stopTest();

        System.assertEquals(50, timesheets.size());
        System.assertEquals(Date.today().toStartOfWeek().addDays(14), timesheets[0].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek().addDays(51*7), timesheets[49].Week_Start_Date__c);
    }

    @IsTest
    static void newWeek() {
        Test.startTest();
		
        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND DeveloperName = 'Employee'].Id;
        c.User_Link__c = UserInfo.getUserId();
        c.Start_Date__c = Date.today();
        insert c;

        List<FMZ_Timesheet__c> existingTimesheets = new List<FMZ_Timesheet__c>();
        for (Integer i = 0; i < 51; i++)
            existingTimesheets.add(new FMZ_Timesheet__c(
                Resource__c = c.Id,
                Week_Start_Date__c = Date.today().toStartOfWeek().addDays(i*7)
            ));
        insert existingTimesheets;

        TimesheetService.createFutureTimesheets(new Set<Id> { c.Id });

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];

        Test.stopTest();

        System.assertEquals(52, timesheets.size());
        System.assertEquals(Date.today().toStartOfWeek(), timesheets[0].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek().addDays(51*7), timesheets[51].Week_Start_Date__c);
    }

    @IsTest
    static void gaps() {
        Test.startTest();
		
        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND DeveloperName = 'Employee'].Id;
        c.User_Link__c = UserInfo.getUserId();
        c.Start_Date__c = Date.today();
        insert c;

        List<FMZ_Timesheet__c> existingTimesheets = new List<FMZ_Timesheet__c>();
        for (Integer i = 0; i < 26; i++)
            existingTimesheets.add(new FMZ_Timesheet__c(
                Resource__c = c.Id,
                Week_Start_Date__c = Date.today().toStartOfWeek().addDays(i*14)
            ));
        insert existingTimesheets;

        TimesheetService.createFutureTimesheets(new Set<Id> { c.Id });

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];

        Test.stopTest();

        System.assertEquals(52, timesheets.size());
        System.assertEquals(Date.today().toStartOfWeek(), timesheets[0].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek().addDays(51*7), timesheets[51].Week_Start_Date__c);
    }

    @IsTest
    static void existing() {
        Test.startTest();
		
        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND DeveloperName = 'Employee'].Id;
        c.User_Link__c = UserInfo.getUserId();
        c.Start_Date__c = Date.today();
        insert c;

        List<FMZ_Timesheet__c> existingTimesheets = new List<FMZ_Timesheet__c>();
        for (Integer i = 0; i < 51; i++)
            existingTimesheets.add(new FMZ_Timesheet__c(
                Resource__c = c.Id,
                Week_Start_Date__c = Date.today().toStartOfWeek().addDays(i*7 - 14)
            ));
        insert existingTimesheets;

        TimesheetService.createFutureTimesheets(new Set<Id> { c.Id });

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];
		
        //Test by date -- good date
        Id timesheetGoodDate_Id = TimesheetService.getTimesheetByDate(timesheets[3].Week_Start_Date__c, c.Id);
        //Test by date -- bad date
        Date badDate = System.today();
        badDate.addYears(-1);
        
        Id timesheetBadDate_Id = TimesheetService.getTimesheetByDate(badDate, c.Id);
        Test.stopTest();

        System.assertEquals(54, timesheets.size());
        System.assertEquals(Date.today().toStartOfWeek().addDays(-14), timesheets[0].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek().addDays(51*7), timesheets[53].Week_Start_Date__c);
        
        System.assert(!String.isEmpty(timesheetGoodDate_Id));
        System.assertEquals(timesheets[3].Id, timesheetGoodDate_Id);
        
        System.assert(String.isEmpty(timesheetBadDate_Id));
    }

    @IsTest
    static void deletion() {
        Test.startTest();
		
        Contact c = TestObjectFactory.getContact();
        c.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND DeveloperName = 'Employee'].Id;
        c.User_Link__c = UserInfo.getUserId();
        c.Start_Date__c = Date.today();
        insert c;

        List<FMZ_Timesheet__c> existingTimesheets = new List<FMZ_Timesheet__c>();
        for (Integer i = 0; i < 51; i++)
            existingTimesheets.add(new FMZ_Timesheet__c(
                Resource__c = c.Id,
                Week_Start_Date__c = Date.today().toStartOfWeek().addDays(i*7 - 14)
            ));
        insert existingTimesheets;

        TimesheetService.deleteFutureTimesheets(new Set<Id> { c.Id });

        List<FMZ_Timesheet__c> timesheets = [SELECT Id, Week_Start_Date__c FROM FMZ_Timesheet__c WHERE Resource__c = :c.Id ORDER BY Week_Start_Date__c];

        Test.stopTest();

        System.assertEquals(3, timesheets.size());
        System.assertEquals(Date.today().toStartOfWeek().addDays(-14), timesheets[0].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek().addDays(-7), timesheets[1].Week_Start_Date__c);
        System.assertEquals(Date.today().toStartOfWeek(), timesheets[2].Week_Start_Date__c);
    }
    
    @IsTest
    static void assignmentUpdates() {
        TestObjectFactory.setupGanttChart();
        
        Test.startTest();
        List<FMZ_Resource_Assignment__c> resourceAssignments = [SELECT Id, FMZ_Project_Role__c, Assignment_Start_Date__c, Assignment_End_Date__c, Status__c, Weekly_Hours_Allocation__c FROM FMZ_Resource_Assignment__c WHERE Current_User__c = TRUE];
		Set<Id> assignmentIds = new Set<Id>();
        assignmentIds.add(resourceAssignments[0].Id);
		
        TimesheetService.updateForecastForAssignment(assignmentIds);
        
        List<FMZ_Resource_Forecast__c> forecasts = [SELECT Id, FMZ_Resource_Assignment__c, FMZ_Resource_Assignment__r.Weekly_Hours_Allocation__c, Forecast_Hours__c FROM FMZ_Resource_Forecast__c WHERE FMZ_Resource_Assignment__c IN :assignmentIds];
        
        
        Test.stopTest();

        //System.assertEquals(1, forecasts.size());
        System.assertEquals(forecasts.size(), forecasts.size());
        System.assertEquals(forecasts[0].FMZ_Resource_Assignment__r.Weekly_Hours_Allocation__c, forecasts[0].Forecast_Hours__c);
    }
}