@isTest
private class InternalRoleNewController_Test {

	@isTest
	private static void testDealTeamRoles() {
		Opportunity opp = CKLST_TestObjectFactory.getDealWithDates();
		insert opp;
		Internal_Role__c ir = new Internal_Role__c(Opportunity__c = opp.Id);
		ApexPages.StandardController sctrl = new ApexPages.StandardController(ir);
		InternalRoleNewController ctrl = new InternalRoleNewController(sctrl);

		List<String> dtrOpts = FMZ_SchemaUtils.GetDependentOptions(
				Internal_Role__c.sObjectType.getDescribe().getName(),
				Internal_Role__c.Object__c.getDescribe().getName(),
				Internal_Role__c.Role__c.getDescribe().getName()).get('Opportunity');
		dtrOpts.sort();
		System.assert(ctrl.dealTeamRoles.size() > 0);
		System.assertEquals(dtrOpts.size()+1, ctrl.dealTeamRoles.size()); //one more for --Select-- Option
	}

	@isTest
	private static void testInternalRole() {
		Opportunity opp = CKLST_TestObjectFactory.getDealWithDates();
		insert opp;
		Internal_Role__c ir = new Internal_Role__c(Opportunity__c = opp.Id);
		ApexPages.StandardController sctrl = new ApexPages.StandardController(ir);
		InternalRoleNewController ctrl = new InternalRoleNewController(sctrl);

		System.assertEquals(null, ctrl.internalRole.Id);
	}
}