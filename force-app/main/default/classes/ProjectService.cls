public with sharing class ProjectService {
    
    public static void createProjectRoles(Set<Id> opportunityIds) {

        Map<Id,FMZ_Opportunity_Role__c> lineItems = new Map<Id, FMZ_Opportunity_Role__c>([
            SELECT Id, Opportunity__r.FMZ_Project__c, Role__c, Total_Hours__c, Hourly_Rate__c
            FROM FMZ_Opportunity_Role__c WHERE Opportunity__c IN :opportunityIds]);

        Map<Id,FMZ_Project_Role__c> projectRolesByLineItem = new Map<Id,FMZ_Project_Role__c>();
        for (FMZ_Project_Role__c projectRole : [
                SELECT Id, FMZ_Opportunity_Role__c 
                FROM FMZ_Project_Role__c WHERE FMZ_Opportunity_Role__c IN :lineItems.keySet()])
            projectRolesByLineItem.put(projectRole.FMZ_Opportunity_Role__c, projectRole);

        List<FMZ_Project_Role__c> newProjectRoles = new List<FMZ_Project_Role__c>();
        for (FMZ_Opportunity_Role__c lineItem : lineItems.values()) {
            FMZ_Project_Role__c projectRole = projectRolesByLineItem.get(lineItem.Id);
            if (projectRole == null) {
                projectRole = new FMZ_Project_Role__c(
                    FMZ_Project__c = lineItem.Opportunity__r.FMZ_Project__c,
                    FMZ_Opportunity_Role__c = lineItem.Id);
                newProjectRoles.add(projectRole);
            }
            projectRole.FMZ_Role__c = lineItem.Role__c;
            projectRole.SOW_Allocated_Hours__c = lineItem.Total_Hours__c;
            projectRole.SOW_Hourly_Rate__c = lineItem.Hourly_Rate__c;
        }
        if (!newProjectRoles.isEmpty())
            insert newProjectRoles;
        if (!projectRolesByLineItem.isEmpty())
            update projectRolesByLineItem.values();
    }
    
    /*
    public static void createProjectRoles(Set<Id> opportunityIds) {

        Map<Id,OpportunityLineItem> lineItems = new Map<Id, OpportunityLineItem>([
            SELECT Id, Opportunity.FMZ_Project__c, Product2.Name, Quantity, UnitPrice
            FROM OpportunityLineItem WHERE OpportunityId IN :opportunityIds]);

        Map<Id,FMZ_Project_Role__c> projectRolesByLineItem = new Map<Id,FMZ_Project_Role__c>();
        for (FMZ_Project_Role__c projectRole : [
                SELECT Id, Opportunity_Product_ID__c
                FROM FMZ_Project_Role__c WHERE Opportunity_Product_ID__c IN :lineItems.keySet()])
            projectRolesByLineItem.put(projectRole.Opportunity_Product_ID__c, projectRole);

        List<FMZ_Project_Role__c> newProjectRoles = new List<FMZ_Project_Role__c>();
        for (OpportunityLineItem lineItem : lineItems.values()) {
            FMZ_Project_Role__c projectRole = projectRolesByLineItem.get(lineItem.Id);
            if (projectRole == null) {
                projectRole = new FMZ_Project_Role__c(
                    FMZ_Project__c = lineItem.Opportunity.FMZ_Project__c,
                    Opportunity_Product_ID__c = lineItem.Id);
                newProjectRoles.add(projectRole);
            }
            projectRole.Role__c = lineItem.Product2.Name;
            projectRole.SOW_Allocated_Hours__c = lineItem.Quantity;
            projectRole.SOW_Hourly_Rate__c = lineItem.UnitPrice;
        }
        if (!newProjectRoles.isEmpty())
            insert newProjectRoles;
        if (!projectRolesByLineItem.isEmpty())
            update projectRolesByLineItem.values();
    }
    */
    
}