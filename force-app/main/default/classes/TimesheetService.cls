/*
* Service to handle all business operations in relation to Timesheets
*/
public with sharing class TimesheetService {
    private static final String CONFIG_NAME = 'Default';

    /*
    * For every input contactid, this method will ensure that the person has a timesheet created up to the configured amount
    * Note that this requires a FMZ_Timesheet_Configuration custom metadata record to exist
    * Bulkified
    */
    public static void createFutureTimesheets(Set<Id> contactIds) {
        Date startDate = Date.today().toStartOfWeek();
        FMZ_Timesheet_Configuration__mdt config = [SELECT Id, Future_Weeks_to_Create_Timesheets__c FROM FMZ_Timesheet_Configuration__mdt WHERE MasterLabel = :CONFIG_NAME];
        
        Set<Date> futureWeeks = new Set<Date>();

        for (Integer i = 0; i < config.Future_Weeks_to_Create_Timesheets__c; i++) {
            futureWeeks.add(startDate.addDays(i*7));
        }

        List<FMZ_Timesheet__c> timesheetsToInsert = new List<FMZ_Timesheet__c>();
        for (Contact contact : [
                SELECT Id, Name, Start_Date__c, (SELECT Id, Week_Start_Date__c FROM FMZ_Timesheets__r WHERE Week_Start_Date__c >= :startDate)
                FROM Contact WHERE Id IN :contactIds]) {
            
            Set<Date> coveredDates = new Set<Date>();
            for (FMZ_Timesheet__c timesheet : contact.FMZ_Timesheets__r) {
                coveredDates.add(timesheet.Week_Start_Date__c);
            }

            Set<Date> missingWeeks = futureWeeks.clone();
            missingWeeks.removeAll(coveredDates);
            for(Date missingWeek : missingWeeks)
                if (missingWeek >= contact.Start_Date__c.toStartOfWeek())
                    timesheetsToInsert.add(new FMZ_Timesheet__c(
                        Name = contact.Name + ' :: ' + Datetime.newInstance(missingWeek, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd'),
                        Resource__c = contact.Id,
                        Week_Start_Date__c = missingWeek));
        }
        insert timesheetsToInsert;
        
        
    }
    
    /*
    * For every input contactid, this method will ensure that the person has a timesheet for N weeks from the provided start date
    * Bulkified
    */
    public static void createTimesheetsByDate(Set<Id> contactIds, Date startDate, Integer numberOfWeeks) {
        Set<Date> futureWeeks = new Set<Date>();

        for (Integer i = 0; i < numberOfWeeks; i++) {
            futureWeeks.add(startDate.addDays(i*7));
        }

        List<FMZ_Timesheet__c> timesheetsToInsert = new List<FMZ_Timesheet__c>();
        for (Contact contact : [
                SELECT Id, Name, Start_Date__c, (SELECT Id, Week_Start_Date__c FROM FMZ_Timesheets__r WHERE Week_Start_Date__c >= :startDate)
                FROM Contact WHERE Id IN :contactIds]) {
            
            Set<Date> coveredDates = new Set<Date>();
            for (FMZ_Timesheet__c timesheet : contact.FMZ_Timesheets__r) {
                coveredDates.add(timesheet.Week_Start_Date__c);
            }

            Set<Date> missingWeeks = futureWeeks.clone();
            missingWeeks.removeAll(coveredDates);
            for(Date missingWeek : missingWeeks)
                if (missingWeek >= contact.Start_Date__c.toStartOfWeek())
                    timesheetsToInsert.add(new FMZ_Timesheet__c(
                        Name = contact.Name + ' :: ' + Datetime.newInstance(missingWeek, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd'),
                        Resource__c = contact.Id,
                        Week_Start_Date__c = missingWeek));
        }
        insert timesheetsToInsert;
    }

    /*
    * Delets all future timesheets from today, should only be called from a batch job. 
    * Bulkified
    */
    public static void deleteFutureTimesheets(Set<Id> contactIds) {
        delete [SELECT Id FROM FMZ_Timesheet__c WHERE Resource__c IN :contactIds AND Week_Start_Date__c > TODAY];
    }

    /*
    * Updates all forecasts related to a resource assignment record so that their hours match accordingly. 
    * Bulkified
    */
    public static void updateForecastForAssignment(Set<Id> assignmentIds) {
        if(assignmentIds == null || assignmentIds.isEmpty()) {
            return;
        }

        List<FMZ_Resource_Forecast__c> forecasts = [SELECT Id, FMZ_Resource_Assignment__c, FMZ_Resource_Assignment__r.Weekly_Hours_Allocation__c FROM FMZ_Resource_Forecast__c WHERE FMZ_Resource_Assignment__c IN :assignmentIds];
        for(FMZ_Resource_Forecast__c forecast : forecasts) {
            forecast.Forecast_Hours__c = forecast.FMZ_Resource_Assignment__r.Weekly_Hours_Allocation__c;
        }

        update forecasts;
    }

    /*
    * Retrieve a timesheet record id based on the week that it starts in
    */
    public static Id getTimesheetByDate(Date startDate, Id resourceId) {
        List<FMZ_Timesheet__c> timesheets = [SELECT Id FROM FMZ_Timesheet__c WHERE Week_Start_Date__c = :startDate AND Resource__c = :resourceId];
        if(timesheets.isEmpty()) {
            return null;
        } else {
            return timesheets[0].Id;
        }
    }
}