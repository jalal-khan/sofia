@isTest
public class MyTimesheetControllerTest {
	static list<Work_Week__c> weeks;
    static Account account;
    static Project__c project;
    static Work__c work;
    static Work_Task__c workTask;
    static integer numWeeks = 3;
    static{
        weeks = new list<Work_Week__c>();
        date startDate = date.today().toStartOfWeek();
        for(integer i=0;i<numWeeks;i++){
            Work_Week__c ww = new Work_Week__c();
            ww.Start_Date__c = startDate;
            ww.End_Date__c = startDate.addDays(6);
            weeks.add(ww);
            startDate = startDate.addDays(7);
        }
        insert weeks;

		Account fortimizeAccount = new Account( Name = 'Fortimize' );
		insert fortimizeAccount;

		RecordType employee = [SELECT Id FROM RecordType WHERE DeveloperName = 'Employee' AND SobjectType = 'Contact'];

		Contact contactUser = new Contact ( LastName = 'Test', FirstName = 'User', RecordTypeId = employee.Id, User_Link__c = UserInfo.getUserId() );
		insert contactUser;
        
        account = new Account();
        account.name = 'test';
        insert account;
        
        
        project = new Project__c();
        project.name = 'Test project';
        project.account__c = account.Id;
        insert project;
        
        work = new Work__c();
        work.Fortimize_Project__c = project.Id;
        insert work;
        
        workTask = new Work_Task__c();
        workTask.Allocated_Hours__c = 100;
        //workTask.Assigned_to__c = UserInfo.getUserId();
		workTask.Employee_Assigned_to__c = contactUser.Id;
        workTask.Task_Type__c = 'Other';
        workTask.Work__c = work.Id;
        workTask.Bill_Rate__c = 100;
        workTask.Task_Subject__c = 'Test Subject';
        insert workTask;
    }
    
    @isTest
    public static void initializeController_TimesheetSaved(){
        MyTimesheetController tc = new MyTimesheetController();
        tc.ensureTimesheet();
        system.assertEquals(true, tc.canSave);
        list<Timesheet__c> tss = [SELECT Id, Work_Week__c, OwnerId FROM TImesheet__c];
        system.assertEquals(1, tss.size());
        system.assertEquals(UserInfo.getUserId(), tss[0].OwnerId);
        system.assertEquals([SELECT Id FROM Work_Week__c WHERE Start_Date__c = :Date.today().toStartOfWeek()][0].Id, tss[0].Work_Week__c);
        
        system.assertEquals(false, tc.allowSubmit);
        
        // add a timesheet task.
        tc.newTaskIds = new list<Id>();
        tc.newTaskIds.add(workTask.Id);
        tc.addWorkTaskLines();
        
        system.assertEquals(true, tc.allowSubmit);
        
        tc.timesheet.taskLines[0].timeLogs.get(Date.today()).Hours_Completed__c = 8;
        tc.timesheet.taskLines[0].timeLogs.get(Date.today()).External_Comments__c = 'Test';
        tc.saveLines();
        
        system.assertEquals(8, tc.totalHoursForTheWeek);
        
        list<Time_Log__c> logs = [SELECT Id, Hours_Completed__c, External_Comments__c FROM Time_Log__c];
        system.assertEquals(1, logs.size());
        system.assertEquals(tc.timesheet.taskLines[0].timeLogs.get(Date.today()).Hours_Completed__c, logs[0].Hours_Completed__c);
        system.assertEquals(tc.timesheet.taskLines[0].timeLogs.get(Date.today()).External_Comments__c, logs[0].External_Comments__c);
        
        tc.deleteRowId = tc.timesheet.taskLines[0].timesheetTaskId;
        tc.deleteRowFromCurrentView();
        list<Time_Log__c> logs2 = [SELECT Id, Hours_Completed__c, External_Comments__c FROM Time_Log__c];
        system.assertEquals(true, logs2.isEmpty());
        
        tc.nextView();
        
        tc.previousView();
        
        system.assertEquals(false, tc.allowSubmit);
        system.assertEquals(true, tc.canSave);
    }
    
    @isTest
    static void testSaveLines(){
        MyTimesheetController tc = new MyTimesheetController();
        tc.ensureTimesheet();
        
        // add a timesheet task.
        tc.newTaskIds = new list<Id>();
        tc.newTaskIds.add(workTask.Id);
        tc.addWorkTaskLines();
        
        tc.timesheet.taskLines[0].timeLogs.get(Date.today()).Hours_Completed__c = 2; // change the hours.
        tc.saveLines();
        
        list<Time_Log__c> logs = [
            SELECT 
            	Id, 
            	Hours_Completed__c, 
            	External_Comments__c,
            	Work_Task__c,
            	Work_Task__r.Hour_Type__c,
            	Work_Task__r.Reference_Name__c,
            	Work_Task__r.Project_Name__c,
            	Work_Task__r.Work__r.Fortimize_Project__c,
            	Work_Task__r.Assigned_to__r.FirstName,
            	Work_Task__r.Assigned_to__r.LastName,
				Work_Task__r.Employee_Assigned_to__r.FirstName,
				Work_Task__r.Employee_Assigned_to__r.LastName,
            	Work_Task__r.Remaining_Hours__c,
            	Work_Task__r.Task_Type__c,
            	Status__c,
            	Date__c
            FROM Time_Log__c];
        system.assertEquals(1, logs.size());
        
        WorkTaskLine wtl = new WorkTaskLine(logs[0]);
        system.assertEquals(tc.timesheet.taskLines[0].timeLogs.get(Date.today()).Hours_Completed__c, wtl.timeLogs.get(Date.today()).Hours_Completed__c);
        system.assertEquals(tc.timesheet.taskLines[0].timeLogs.get(Date.today()).Hours_Completed__c, logs[0].Hours_Completed__c);
        system.assertEquals(tc.timesheet.taskLines[0].timeLogs.get(Date.today()).External_Comments__c, wtl.timeLogs.get(Date.today()).External_Comments__c);
        system.assertEquals(tc.timesheet.taskLines[0].timeLogs.get(Date.today()).External_Comments__c, logs[0].External_Comments__c);
        system.assertEquals(true, tc.allowSubmit);
        system.assertEquals(true, tc.canSave);
    }
    
    @isTest
    static void testSaveAndSubmit(){
        MyTimesheetController tc = new MyTimesheetController();
        tc.ensureTimesheet();
        
        // add a timesheet task.
        tc.newTaskIds = new list<Id>();
        tc.newTaskIds.add(workTask.Id);
        tc.addWorkTaskLines();
        
        tc.timesheet.taskLines[0].timeLogs.get(Date.today()).Hours_Completed__c = 2; // change the hours.
        tc.saveAndSubmit();
        
        list<Time_Log__c> logs = [SELECT Id, Hours_Completed__c, External_Comments__c FROM Time_Log__c];
        system.assertEquals(1, logs.size());
        system.assertEquals(tc.timesheet.taskLines[0].timeLogs.get(Date.today()).Hours_Completed__c, logs[0].Hours_Completed__c);
        system.assertEquals(tc.timesheet.taskLines[0].timeLogs.get(Date.today()).External_Comments__c, logs[0].External_Comments__c);
        // assert the timesheet task is approved.
        Timesheet_Task__c ttRes = [SELECT Id, Status__c FROM Timesheet_Task__c WHERE Id = :tc.timesheet.taskLines[0].timesheetTaskId];
        system.assertEquals(WorkTaskLine.STATUS_SUBMITTED, ttRes.Status__c);
        
        system.assertEquals(false, tc.allowSubmit);
    }
    
    @isTest
    public static void addLinesTest(){
        MyTimesheetController tc = new MyTimesheetController();
        tc.ensureTimesheet();
        
        // add a timesheet task.
        tc.newTaskIds = new list<Id>();
        tc.newTaskIds.add(workTask.Id);
        tc.addWorkTaskLines();
        
        system.assertEquals(1,tc.timesheet.taskLines.size());
    }
    
    @isTest
    public static void deleteLineTest(){
        MyTimesheetController tc = new MyTimesheetController();
        tc.ensureTimesheet();
        
        // add a timesheet task.
        tc.newTaskIds = new list<Id>();
        tc.newTaskIds.add(workTask.Id);
        tc.addWorkTaskLines();
        
        tc.deleteRowId = tc.timesheet.taskLines[0].timesheetTaskId;
        tc.deleteRowFromCurrentView();
        system.assertEquals(0,tc.timesheet.taskLines.size());
    }
}