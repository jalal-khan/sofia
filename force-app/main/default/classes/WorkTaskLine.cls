public class WorkTaskLine implements comparable{
    public Id workTaskId{get;set;}
    public Id timesheetTaskId{get;set;}
    public string workTaskHourType{get;set;}
    public string workTaskName{get;set;}
    public string workTaskProjectName{get;set;}
    public string workTaskProjectId{get;set;}
    public string assignedTo{get;set;}
    public map<Date,Time_Log__c> timeLogs{get;set;}
    public decimal remainingHours{get;set;}
    public decimal totalHours{get;set;}
    public decimal billableAmount{get;set;}
    public Date startDate{get;set;}
    public string taskType{get;set;}
    public string sortByField{get;set;}
    public boolean canEdit{get;set;}
    public boolean isSelected{get;set;}
    public boolean canApprove{get;set;}
    public string status{get;set;}
    public string comment{get;set;}
    public static string STATUS_OPEN = 'Open';
    public static string STATUS_SUBMITTED = 'Submitted';
    public static string STATUS_REJECTED = 'Rejected';
    public static string STATUS_APPROVED = 'Approved';
    public static string STATUS_INVOICED = 'Invoiced';
    public WorkTaskLine worktaskline{get;set;}
    
    public WorkTaskLine(Timesheet_Task__c tst){
        this.timesheetTaskId = tst.Id;
        this.workTaskId = tst.Work_Task__c;
        this.workTaskHourType = tst.Work_Task__r.Hour_Type__c;
        this.workTaskName = tst.Work_Task__r.Reference_Name__c;
        this.workTaskProjectName = tst.Work_Task__r.Project_Name__c;
        this.workTaskProjectId = tst.Work_Task__r.Work__r.Fortimize_Project__c;
        this.assignedTo =
			String.isNotBlank(tst.Work_Task__r.Assigned_to__r.LastName) ?
				(tst.Work_Task__r.Assigned_to__r.FirstName + ' ' + tst.Work_Task__r.Assigned_to__r.LastName) :
				(tst.Work_Task__r.Employee_Assigned_to__r.FirstName + ' ' + tst.Work_Task__r.Employee_Assigned_to__r.LastName);
        this.totalHours = tst.Hours_Used__c != null ? tst.Hours_Used__c : 0;
        this.remainingHours = tst.Work_Task__r.Remaining_Hours__c;
        this.taskType = tst.Work_Task__r.Task_Type__c;
        this.sortByField = workTaskProjectName + workTaskName;// concatenated string to sort by.
        this.canEdit = tst.Status__c == null || tst.Status__c == STATUS_OPEN || tst.Status__c == STATUS_REJECTED ? true : false;
        this.canApprove = tst.Status__c == STATUS_SUBMITTED;
        timeLogs = new map<Date,Time_Log__c>();
        this.totalHours = tst.Hours_Used__c != null ? tst.Hours_Used__c : 0;
        this.isSelected = false;
        this.status = tst.status__c;
        this.comment = tst.Rejected_Hours_Comment__c;
        try{
            this.billableAmount = tst.Billable_Amount__c;
        }catch(Exception e){
            // not used in current context, do not throw an error.
        }
        try{
            this.startDate = tst.Timesheet__r.Start_Date__c;
        }catch(Exception e){
            // not used in current context, do not throw an error.
        }
    }
    
    public WorkTaskLine(Timesheet_Task__c tst, boolean isAdmin){
        this(tst);
        if(isAdmin && !this.canEdit && tst.Status__c == STATUS_SUBMITTED){
            // an admin can edit a submitted timesheet line
            this.canEdit = true;
        }
    }
    
    public WorkTaskLine(Time_Log__c tl){
        this.workTaskId = tl.Work_Task__c;
        this.workTaskHourType = tl.Work_Task__r.Hour_Type__c;
        this.workTaskName = tl.Work_Task__r.Reference_Name__c;
        this.workTaskProjectName = tl.Work_Task__r.Project_Name__c;
        this.workTaskProjectId = tl.Work_Task__r.Work__r.Fortimize_Project__c;
        this.assignedTo =
			String.isNotBlank(tl.Work_Task__r.Assigned_to__r.LastName) ?
			tl.Work_Task__r.Assigned_to__r.FirstName + ' ' + tl.Work_Task__r.Assigned_to__r.LastName :
			tl.Work_Task__r.Employee_Assigned_to__r.FirstName + ' ' + tl.Work_Task__r.Employee_Assigned_to__r.LastName;
        this.totalHours = 0;
        this.remainingHours = tl.Work_Task__r.Remaining_Hours__c;
        this.taskType = tl.Work_Task__r.Task_Type__c;
        this.sortByField = workTaskProjectName + workTaskName;// concatenated string to sort by.
        this.canEdit = tl.Status__c == null || tl.Status__c == STATUS_OPEN || tl.Status__c == STATUS_REJECTED ? true : false;
        this.canApprove = tl.Status__c == STATUS_SUBMITTED;
        timeLogs = new map<Date,Time_Log__c>();
        timeLogs.put(tl.Date__c,tl);
        if(tl.Hours_Completed__c != null){
            this.totalHours += tl.Hours_Completed__c;
        }
        this.isSelected = false;
        this.status = tl.status__c;
    }

    public WorkTaskLine(Time_Log__c tl, string sortBy){
        this(tl);
        this.sortByField = sortBy;
    }
    
    public void addTime(Time_Log__c tl){
        this.timeLogs.put(tl.Date__c, tl);
        /*if(tl.Hours_Completed__c != null){
            this.totalHours += tl.Hours_Completed__c;
        }*/
    }
    
    // used to add a new line to the existing view - need to create all the time logs.
    public WorkTaskLine(list<Date> workDays, Timesheet_Task__c tst){
        system.debug('tst.Work_Task__c: ' + tst.Work_Task__c);
        this.timeLogs = new map<Date,Time_Log__c>();
        for(Date d: workDays){
            Time_Log__c tl = new Time_Log__c();
            tl.Work_Task__c = tst.Work_Task__c;
            tl.Hours_Completed__c = 0;
            tl.Status__c = 'Open';
            this.timeLogs.put(d,tl);
        }
        Work_Task__c wt = [SELECT Id, Reference_Name__c, Hour_Type__c, Remaining_Hours__c, Project_Name__c, Task_Type__c, Work__r.Fortimize_Project__c FROM Work_Task__c WHERE Id = :tst.Work_Task__c];
        this.workTaskId = wt.Id;
        this.workTaskHourType = wt.Hour_Type__c;
        this.status = tst.Status__c;
        this.timesheetTaskId = tst.Id;
        this.workTaskProjectName = wt.Project_Name__c;
        this.workTaskName = wt.Reference_Name__c;
        this.sortByField = workTaskProjectName + workTaskName;
        this.totalHours = 0;
        this.remainingHours = wt.Remaining_Hours__c;
        this.taskType = wt.Task_Type__c;
        this.workTaskProjectId = wt.Work__r.Fortimize_Project__c;
        this.canEdit = true;
    }
    
    public integer compareTo(Object compareTo){
        WorkTaskLine compareWTL = (WorkTaskLine)compareTo;
        Integer returnValue = 0;
        system.debug('work task stuff ' +this.worktaskname);
        system.debug(compareWTL.worktaskname);
        if (this.workTaskName < compareWTL.workTaskName) returnValue = -1;
        if (this.workTaskName > compareWTL.workTaskName) returnValue = 1;
        system.debug('return value: ' + returnvalue);
        return returnValue;    
    }

}