public with sharing class DealChecklistPrintViewExtension {
	private ApexPages.StandardController ctrl {get; set;}

	public Checklist__c checklist {get; set;}
	public SObject deal {get; set;}

	public list<Internal_Role__c> internalContactRoles {
		get; set;
	}
	public list<ChecklistSection> sections {get; set;}


	// ******************************************************** //
	// ****          Private Controller Properties         **** //
	// ******************************************************** //

	private List<Schema.SObjectField> lookupSObjFields { get; set; }

	private Schema.SObjectField lookupSObjField {
		get {
			if(lookupSObjField == null){
				for(Schema.SObjectField field : this.lookupSObjFields){
					DescribeFieldResult dfr = field.getDescribe();
					if(this.checklist.get(dfr.getName()) != null){
						lookupSObjField = field;
						break;
					}
				}
			}
			return lookupSObjField;
		}
		set;
	}

	public String clObjAPIName {
		get{
			if(clObjAPIName == null){
				clObjAPIName = this.lookupSObjField.getDescribe().getName();
			}
			return clObjAPIName;
		}
		private set;
	}

	private String clObject {
		get {
			if(clObject == null){
				clObject = this.lookupSObjField.getDescribe().getReferenceTo()[0].getDescribe().getLabel();
			}
			return clObject;
		}
		set;
	}

	private String intRoleRelationName {
		get {
			if(intRoleRelationName == null){
				List<Schema.ChildRelationship> children = this.lookupSObjField.getDescribe().getReferenceTo()[0].getDescribe().getChildRelationships();
				for(Schema.ChildRelationship child : children){
					if(child.getChildSObject() == Internal_Role__c.SObjectType){
						intRoleRelationName = child.getRelationshipName();
						break;
					}
				}
			}
			return intRoleRelationName;
		}
		set;
	}

	public DealChecklistPrintViewExtension(ApexPages.StandardController sctrl) {
		Map<String,Schema.SObjectField> fields = Checklist__c.sObjectType.getDescribe().fields.getMap();
		Schema.FieldSet fieldset = Checklist__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		List<DescribeFieldResult> dfrs = new List<DescribeFieldResult>();
		List<Schema.SObjectField> objFields = new List<Schema.SObjectField>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			Schema.SObjectField sof = fields.get(fsm.getFieldPath());
			DescribeFieldResult dfr = sof.getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
				objFields.add(sof);
			}
		}
		List<String> lookupFields = new List<String>();
		if(dfrs.size() > 0) {
			for(DescribeFieldResult dfr : dfrs){
				lookupFields.add(dfr.getName());
			}
		}
		this.lookupSObjFields = objFields;
		this.ctrl = sctrl;
		if (Test.isRunningTest() == false) {
			sctrl.addFields(lookupFields);
		}
		this.checklist = (Checklist__c)sctrl.getRecord();
		this.loadDealDetails(); // load deal, externalContactRoles, internalContactRoles
		this.loadChecklist(); // load sections and items

	}

	public class ChecklistSection {
		public string name {get; set;}
		public list<ChecklistItem> items {get; set;}

		public ChecklistSection(string name) {
			this.name = name;
			this.items = new list<ChecklistItem>();
		}

		public void addItem(Checklist_Entry__c item, map<string, string> roleMap) {
			this.items.add(new ChecklistItem(item, roleMap));
		}
	}

	public class ChecklistItem {
		public string name {get; set;}
		public string status {get; set;}
		public Date dueDate {get; set;}
		public string roleName {get; set;}
		public string notes {get; set;}

		public ChecklistItem(Checklist_Entry__c entry, map<string,string> roleMap) {
			this.name = entry.name;
			this.status = entry.Status__c;
			this.dueDate = entry.Due_Date__c;
			this.roleName = entry.Internal_Role__c;
			if (this.roleName == null) {
				this.roleName = '(Unassigned)';
			}
			else if (this.roleName == 'User') {
				if (entry.Specific_User__c == null) {
					this.rolename = '(Unassigned)';
				}
				else {
					this.roleName = entry.Specific_User__r.name + ' (Non-role assignment)';
				}
			}
			else {
				string roleUser = roleMap.get(this.roleName);
				if (roleUser != null) {
					this.roleName += ' (' + roleUser +')';
				}
			}
			this.notes = entry.Notes__c;
		}

	}

	private void loadDealDetails() {
		try {
			Map<String,Schema.SObjectField> fields = Internal_Role__c.sObjectType.getDescribe().fields.getMap();
			Schema.FieldSet fieldset = Internal_Role__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
			List<DescribeFieldResult> dfrs = new List<DescribeFieldResult>();
			List<Schema.SObjectField> objFields = new List<Schema.SObjectField>();
			List<String> lookupFields = new List<String>();
			for(Schema.FieldSetMember fsm : fieldset.getFields()){
				Schema.SObjectField sof = fields.get(fsm.getFieldPath());
				DescribeFieldResult dfr = sof.getDescribe();
				List<Schema.SObjectType> types = dfr.getReferenceTo();
				if(types != null && types.size() > 0){
					dfrs.add(dfr);
					objFields.add(sof);
					lookupFields.add(dfr.getName()+' =: temp');
				}
			}

			String query = 'select id, name '
					+ 'from '+ this.lookupSObjField.getDescribe().getReferenceTo()[0].getDescribe().getName() +' where id = \''
					+ this.checklist.get(clObjAPIName) + '\'';
			List<SObject> objs = Database.query(query);
			Id temp = objs[0].Id;
			List<Internal_Role__c> irs = Database.query('select Role__c, Internal_Team_Member__r.name from Internal_Role__c where '+String.join(lookupFields, ' OR '));
			this.deal = objs[0];
			this.internalContactRoles = irs;
		}
		catch (Exception e) {
			system.debug(e);  //this should never happen; logging is sufficient
		}
	}

	private void loadChecklist() {
		map<string, ChecklistSection> sectionMap = new map<string, ChecklistSection>();
		map<string, string> roleMap = this.getRoleMap();
		for (Checklist_Entry__c entry : [
				select id, Name, Status__c, Internal_Role__c, Due_Date__c, checklist__c, Notes__c,
							 Template__c, Template__r.Section__c, Template__r.Due_Date_Based_On__c, Template__r.Due_Date_Offset__c,
			 				 Specific_User__c, Specific_User__r.Name
					from Checklist_Entry__c
				 where Checklist__c = :this.checklist.Id
			order by Template__r.Sort_Order__c
		]) {
			if (sectionMap.containsKey(entry.Template__r.section__c) == false) {
				sectionMap.put(entry.Template__r.section__c, new ChecklistSection(entry.Template__r.Section__c));
			}
			sectionMap.get(entry.Template__r.section__c).addItem(entry, roleMap);
		}
		this.sections = sectionMap.values();

	}

	private map<string, string> getRoleMap() {
		map<string,string> res = new map<string,string>();
		for (Internal_Role__c dcr : this.internalContactRoles) {
			res.put(dcr.Role__c, dcr.Internal_Team_Member__r.name);
		}
		return res;
	}
}