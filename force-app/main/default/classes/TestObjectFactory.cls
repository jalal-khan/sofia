@isTest
public class TestObjectFactory {
    
    public static integer random = 1;
    
    public static string getRandom(string prefix){
        random += 1;
        return prefix + random;
    }
    
    public static list<Account> getAccounts(integer howMany){
        list<Account> accs = new list<Account>();
        for(integer i = 0; i < howMany; i++){
            accs.add(getAccount());
        }
        return accs;
    }
    
    public static string userSuffix = UserInfo.getUserId().right(5);
    
    public static Account getAccount(){
        return new Account(
            Name = getRandom('Account ') + userSuffix,
            BillingCity = 'city',
            BillingStreet = 'street',
            BillingPostalCode = '12345',
            BillingState = 'NY'
        );
    }

    public static Contact getContact(){
        return new Contact(
            FirstName = getRandom('Fname ') + userSuffix,
            LastName = getRandom('Contact ') + userSuffix,
            Email = userSuffix + '@testing.com'
        );
    }
    
    public static Opportunity getOpportunity(){
        return new Opportunity(
            Name = getRandom('Opportunity ') + userSuffix,
            StageName = Opportunity.StageName.getDescribe().getPicklistValues()[0].getValue(),
            CloseDate = Date.today()
        );
    }

    public static list<Project__c>getProjects(list<Account> accounts, integer howManyPerAccount){
        list<Project__c> projects = new list<Project__c>();
        for(Account a:accounts){
            for(integer i = 0; i < howManyPerAccount; i++){
                projects.add(getProject(a));
            }
        }
        return projects;
    }
    
    public static Project__c getProject(Account a){
        return new Project__c(
            Account__c = a.Id,
            Name = getRandom('Project ') + ' ' + userSuffix + ' '
        );
    }
    
    public static Work__c getWork(Project__c p, string hourType, Id rtId, string needToId){
        return new Work__c(
            User_Story__c = getRandom('story'),
            Fortimize_Project__c = p.Id,
            Hour_Type__c = hourType,
            RecordTypeId = rtId,
            As_a__c = getRandom('As a '),
            I_need_to__c = needToId
        );
    }
    
    public static Work_Task__c getWorkTask(Work__c w,Decimal hours, Decimal billRate, Id userId, string nameIdentifier){
        return new Work_Task__c(
            Role__c = 'Solution Architect (SA)',
            Bill_Rate__c = billRate,
            Task_Subject__c = getRandom(nameIdentifier + ' - '),
            Allocated_Hours__c = hours,
            Work__c = w.Id,
            Assigned_To__c = userId
        );
    }

    public static void setupGanttChart() {
        Date tDay = system.today();
        Id empRT = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND DeveloperName = 'Employee'].Id;
        
        Contact c = new Contact(LastName = 'test', Email = 'test@test.com', FO_Employment_Status__c = 'Active', RecordTypeId = empRT);
        c.Start_Date__c = tDay;
        c.User_Link__c = UserInfo.getUserId();
        insert c;

        //FMZ_Timesheet__c timesheet = new FMZ_Timesheet__c();
        //insert timesheet;
        TimesheetService.createTimesheetsByDate(new Set<Id> { c.Id }, tDay.toStartOfWeek(), 1);
        //toStartOfWeek 

        FMZ_Project__c project = new FMZ_Project__c(Name='Test', Display_for_Resource_Management__c=true);
        insert project;
        
        FMZ_Role__c role = new FMZ_Role__c(Name='Test',Role_Abbreviation__c='TST',Effective_Start_Date__c=system.today(),List_Price__c=200,Active__c=true);
        insert role;

        FMZ_Project_Role__c project_role = new FMZ_Project_Role__c(FMZ_Project__c = project.Id, SOW_Allocated_Hours__c=10, SOW_Hourly_Rate__c=0, FMZ_Role__c=role.Id);
        insert project_role;

        FMZ_Resource_Assignment__c assignment = new FMZ_Resource_Assignment__c(FMZ_Resource__c = c.Id, FMZ_Project_Role__c = project_role.Id, Assignment_Start_Date__c=tDay, Assignment_End_Date__c=tDay.addDays(14), Status__c='Confirmed', Weekly_Hours_Allocation__c=10);
        insert assignment;
    }
}