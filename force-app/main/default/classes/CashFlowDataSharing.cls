/*Created by Andrew Hawkins on July 5, 2019
 * Version 1.00.000:
 * 	- newDataSharing: new method that takes newly created Cash Flow Data and shares it
 *	- userSharing: new method that takes newly created or updated Users and shares the Cash Flow Data with them
 *                 note: create/update the custom metadata before creating/updating the user
 * 	- updateDataSharing: new method that updates sharing when a Cash Flow Data record is updated
 * 	- updateContactSharing: new method that updates the sharing of all attached Cash Flow Data records if the Employee 
 * 							Department is updated
 * 	- deleteOldSharing: new method that deletes sharing related to Cash Flow Data whose Ids are provided
 * 	- getCashFlowData: new method that converts a list of Cash Flow Data Ids into a List of Cash Flow Data that includes
 * 					   the Contact's Employee Department
 * 	- handlerSharing: new method that takes the output from getCashFlowData and any new/updated User Ids to generate
 * 					  the sharing records
 * 	- getDepartmentIdMap: new method that takes a list of User Ids to generate a full or partial map of the departments
 * 						  to the Users that need to be shared for each department.
 * 
 * Version 1.00.001 (July 10, 2019):
 * 	- getDepartmentIDMap: refactored the loop to condense the code. now pulls fields dynamically.
 * 
 * Version 1.00.002 (July 11, 2019):
 * 	- userSharing: made into future method to remove issue with updating users.
*/
public class CashFlowDataSharing {
    
    public static void newDataSharing(List<Cash_Flow_Data__c> newData){
        List<Id> cfdIds = new List<Id>();
        for(Cash_Flow_Data__c cfd : newData){
            cfdIds.add(cfd.Id);
        }
        
        List<Cash_Flow_Data__c> cfdWithContact = getCashFlowData(cfdIds);
        
        handleSharing(cfdWithContact,new List<Id>());
    }
    
    @future
    public static void userSharing(List<Id> userIds, Boolean isUpdate){
        List<Id> userIdsList = new List<Id>();
        List<Id> cfdIds = new List<Id>();
        Set<Id> userIdsSet = new Set<Id>();
        
        if(isUpdate){
                List<Cash_Flow_Data__Share> cfdSharing = [SELECT Id FROM Cash_Flow_Data__Share
                                                          WHERE UserOrGroupId IN: userIds
                                                         AND RowCause =: Schema.Cash_Flow_Data__Share.RowCause.Executive__c];
                if(cfdSharing.size() > 0){
                    DELETE cfdSharing;
                }
            }
        
        Map<String,List<Id>> departmentIdMap = getDepartmentIdMap(userIds);
        if(departmentIdMap.size() > 0){
            List<String> departments = new List<String>(departmentIdMap.keySet());
            for(String s : departments){
                for(Id i : departmentIdMap.get(s)){
                    userIdsSet.add(i);
                }
            }
            userIdsList.addAll(userIdsSet);
            
            List<Contact> contactsWithCFD = [SELECT Id, (SELECT Id FROM Cash_Flow_Data__r) 
                                             FROM Contact WHERE Employee_Department__c IN: departments];
        
            for(Contact c : contactsWithCFD){
                for(Cash_Flow_Data__c cfd : c.Cash_Flow_Data__r){
                    cfdIds.add(cfd.Id);
                }
            }
            
            List<Cash_Flow_Data__c> cfdWithContact = getCashFlowData(cfdIds);
        
        	handleSharing(cfdWithContact, userIdsList);
        }
    }
    
    public static void updateDataSharing(List<Cash_Flow_Data__c> newData, Map<Id,Cash_Flow_Data__c> oldDataMap){
        List<Id> cfdIds = new List<Id>();
        for(Cash_Flow_Data__c cfd : newData){
            if(cfd.Department__c != oldDataMap.get(cfd.Id).Department__c){
                cfdIds.add(cfd.Id);
            }
        }
        
        deleteOldSharing(cfdIds);
        
        List<Cash_Flow_Data__c> cfdWithContact = getCashFlowData(cfdIds);
        
        handleSharing(cfdWithContact, new List<Id>());
    }
    
    public static void updateContactSharing(List<Contact> newContacts, Map<Id,Contact> oldContactMap){
        List<Id> contactIds = new List<Id>();
        List<Id> cfdIds = new List<Id>();
        for(Contact c : newContacts){
            if(c.Employee_Department__c != oldContactMap.get(c.Id).Employee_Department__c){
                contactIds.add(c.Id);
            }
        }
        
        List<Contact> contactsWithCFD = [SELECT Id, (SELECT Id FROM Cash_Flow_Data__r) FROM Contact WHERE Id IN: contactIds];
        
        for(Contact c : contactsWithCFD){
            for(Cash_Flow_Data__c cfd : c.Cash_Flow_Data__r){
                cfdIds.add(cfd.Id);
            }
        }
        
        deleteOldSharing(cfdIds);
        
        List<Cash_Flow_Data__c> cfdWithContact = getCashFlowData(cfdIds);
        
        handleSharing(cfdWithContact, new List<Id>());
    }
    
    private static void deleteOldSharing(List<Id> cfdIds){
        List<Cash_Flow_Data__Share> cfdSharing = [SELECT Id FROM Cash_Flow_Data__Share WHERE ParentId IN: cfdIds 
                                                 AND RowCause =: Schema.Cash_Flow_Data__Share.RowCause.Executive__c];
        if(cfdSharing.size() > 0){
            DELETE cfdSharing;
        }
    }
    
    private static List<Cash_Flow_Data__c> getCashFlowData(List<ID> cfdIds){
        List<Cash_Flow_Data__c> cfdWithContact = [SELECT Id, Employee_Contractor__c, Department__c, 
                                                  Employee_Contractor__r.Employee_Department__c
                                                  FROM Cash_Flow_Data__c WHERE Id IN: cfdIds];
        
        return cfdWithContact;
    }
    
    private static void handleSharing(List<Cash_Flow_Data__c> dataWithContact,List<Id> userId){
        Set<Cash_Flow_Data__Share> shareSet = new Set<Cash_Flow_Data__Share>();
        List<Cash_Flow_Data__Share> shareList = new List<Cash_Flow_Data__Share>();
        Map<String,List<Id>> departmentIdMap = getDepartmentIdMap(userId);
        
        for(Cash_Flow_Data__c cfd : dataWithContact){
            String dept = cfd.Employee_Contractor__r.Employee_Department__c;
            if(cfd.Department__c != dept && departmentIdMap.get(dept) != null){
                for(Id i : departmentIdMap.get(dept)){
                    if(i != null){
                        Cash_Flow_Data__Share temp = new Cash_Flow_Data__Share(ParentId = cfd.Id, userOrGroupId = i,
                                                                               AccessLevel = 'Read', 
                                                                               RowCause = Schema.Cash_Flow_Data__Share.RowCause.Executive__c);                    
                        shareSet.add(temp);
                    }
                }
            }
            if(departmentIdMap.get(cfd.Department__c) != null){
                for(Id i : departmentIdMap.get(cfd.Department__c)){
                    if(i != null){
                        Cash_Flow_Data__Share temp = new Cash_Flow_Data__Share(ParentId = cfd.Id, userOrGroupId = i,
                                                                               AccessLevel = 'Read',
                                                                               RowCause = Schema.Cash_Flow_Data__Share.RowCause.Executive__c);                    
                        shareSet.add(temp);
                    }
                }
            }
        }
        
        shareList.addAll(shareSet);
        if(shareList.size() > 0){
            system.debug(shareList);
            INSERT shareList;
        }
    }
    
    private static Map<String,List<Id>> getDepartmentIdMap(List<Id> userId){
        Map<String,List<Id>> departmentIdMap = new Map<String,List<Id>>();
        List<CashFlowSharing__c> cfdm = new List<CashFlowSharing__c>();
        List<CashFlowSharing__c> metadata = [SELECT Name, BAT__c, Executive__c, Finance__c,
                                                 HR__c, Legal__c, Professional_Services__c, Revenue__c,
                                                 User_Id__c, isDeleted FROM CashFlowSharing__c];
        
        Map<String,Schema.SObjectField> sharingFields = Schema.getGlobalDescribe().get('CashFlowSharing__c').getDescribe().fields.getMap();
        
        for(CashFlowSharing__c cfs : metadata){
            if(userId.size() > 0){
                for(Id i : userId){
                    if(cfs.User_Id__c == i){
                    	cfdm.add(cfs);
                	}
                }
            }else{
                cfdm.add(cfs);
            }
        }
        
        for(CashFlowSharing__c cfs : cfdm){
            system.debug(cfs);
            for(Schema.SObjectField sobj : sharingFields.values()){
                if(sobj.getDescribe().getType() == Schema.DisplayType.BOOLEAN){
                    if(boolean.valueOf(cfs.get(sobj.getDescribe().getName()))){
                        String label = sobj.getDescribe().getLabel();
                        if(!departmentIdMap.keySet().contains(label)){
                            List<Id> temp = new List<Id>();
                            temp.add(cfs.User_Id__c);
                            departmentIdMap.put(label, temp);
                        }else{
                            List<Id> temp = departmentIdMap.get(label);
                            temp.add(cfs.User_Id__c);
                            departmentIdMap.put(label,temp);
                		}
                    }
                }
            }
        }
        system.debug(departmentIdMap);
        return departmentIdMap;
    }
}