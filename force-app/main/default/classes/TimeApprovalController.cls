public class TimeApprovalController {
    
    public Date startDate{get;set;}
    public Work_Week__c workWeek{get;set;}
    public list<Date> workDays{get;set;}
    public list<String> workDaysString{get;set;}
    public Id activeProject{get;set;}
    public Id viewingAs{get;set;}
    public boolean isApprovalManager{
        get{
            if(isApprovalManager==null){
                if(ApexPages.currentPage().getParameters().get('adminView')!=null){
                    isApprovalManager = true;
                }else{
                    isApprovalManager = [SELECT Manage_Time_Approvals__c FROM User WHERE Id = :UserInfo.getUserId()][0].Manage_Time_Approvals__c;
                }
            }
            return isApprovalManager;
        }set;
    }
    public void changeUser(){
        activeProject = null;
        activeProjectRecord = null;
        doSetup();
    }
    public list<SelectOption> pms{
        get{
            if(pms == null){
                pms = new list<SelectOption>();
                for(User u:[SELECT Id, FirstName, LastName FROM User WHERE isActive = true ORDER BY FirstName ASC]){
                    pms.add(new SelectOption(u.Id,u.FirstName + ' ' + u.LastName));
                }
            }
            return pms;
        }set;
    }
    public TimeApprovalController(){
        this.viewingAs = UserInfo.getUserId();
        this.startDate = Date.today().toStartOfWeek();
        doSetup();
    }
    public void nextView(){
        this.startDate = this.startDate.addDays(7);
        doSetup();
    }
    
    public void previousView(){
        this.startDate = this.startDate.addDays(-7);
        doSetup();
    }
    
    public void approveLines(){
            try{
                activeProjectRecord.approveHours();
                activeProjectRecord = getProjectDetail(this.workDays, activeProjectRecord.projectId, UserInfo.getUserId());
                if(activeProjectRecord.totalHours == 0){
                    unsetActiveProject();
                }
                doSetup();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM ,'Hours Approved.'));
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,e.getMessage()));
            }
    }
    public void setActiveProject(){
        this.activeProjectRecord = getProjectDetail(this.workDays, activeProject, UserInfo.getUserId());
        system.debug('activProject: ' + activeProject);
    }
    public void unsetActiveProject(){
        this.activeProject = null;
        this.activeProjectRecord = null;
        doSetup();
    }
    
    public string rejectReason{get;set;}
    public void rejectLines(){
        system.debug('activProject: ' + activeProject);
        ProjectWrapper pActive;
        if(rejectReason==null){
            throw new CustomException('You must enter a reason for rejecting hours.');
        }
            try{
                activeProjectRecord.rejectHours(rejectReason);
                if(activeProjectRecord.totalHours == 0){
                    unsetActiveProject();
                }
                doSetup();
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR ,e.getMessage()));
            }
    }
    
    public void doSetup(){
        Date sd = this.startDate;
        system.debug('sd::: ' + sd);
        // get the current work week in scope:
        this.workWeek = [SELECT Id FROM Work_Week__c WHERE Start_Date__c = :sd][0];
        
        this.workDays = new list<Date>();
        this.workDaysString = new list<String>();
        for(integer i = 0;i<7;i++){
            this.workDays.add(this.startDate.addDays(i));
            this.workDaysString.add(this.startDate.addDays(i).format());
        }
        if(activeProject != null){
            this.activeProjectRecord = getProjectDetail(workDays, activeProject, UserInfo.getUserId());
        }else{
            // populate the projects to view
            // only projects with unapproved hours will be returned.
            this.projects = populateProjects(this.workWeek, UserInfo.getUserId());
            this.projects.sort();
        }
    }
    
    public list<ProjectWrapper> projects{get;set;}
    
    public list<ProjectWrapper> populateProjects(Work_Week__c ww, Id userId){
        list<ProjectWrapper> projectList = new list<ProjectWrapper>();
        map<Id, ProjectWrapper> projectMap = new map<Id, ProjectWrapper>();
        list<AggregateResult> aggRes = [SELECT SUM(Hours_Used__c) hrs, 
                              Work_Task__r.Work__r.Fortimize_Project__r.Id pId, 
                              Work_Task__r.Work__r.Fortimize_Project__r.Name pName,
                                         Status__c
                              FROM Timesheet_Task__c 
                              WHERE Timesheet__r.Work_Week__c = :ww.Id AND Work_Task__r.Work__r.Fortimize_Project__r.Project_Manager__c = :this.viewingAs
                              AND Status__c != 'Approved' AND Status__c != 'Invoiced'
                             GROUP BY Work_Task__r.Work__r.Fortimize_Project__r.Id, Work_Task__r.Work__r.Fortimize_Project__r.Name, Status__c];
        for(AggregateResult ar : aggRes){
            if(!projectMap.containsKey((Id)ar.get('pId'))){
                projectMap.put((Id)ar.get('pId'), new ProjectWrapper((string)ar.get('pName'),(string)ar.get('pId')));
            }
            projectMap.get((Id)ar.get('pId')).addStatusHours((string)ar.get('Status__c'), (decimal)ar.get('hrs'));
        }
        projectList = projectMap.values();
        projectList.sort();
        return projectList;
    }
    
    public ProjectWrapper getProjectDetail(list<Date> dates, Id projectId, Id userId){
        Project__c p = [SELECT Id, Name FROM Project__c WHERE Id = :projectId];
        ProjectWrapper pr = new ProjectWrapper(p);
        pr.getTimesheetTasks(this.workWeek, dates);
        return pr;
        
    }
    
    public ProjectWrapper activeProjectRecord{get;set;}
    
    public class ProjectWrapper implements comparable{
        public string projectName{get;set;}
        public Id projectId{get;set;}
        public list<WorkTaskLine> workTaskLines{get;set;}
        public list<WorkTaskLine> openWorkTaskLines{get;set;}
        public list<WorkTaskLine> rejectedWorkTaskLines{get;set;}
        public double totalHours{get;set;}
        public map<Id,WorkTaskLine> tMap{get;set;}
        public decimal rejectedHours{get;set;}
        public decimal submittedHours{get;set;}
        public decimal openHours{get;set;}
        
        public ProjectWrapper(Project__c p){
            this.projectName = p.Name;
            this.projectId = p.Id;
            this.submittedHours = 0;
            this.openHours = 0;
            this.rejectedHours = 0;
            this.workTaskLines = new list<WorkTaskLine>();
            this.openWorkTaskLines = new list<WorkTaskLine>();
            this.rejectedWorkTaskLines = new list<WorkTaskLine>();
            this.tMap = new map<Id, WorkTaskLine>();
        }
        public ProjectWrapper(string pName, string pId){
            this.projectName = pName;
            this.projectId = pId;
        }
        
        public void getTimesheetTasks(Work_Week__c ww, list<Date> dates){
            list<Timesheet_Task__c> tasks = [SELECT Id, Timesheet__r.Work_Week__c, Hours_Used__c, Work_Task__c, Status__c, Rejected_Hours_Comment__c,
                                             Work_Task__r.Remaining_Hours__c, Work_Task__r.Allocated_Hours__c, Work_Task__r.Project_Name__c, Work_Task__r.Hour_Type__c,
                                             Work_Task__r.Assigned_to__r.FirstName, Work_Task__r.Assigned_to__r.LastName, 
                                             Work_Task__r.Employee_Assigned_to__r.FirstName, Work_Task__r.Employee_Assigned_to__r.LastName,
                                             Work_Task__r.Reference_Name__c, 
                                             Work_Task__r.Task_Type__c, Work_Task__r.Work__r.Fortimize_Project__c, Work_Task__r.Work__r.Fortimize_Project__r.Id,
                                             Work_Task__r.Work__r.Fortimize_Project__r.Name
                                             FROM Timesheet_Task__c 
                                             WHERE Timesheet__r.Work_Week__c = :ww.Id
                                             AND Work_Task__r.Work__r.Fortimize_Project__c = :projectId];
            for(Timesheet_Task__c tt:tasks){
                tMap.put(tt.Id, new WorkTaskLine(tt));
            }
            
            // this loop populates all the time logs that already exist
            for(Time_Log__c log : [SELECT Id, Hours_Completed__c, Date__c, Work_Week__c, Internal_Comments__c, External_Comments__c , Status__c, 
                                   Timesheet_Task__c, Timesheet_Task__r.Status__c
                                   FROM Time_Log__c
                                   WHERE Timesheet_Task__c IN :tMap.keySet()
                                   AND Work_Task__r.Work__r.Fortimize_Project__c = :projectId
                                   AND Timesheet_Task__r.Status__c != 'Approved']){
                                       tMap.get(log.Timesheet_Task__c).addTime(log);
                                   }
            
            // fill out blank time log entries if needed?
            for(Date dt:dates){
                for(WorkTaskLine wtl:tMap.values()){
                    if(!wtl.timeLogs.containsKey(dt)){
                        Time_Log__c tl = new Time_Log__c();
                        tl.Date__c = dt;
                        tl.Hours_Completed__c = 0;
                        wtl.addTime(tl);
                    }
                }
            }
            this.splitTasks();
        }
        
        public void addStatusHours(string status, decimal hours){
            if(status.containsIgnoreCase('Rejected')){
                this.rejectedHours = hours;
            }else if(status.containsIgnoreCase('Submitted')){
                this.submittedHours = hours;
            }else if(status.containsIgnoreCase('Open')){
                this.openHours = hours;
            }
        }
/*        public ProjectWrapper(Time_Log__c tl){
            this.projectName = tl.Work_Task__r.Work__r.Fortimize_Project__r.Name;
            this.projectId = tl.Work_Task__r.Work__r.Fortimize_Project__r.Id;
            this.workTaskLines = new list<WorkTaskLine>();
            this.openWorkTaskLines = new list<WorkTaskLine>();
            this.rejectedWorkTaskLines = new list<WorkTaskLine>();
            this.selectedLines = new map<Id,WorkTaskLine>();
            this.openHours = 0;
            this.rejectedHours = 0;
            this.submittedHours = 0;
            tMap = new map<Id,WorkTaskLine>();
            tMap.put(tl.Work_Task__c, new WorkTaskLine(tl, tl.Work_Task__r.Assigned_to__r.LastName));
        }
        public void addTimeLog(Time_Log__c tl){
            if(!tMap.containsKey(tl.Work_Task__c)){
                tMap.put(tl.Work_Task__c, new WorkTaskLine(tl));
            }else{
                tMap.get(tl.Work_Task__c).addTime(tl);
            }
        }
*/        public void splitTasks(){
            for(WorkTaskLine wtl:tMap.values()){
                if(wtl.status == WorkTaskLine.STATUS_OPEN){
                    openWorkTaskLines.add(wtl);
                    this.openHours += wtl.totalHours;
                }else if(wtl.status == WorkTaskLine.STATUS_REJECTED){
                    rejectedWorkTaskLines.add(wtl);
                    this.rejectedHours += wtl.totalHours;
                }else if(wtl.status == WorkTaskLine.STATUS_SUBMITTED){
                    workTaskLines.add(wtl);
                    this.submittedHours += wtl.totalHours;
                }
            }
            this.openWorkTaskLines.sort();
            this.rejectedWorkTaskLines.sort();
            this.workTaskLines.sort();
        }
        public integer compareTo(Object compareTo){
            ProjectWrapper compareP = (ProjectWrapper)compareTo;
            if (this.totalHours == compareP.totalHours) return 0;
            if (this.totalHours > compareP.totalHours) return 1;
            return -1;    
        }
        public void approveHours(){
            // first, validate there are selected lines.
            list<WorkTaskLine> selected = new list<WorkTaskLine>();
            for(WorkTaskLine wtl:this.workTaskLines){
                if(wtl.isSelected){
                    selected.add(wtl);
                }
            }
            if(selected.isEmpty()){
                throw new CustomException('You must select hours to approve.');
            }else{
                // set them all for approval.
                setWorkTaskStatus(selected,WorkTaskLine.STATUS_APPROVED, null);
            }
        }
        
        public void rejectHours(string reason){
            // first, validate there are selected lines.
            list<WorkTaskLine> selected = new list<WorkTaskLine>();
            for(WorkTaskLine wtl:this.workTaskLines){
                if(wtl.isSelected){
                    selected.add(wtl);
                }
            }
            if(selected.isEmpty()){
                throw new CustomException('You must select rows in the table.');
            }else{
                // set them all for approval.
                setWorkTaskStatus(selected,WorkTaskLine.STATUS_REJECTED, reason);
            }
        }
        private void setWorkTaskStatus(list<WorkTaskLine> wtls, string status, string comment){
            // validation
            if(status == WorkTaskLine.STATUS_REJECTED && (comment == null || comment == '')){
                throw new CustomException('You must enter a comment');
            }
            list<Timesheet_Task__c> wtlUpdate = new list<Timesheet_Task__c>();
            for(WorkTaskLine wtl:wtls){
                wtl.status = status;
                wtl.comment = comment;
                Timesheet_Task__c tt = new Timesheet_Task__c(
                    Id = wtl.timeSheetTaskId,
                    Status__c = status,
                    Rejected_Hours_Comment__c = comment
                );
                wtlUpdate.add(tt);
            }
            if(!wtlUpdate.isEmpty()){
                system.debug('updating tasks: ' + wtlUpdate);
                update wtlUpdate;
            }
        }
    }
    
    public class CustomException extends Exception{}
}