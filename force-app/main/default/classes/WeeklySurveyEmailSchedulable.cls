/*Written by Andrew Hawkins on 10/12/2018
 * 
 * Verion 1.0:
 * 			This class emails a survey link specific contacts from active projects.
*/
public class WeeklySurveyEmailSchedulable implements schedulable{

    public Void execute(SchedulableContext sc){
        Map<Id,Project__c> activeProjects = new Map<id,Project__c>([SELECT Id, Project_Manager__r.Name FROM Project__c WHERE Is_Active__c = true]);
        List<Project_Contact_Role__c> contacts = [SELECT Id, Contact__c, Contact__r.Email, Project__c FROM Project_Contact_Role__c WHERE Project__c IN: activeProjects.keySet() AND Role__c = 'Survey Recipient'];
        EmailTemplate et=[Select id from EmailTemplate where name = 'Weekly CSAT Survey' limit 1];
        List<id> contactIds = new List<id>();
        List<Messaging.Email> emails = new List<Messaging.Email>();
        
        
        if(contacts.size() > 0){
            for(Project_Contact_Role__c pcr : contacts){
                if(pcr.Contact__r.Email != null){
                    String[] to = new String[]{pcr.Contact__r.Email};
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateId(et.Id);
                    mail.setToAddresses(to);
                    mail.setSenderDisplayName(activeProjects.get(pcr.Project__c).Project_Manager__r.Name);
                    mail.setWhatId(pcr.Project__c);
                    mail.setTargetObjectId(pcr.Contact__c);
                    emails.add(mail);
                }
            }
            
            Messaging.sendEmail(emails, false);
        }
    }
}