@isTest
private class DealChecklistPrintViewExtension_Test {

	private static final string TEMPLATE_NAME = 'Template 1';
	private static final string TASK_USER_PROFILE = 'Standard User';

	private static list<string> internalDealContactRoles = FMZ_SchemaUtils.GetDependentOptions(
			Internal_Role__c.sObjectType.getDescribe().getName(),
			Internal_Role__c.Object__c.getDescribe().getName(),
			Internal_Role__c.Role__c.getDescribe().getName()).get('Opportunity');

	@testSetup
	private static void setup() {
		list<Checklist_Template__c> templates = CKLST_TestObjectFactory.getChecklistTemplates(3);
		templates[0].name = TEMPLATE_NAME;
		insert templates;

		list<Checklist_Template_Entry__c> entries = CKLST_TestObjectFactory.getChecklistTemplateEntries(templates, 3);
		insert entries;

	}

	@isTest
	private static void testPrintView() {
		Opportunity deal = CKLST_TestObjectFactory.getDealWithDates();
		insert deal;

		list<User> users = CKLST_TestObjectFactory.getUsersForProfile(TASK_USER_PROFILE, internalDealContactRoles.size());
		insert users;

		list<Internal_Role__c> dcrList = CKLST_TestObjectFactory.getInternalDealContactRoles(deal, users);
		insert dcrList;

		Checklist_Template__c template = [select id, name from Checklist_Template__c where name = :TEMPLATE_NAME];

		Checklist__c cklist = new Checklist__c(Opportunity__c = deal.id);
		ApexPages.StandardController sctrl = new ApexPages.StandardController(cklist);
		DealChecklistController ctrl = new DealChecklistController(sctrl);
		System.assertNotEquals(0, ctrl.templateOptions.size());
		ctrl.templateId = template.id;
		ctrl.createChecklist();
		ctrl.saveChecklist();

		list<Checklist__c> res = [
				select id, name, Opportunity__c, Template__c
				  from Checklist__c
				 where Opportunity__c = :deal.id
		];

		Checklist__c existing = res[0];
		sctrl = new ApexPages.StandardController(existing);
		DealChecklistPrintViewExtension pve = new DealChecklistPrintViewExtension(sctrl);
		System.assertEquals(1, pve.sections.size());
		System.assertEquals(3, pve.sections[0].items.size());
		System.assertEquals(internalDealContactRoles.size(), pve.internalContactRoles.size());
		// System.assertEquals(externalDealContactRoles.size(), pve.externalContactRoles.size());
	}
}