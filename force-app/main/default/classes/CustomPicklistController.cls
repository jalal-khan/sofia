public class CustomPicklistController {
    
    @AuraEnabled
    public static List<String> getOptions(string ObjectApi_name, string Field_name){
        return Utils.getPicklistValues(ObjectApi_name, Field_name);
    }
}