public class CashFlowTriggerHandler {
	
    Public Void beforeInsert(List<Cash_Flow_Data__c> newData){
        fillInRoleAndDepartment(newData);
    }
    
    Public void afterInsert(List<Cash_Flow_Data__c> newData){
        CashFlowDataSharing.newDataSharing(newData);
    }
    
    Public Void afterUpdate(List<Cash_Flow_Data__c> newData, Map<Id,Cash_Flow_Data__c> oldDataMap){
        for(Cash_Flow_Data__c cfd : newData){
            if(cfd.Department__c != oldDataMap.get(cfd.id).Department__c){
                CashFlowDataSharing.updateDataSharing(newData, oldDataMap);
                break;
            }
        }
    }
    
    Private Void fillInRoleAndDepartment(List<Cash_Flow_Data__c> newData){
        List<Id> contactIDs = new List<Id>();
        
        for(Cash_Flow_Data__c cfd : newData){
            contactIDs.add(cfd.Employee_Contractor__c);
        }
        
        Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT Id, Employee_Role__c, Employee_Department__c
                                                          FROM Contact WHERE Id IN: contactIDs]);
        
        for(Cash_Flow_Data__c cfd : newData){
            if(cfd.Employee_Contractor__c == null){
                Continue;
            }
            cfd.Role__c = contactMap.get(cfd.Employee_Contractor__c).Employee_Role__c;
            cfd.Department__c = contactMap.get(cfd.Employee_Contractor__c).Employee_Department__c;
        }
    }
}