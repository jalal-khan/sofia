public class FieldSetServices {
    public static string VIEW_TYPE_INPUT = 'input';
    public static string VIEW_TYPE_OUTPUT = 'output';
    
    public static list<FieldDefinition> getFieldDefinitions(list<FieldSetMember> fields){
        list<FieldDefinition> output = new list<FieldDefinition>();
        return null;
    }
    
    public static list<String> getFieldsAsStringList(string objectType, string fieldSetName){
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Schema.FieldSet fs = fsMap.get(fieldSetName);
        List<Schema.FieldSetMember> fieldSet = fs.getFields();
        set<string> fields = new set<string>();
        fields.add('Id'); // always include this.  because reasons.
        for (Schema.FieldSetMember f: fieldSet) {
            // if the field is a reference, add the related field.name field for query.
            fields.add(f.getFieldPath());
            if(string.valueOf(f.getType()).toLowerCase() == 'reference'){
                string fp2 = string.valueOf(f.getFieldPath());
                if(fp2.endsWithIgnoreCase('__c')){
                	fp2 = fp2.removeEnd('c');
                	fp2 += 'r';
                }else{
                    fp2 = fp2.removeEnd('Id');
                }
                fp2 += '.Name';
                fields.add(fp2);
            }
        }
        list<string> fieldList = new list<string>();
        fieldList.addAll(fields);
        return fieldList;
    }
    
    public class FieldDefinition{
        @AuraEnabled
        public map<string,string> attributes{get;set;}
        @AuraEnabled
        public map<string,string> callbacks{get;set;}
        @AuraEnabled
        public string componentDef{get;set;}
        public FieldDefinition(string dataType, string viewType, string objectType){
            this.attributes = new map<string,string>();
            this.callbacks = new map<string,string>();
            if(viewType.containsIgnoreCase(VIEW_TYPE_INPUT)){
                this.componentDef = inputDataTypes.get(dataType.toLowerCase());
                if(inputCallbacks.containsKey(dataType.toLowerCase())){
                    for(string s:inputCallbacks.get(dataType.toLowerCase()).keySet()){
                        this.callbacks.put(s,inputCallbacks.get(dataType.toLowerCase()).get(s));
                    }
                }
            }else{
                this.componentDef = outputDataTypes.get(dataType.toLowerCase());
            }
            if(fieldAttributesMap.containsKey(dataType.toLowerCase())){
                for(string s:fieldAttributesMap.get(dataType.toLowerCase()).keySet()){
                    this.attributes.put(s,fieldAttributesMap.get(dataType.toLowerCase()).get(s));
                }
            }else if(this.attributes == null){
                this.attributes = new map<string,string>();
            }
        }
    }
    
    public static map<string,string> inputDataTypes = new map<string,string>
    {
        'anytype' => 'ui:inputText',
            'base64' => 'ui:inputText',
            'boolean' => 'ui:inputCheckbox',
            'combobox' => 'c:CustomPicklist',
            'currency' => 'ui:inputText',
            'datacategorygroupreference' => 'ui:inputText',
            'date' => 'ui:inputDate',
            'datetime' => 'ui:inputDateTime',
            'double' => 'ui:inputNumber',
            'email' => 'ui:inputEmail',
            'encryptedstring' => 'ui:inputText',
            'id' => 'ui:inputText',
            'integer' => 'ui:inputNumber',
            'multipicklist' => 'c:CustomPicklist',
            'percent' => 'ui:inputNumber',
            'phone' => 'ui:inputPhone',
            'picklist' => 'c:CustomPicklist',
            'reference' => 'c:CustomLookup',
            'string' => 'ui:inputText',
            'textarea' => 'ui:inputText',
            'time' => 'ui:inputDateTime',
            'url' => 'ui:inputText'
            };
                
                public static map<string,string> outputDataTypes = new map<string,string>{
                    'anytype' => 'ui:outputText',
                        'base64' => 'ui:outputText',
                        'boolean' => 'ui:outputCheckbox',
                        'combobox' => 'ui:outputText',
                        'currency' => 'ui:outputText',
                        'datacategorygroupreference' => 'ui:outputText',
                        'date' => 'ui:outputDate',
                        'datetime' => 'ui:outputDateTime',
                        'double' => 'ui:outputNumber',
                        'email' => 'ui:outputEmail',
                        'encryptedstring' => 'ui:outputText',
                        'id' => 'ui:outputText',
                        'integer' => 'ui:outputNumber',
                        'multipicklist' => 'ui:outputText',
                        'percent' => 'ui:outputNumber',
                        'phone' => 'ui:outputPhone',
                        'picklist' => 'ui:outputText',
                        'reference' => 'ui:outputText',
                        'string' => 'ui:outputText',
                        'textarea' => 'ui:outputText',
                        'time' => 'ui:outputDateTime',
                        'url' => 'ui:outputText'
                        };
                            
                            public static map<string,map<string,string>> inputCallbacks = new map<string,map<string,string>>{
                                'date' => new map<string,string>{
                                    'focus' => 'c.handleFieldUpdated'
                                        }
                            };
                            
                            public static map<string,map<string,string>> fieldAttributesMap = new map<string,map<string,string>>{
                                'date' => new map<string,string>{
                                    'displayDatePicker'=>'true'
                                        },
                                            'reference' => new map<string,string>{
                                                'IconName'=>'standard:account'
                                            }
                            };
                                }