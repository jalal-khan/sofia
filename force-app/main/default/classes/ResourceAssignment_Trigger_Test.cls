// ==================================================================================
//  Company : Force Optimized
//  Author : Mahesh S
//  Comments : Test Class for ResourceAssignment_Trigger
// ==================================================================================
//  Changes: 2013-07-16 Initial version.
// ==================================================================================
@isTest
private class ResourceAssignment_Trigger_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        
        Account account = TestDataFactory_AC.createAccounts(1)[0];
        account.BillingCity = 'city';
        account.BillingStreet = 'street';
        account.BillingPostalCode = '12345';
        account.BillingState = 'NY';
        insert account;
        
        Contact contct = TestDataFactory_AC.createContacts(1)[0];
        contct.AccountId = account.id;
        contct.FirstName = 'Test';
        insert contct;
        
        Opportunity opp = TestDataFactory_AC.createOpportunities(1)[0];
        opp.AccountId = account.id;
        opp.SFDC_Account_Executive__c = contct.id;
        
        insert opp;
        
        /* Commented on 14-08-14 to remove milestone Package 
        Milestone1_Project__c Project = TestDataFactory_AC.createProjects(1)[0];
        
        Project.Opportunity__c = opp.id;
        
        insert Project;
        */
        
        /* Testing Single RA Record */
        
        Projects_ForceOptimized__c RA = TestDataFactory_AC.createRAs(1)[0];
        RA.Project_Coordinator__c = true;
        RA.Opportunity__c = opp.id;
        
        insert RA;
        
        /* Asserting the result */
        System.assertEquals(RA.Opportunity__c, opp.id);
        
        /* Testing for Bulk records */
        List<Projects_ForceOptimized__c> RAList = TestDataFactory_AC.createRAs(150);
        
        for(Projects_ForceOptimized__c R : RAList)
        {
            R.Project_Coordinator__c = false;
            R.Opportunity__c = opp.id;
        }
        
        insert RAList;
        
        
    }
}