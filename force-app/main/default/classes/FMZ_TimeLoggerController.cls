public with sharing class FMZ_TimeLoggerController {

    @AuraEnabled
    public static Id getTimesheet(String startTime) {
        if(String.isEmpty(startTime)) {
            throw new AuraHandledException('Invalid date');
        }

        Date startDate = DateTime.newInstance(Long.valueOf(startTime)).date();
        Id contactId = [SELECT ContactId FROM User WHERE Id=:UserInfo.getUserId()].Id;
        Id timesheetRecordId = TimesheetService.getTimesheetByDate(startDate, contactId);

        if(timesheetRecordId == null) {
            throw new AuraHandledException('No Timesheets found for entered date');
        }

        return timesheetRecordId;
    }
}
