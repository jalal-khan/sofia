public with sharing class DealChecklistController {

	// ******************************************************** //
	// ****          Public Controller Properties          **** //
	// ******************************************************** //
	public Checklist__c checklist {
		get {
			return (Checklist__c)ctrl.getRecord();
		}
	}

	public id checklistId {
		get {
			return this.checklist.id;
		}
	}

	// Do we have a checklist selected?  Either viewing/Editing an existing checklist, or if creating a new checklist,
	// has the checklist type been selected and checklist entries created (in sections)?
	public boolean templateOK {
		get {
			return this.checklist.Template__c != null;
		}
	}

	// Are we without a template, and are there templates still left to create for this deal?
	public boolean okToCreate {
		get {
			return (!this.templateOK && this.templateOptions.size() > 1); // --select-- is always option 1
		}
	}

	public string subtitle {
		get {
			if (this.checklist == null || this.checklist.id == null ) {
				return 'New Checklist';
			}
			return this.checklist.Name;
		}
	}

	public id templateId {
		get {return this.checklist.Template__c;}
		set {this.checklist.Template__c = value;}
	}

	public id dealId {
		get {
			return (Id) this.checklist.get(this.clObjAPIName);
		}
	}

	public list<ChecklistSection> sections {get; set;}
	public integer actionSectionIndex {get; set;}
	public integer actionItemIndex {get; set;}

	public list<SelectOption> templateOptions {
		get {
			list<SelectOption> res = new list<SelectOption>();
			this.templateNames = new map<id, string>();
			res.add(new SelectOption('', '--Select--'));
			if(String.isNotBlank(this.clObject)){
				String query = 'select id, name from Checklist_Template__c where id not in (select Template__c from Checklist__c where '
						+ this.clObjAPIName +' = \'' +this.dealId +'\') and Object__c = \''+ this.clObject +'\'';
				List<Checklist_Template__c> tmpls = (List<Checklist_Template__c>) Database.query(query);
				if(tmpls != null && tmpls.size() > 0){
					for (Checklist_Template__c tmpl : tmpls) {
						res.add(new SelectOption(tmpl.id, tmpl.name));
						this.templateNames.put(tmpl.id, tmpl.name);
					}
				}
			}
			return res;
		}
	}

	public list<SelectOption> dealContactRoles {
		get {
			if (this.dealContactRoles == null) {
				map<string, string> existingContactRoles = getExistingDealRoles(this.dealId, this.clObjAPIName);
				list<SelectOption> options = new list<SelectOption>();
				options.add(new SelectOption('', '--Select--'));
				options.add(new SelectOption('User', 'Select Non-role User...'));
				map<string, string> dcrMap = FMZ_SchemaUtils.getPickListValueMap(Internal_Role__c.sObjectType, Internal_Role__c.Role__c.getDescribe().getName());
				map<string, List<string>> dcrDependentValuesMap = FMZ_SchemaUtils.GetDependentOptions(
						Internal_Role__c.sObjectType.getDescribe().getName(),
						Internal_Role__c.Object__c.getDescribe().getName(),
						Internal_Role__c.Role__c.getDescribe().getName()
				);
				list<string> dcrList = dcrDependentValuesMap.get(this.clObject);
				if(dcrList == null || dcrList.size() == 0){
					dcrList = FMZ_SchemaUtils.getPickListValues(Internal_Role__c.sObjectType, Internal_Role__c.Role__c.getDescribe().getName());
				}
				dcrList.sort();
				for (string dcr : dcrList) {
					string label = getRoleAndUser(dcrMap.get(dcr), existingContactRoles);
					options.add(new SelectOption(dcr, label));
				}
				this.dealContactRoles = options;
			}
			return dealContactRoles;
		}
		private set;
	}

	public map<string, string> fieldLabelMap {
		get{
			if(fieldlabelMap == null){
				map<string, string> labelMap = new map<string, string>();
				Map<String, Schema.DescribeFieldResult> fieldMap = FMZ_SchemaUtils.getObjectFieldDescribes(this.lookupSObjField.getDescribe().getReferenceTo()[0]);
				for (Schema.DescribeFieldResult dfr : fieldMap.values()) {
					labelMap.put(dfr.getName(), dfr.getLabel());
				}
				fieldLabelMap = labelMap;
			}
			return fieldLabelMap;
		}
		private set;
	}

	public String clObjAPIName {
		get{
			if(clObjAPIName == null){
				clObjAPIName = this.lookupSObjField.getDescribe().getName();
			}
			return clObjAPIName;
		}
		private set;
	}


	// ******************************************************** //
	// ****          Private Controller Properties         **** //
	// ******************************************************** //

	private List<Schema.SObjectField> lookupSObjFields { get; set; }

	private Schema.SObjectField lookupSObjField {
		get {
			if(lookupSObjField == null){
				for(Schema.SObjectField field : this.lookupSObjFields){
					DescribeFieldResult dfr = field.getDescribe();
					if(this.checklist.get(dfr.getName()) != null){
						lookupSObjField = field;
						break;
					}
				}
			}
			return lookupSObjField;
		}
		set;
	}

	private String clObject {
		get {
			if(clObject == null){
				clObject = this.lookupSObjField.getDescribe().getReferenceTo()[0].getDescribe().getLabel();
			}
			return clObject;
		}
		set;
	}

	private map<id, string> templateNames {get; set;}
	private ApexPages.StandardController ctrl {get; set;}


	// ******************************************************** //
	// ****                  Constructor                   **** //
	// ******************************************************** //

	public DealChecklistController(ApexPages.StandardController ctrl) {
		Map<String,Schema.SObjectField> fields = Checklist__c.sObjectType.getDescribe().fields.getMap();
		Schema.FieldSet fieldset = Checklist__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		List<DescribeFieldResult> dfrs = new List<DescribeFieldResult>();
		List<Schema.SObjectField> objFields = new List<Schema.SObjectField>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			Schema.SObjectField sof = fields.get(fsm.getFieldPath());
			DescribeFieldResult dfr = sof.getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
				objFields.add(sof);
			}
		}
		List<String> lookupFields = new List<String>();
		if(dfrs.size() > 0) {
			for(DescribeFieldResult dfr : dfrs){
				lookupFields.add(dfr.getName());
			}
		}
		this.lookupSObjFields = objFields;
		Set<String> fta = new Set<String>{'id', 'name', FMZ_SchemaUtils.currentNamespacePrefix+'Template__c'};
		fta.addAll(lookupFields);

		this.ctrl = ctrl;
		if (Test.isRunningTest() == false) {
			this.ctrl.addFields(new List<String>(fta));
		}
		this.sections = new list<ChecklistSection>();
		this.templateNames = new map<id, string>();

		if (this.checklistId != null) {
			integer ndx = 0;
			map<string, ChecklistSection> sectionMap = new map<string, ChecklistSection>();
			for (Checklist_Entry__c entry : [
					select id, Name, Status__c, Internal_Role__c, Due_Date__c, checklist__c, Notes__c,
								 Template__c, Template__r.Section__c, Template__r.Due_Date_Based_On__c, Template__r.Due_Date_Offset__c,
				 				 Specific_User__c, Specific_User__r.Name, Internal_Deal_Team_Role__c
					  from Checklist_Entry__c
					 where Checklist__c = :this.checklistId
				order by Template__r.Sort_Order__c
			]) {
				if (sectionMap.containsKey(entry.Template__r.section__c) == false) {
					sectionMap.put(entry.Template__r.section__c, new ChecklistSection(entry.Template__r.Section__c, ndx++, this));
				}
				sectionMap.get(entry.Template__r.section__c).addItem(entry, ChecklistServices.getDealWithDates(this.dealId));
			}
			//FIXME sections must be sorted
			this.sections = sectionMap.values();

		}
	}


	// ******************************************************** //
	// ****             Public Action Methods              **** //
	// ******************************************************** //

	public void refreshRoleInput() {
		// do nothing but force a round-trip
	}

	public PageReference saveChecklist() {
		Checklist__c chklist;

		System.debug('### Starting ###');

		for (ChecklistSection section : sections) {
			section.setChecklistEntryData();
		}

		if (validateInput() == false) {
			// validateInput adds error messages, just return.
			return null;
		}

		System.debug('### Here 1 ###');

		Savepoint sp = Database.setSavepoint();
		try {
			chklist = this.checklist;
			upsert chklist;
			list<Checklist_Entry__c> forUpsert = new list<Checklist_Entry__c>();
			for (ChecklistSection section : sections) {
				forUpsert.addAll(section.getChecklistEntriesForUpsert(chklist.Id));
			}
			upsert forUpsert;
		}
		catch (Exception e) {
			Database.Rollback(sp);
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Could not save checklist: ' + e.getMessage()));
			System.debug('### Save didnt work, error: '+e.getMessage() );
			System.debug('### Stace Trace: '+e.getLineNumber());
			return null;
		}

		System.debug('### Finishing ###');

		return (new ApexPages.StandardController(chklist)).view();
	}

	public PageReference createChecklist() {
		if (this.templateOk == false) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'You must select a checklist template.'));
			return null;
		}
		this.checklist.name = this.templateNames.get(this.templateId);
		SObject deal = ChecklistServices.getDealWithDates(dealId);
		map<string, ChecklistSection> sectionMap = new map<string, ChecklistSection>();
		integer ndx = 0;
		for (Checklist_Template_Entry__c entry : [
				select id, Name, Default_Deal_Team_Role__c,
				Due_Date_Based_On__c, Due_Date_Offset__c, Section__c, Default_Notes__c
				  from Checklist_Template_Entry__c
				 where Checklist_Template__c = :this.templateId
			order by Sort_Order__c
		]) {
			if (sectionMap.containsKey(entry.section__c) == false) {
				sectionMap.put(entry.section__c, new ChecklistSection(entry.section__c, ndx++, this));
			}
			sectionMap.get(entry.section__c).addItem(entry, deal);
		}
		//FIXME sections must be sorted
		this.sections = sectionMap.values();
		return null;
	}

	public PageReference resetDueDates() {
		list<ChecklistSection> selectedSections = new list<ChecklistSection>();
		if (this.actionSectionIndex <0) {
			selectedSections = sections;
		}
		else if (this.actionItemIndex >= 0) {
			this.sections[this.actionSectionIndex].items[actionItemIndex].resetDueDates();
		}
		else if (this.actionSectionIndex > this.sections.size()-1) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Cannot reset due dates: invalid section index (internal error)'));
			return null;
		}
		else {
			selectedSections.add(this.sections[this.actionSectionIndex]);
		}
		for (ChecklistSection sect : selectedSections) {
			sect.resetDueDates();
		}
		return null;
	}


	// ******************************************************** //
	// ****                Wrapper Classes                 **** //
	// ******************************************************** //

	public class ChecklistSection {
		public integer index {get; set;}
		public string name {get; set;}
		public list<ChecklistItem> items {get; set;}

		private DealChecklistController ctrl {get; set;}

		public ChecklistSection(string name, integer ndx, DealChecklistController ctrl) {
			this.name = name;
			this.index = ndx;
			this.items = new list<ChecklistItem>();
			this.ctrl = ctrl;
		}

		public void addItem(Checklist_Template_Entry__c entry, SObject deal) {
			items.add(new ChecklistItem(entry, deal, items.size(), ctrl));
		}

		public void addItem(Checklist_Entry__c entry, SObject deal) {
			items.add(new ChecklistItem(entry, deal, items.size(), ctrl));
		}

		public void resetDueDates() {
			for (ChecklistItem item : this.items) {
				item.resetDueDates();
			}
		}

		public list<Checklist_Entry__c> getChecklistEntriesForUpsert(id checklistId) {
			list<Checklist_Entry__c> entries = new list<Checklist_Entry__c>();
			for (ChecklistItem item : items) {
				entries.add(item.getChecklistEntryForUpsert(checklistId));
			}
			return entries;
		}

		public void setChecklistEntryData(){
			for (ChecklistItem item : items) {
				item.setChecklistEntryData();
			}
		}

	}

	public class ChecklistItem {
		private Checklist_Template_Entry__c template {get; set;}
		public integer index {get; set;}
		private SObject deal {get; set;}
		public map<string, string> existingContactRoles {get; set;}
		public Checklist_Entry__c obj {get; set;}
		public Date defaultDueDate {get; set;}
		public string roleError {get; set;}  // we don't use inputField for role, so need to track error separately
		public boolean canEdit {
			get {
				return this.obj.Status__c != ChecklistServices.STATUS_COMPLETE;
			}
		}

		public string dealContactRole {
			get {
				if (this.obj.Internal_Deal_Team_Role__c == 'User') {
					if (this.obj.Specific_User__c == null) {
						return 'Unassigned (Non-role assignment)';
					}
					return this.obj.Specific_User__r.Name + ' (Non-role assignment)';
				}

				System.debug('#### this.obj.Internal_Role__c: '+this.obj.Internal_Role__c);
				System.debug('#### existingContactRoles: '+existingContactRoles);

				return getRoleAndUser(this.obj.Internal_Deal_Team_Role__c, existingContactRoles);
			}
		}

		public string dateFieldDisplay {
			get {
				string res = this.template.Due_Date_Based_On__c;
				if (res == null) return '(No default)';
				if (ctrl.fieldLabelMap.containsKey(res)) {
					res = ctrl.fieldLabelMap.get(res);
				}
				if (this.template.Due_Date_Offset__c != null && this.template.Due_Date_Offset__c != 0) {
					list<string> parts = new list<string>{
						res,
						this.template.Due_Date_Offset__c < 0 ? '-' : '+',
						String.valueOf(Math.abs((integer)this.template.Due_Date_Offset__c))
					};
					string space = '&nbsp;';
					res = string.join(parts, space);
				}
				return res;
			}
		}

		private DealChecklistController ctrl {get; set;}

		public ChecklistItem(Checklist_Template_Entry__c entry, SObject deal, integer ndx, DealChecklistController ctrl) {
			this.template = entry;
			this.deal = deal;
			this.index = ndx;
			this.ctrl = ctrl;
			Date dueDate = null;
			if (entry.Due_Date_Based_On__c != null) {
				dueDate = (Date)deal.get(entry.Due_Date_Based_On__c);
			}
			if (dueDate != null && entry.Due_Date_Offset__c != null) {
				dueDate = dueDate.addDays((integer)entry.Due_Date_Offset__c);
			}
			this.defaultDueDate = dueDate;
			this.obj = new Checklist_Entry__c(
				name = entry.name,
				template__c = entry.id,
				Notes__c = entry.Default_Notes__c,
				Internal_Deal_Team_Role__c = entry.Default_Deal_Team_Role__c,
				Status__c = ChecklistServices.STATUS_NEEDED,
				Due_Date__c = dueDate
			);
		}

		public ChecklistItem(Checklist_Entry__c entry, SObject deal, integer ndx, DealChecklistController ctrl) {
			this.obj = entry;
			this.template = this.obj.template__r;
			this.index = ndx;
			this.deal = deal;
			this.ctrl = ctrl;
			Id dealId = (Id) deal.get('id');
			this.existingContactRoles = getExistingDealRoles(dealId, ctrl.clObjAPIName);
			Date dueDate = null;
			if (this.template.Due_Date_Based_On__c != null) {
				dueDate = (Date)deal.get(this.template.Due_Date_Based_On__c);
			}
			if (dueDate != null && this.template.Due_Date_Offset__c != null) {
				dueDate = dueDate.addDays((integer)this.template.Due_Date_Offset__c);
			}
			system.debug(this.obj);
			this.defaultDueDate = dueDate;
			system.debug(this.defaultDueDate);

		}

		public boolean validate() {
			boolean isValid = true;
			this.roleError = null;
			if (this.obj.Status__c == ChecklistServices.STATUS_NEEDED) {
				// validate DCR and Date
				if (this.obj.Internal_Role__c == null || this.obj.Internal_Role__c == '') {
					this.roleError = 'Must select a role when status is ' + ChecklistServices.STATUS_NEEDED;
					this.obj.Internal_Role__c.addError(this.roleError);
					isValid = false;
				}
				if (this.obj.Internal_Role__c == 'User' && this.obj.Specific_User__c == null) {
					this.roleError = 'Must select a user when role is "Specific User..."';
					this.obj.Internal_Role__c.addError(this.roleError);
					isValid = false;
				}
				if (this.obj.Due_Date__c == null) {
					this.obj.Due_Date__c.addError('Must select a due date when status is ' + ChecklistServices.STATUS_NEEDED);
					isValid = false;
				}
			}
			return isValid;
		}

		public Checklist_Entry__c getChecklistEntryForUpsert(id checklistId) {
			if (this.obj.checklist__c == null) {
				this.obj.Checklist__c = checklistId;
			}
			return obj;
		}

		public void setChecklistEntryData() {
			if (this.obj.Internal_Role__c != 'User') {
				this.obj.Specific_User__c = null;
			}
			this.obj.Internal_Role__c = this.obj.Internal_Deal_Team_Role__c;
		}

		private void resetDueDates() {
			this.obj.Due_Date__c = this.defaultDueDate;
		}

	}


	// ******************************************************** //
	// ****                Private Helpers                 **** //
	// ******************************************************** //

	private boolean validateInput() {
		boolean isValid = true;
		for (ChecklistSection sect : this.sections) {
			for (ChecklistItem item : sect.items) {
				isValid = item.validate() && isValid;  // item.validate() must come first due to short-circuit evaluation
			}
		}

		System.debug('### isValid: '+isValid);

		return isValid;
	}

	private static string getRoleAndUser(string rolename, map<string, string> roleToUserMap) {
		string label = rolename;
		string username = roleToUserMap.get(label);
		if (username == null) username = 'Unassigned';
		if (label == null) return username;
		label = label + ' (' + username + ')';
		return label;

	}

	//dead-simple caching since this controller should only ever see one deal at a time
	private static id lastDealId;
	private static map<string, string> lastDealRoles;
	private static map<string, string> getExistingDealRoles(id dealId, string clObjAPIName) {
		if (lastDealId != dealId) {
			lastDealId = dealId;
			map<string, string> rolenames = new map<string, string>();
			String query = 'select Role__c, Internal_Team_Member__r.Name from Internal_Role__c where '+clObjAPIName+' = \''+dealId+'\' and Role__c != null';
			List<Internal_Role__c> dcrs = (List<Internal_Role__c>) Database.query(query);
			for (Internal_Role__c dcr : dcrs) {
				rolenames.put(dcr.role__c, dcr.Internal_Team_Member__r.Name);
			}
			lastDealRoles = rolenames;
		}
		return lastDealRoles;
	}
}