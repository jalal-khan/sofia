@IsTest
private class ProjectServiceTest {
    
    @TestSetup
    static void makeData(){        
        FMZ_Role__c role1 = new FMZ_Role__c(
            Active__c = TRUE,
            Effective_Start_Date__c = date.valueOf('2020-01-01'),
            Name = 'Solution Architect',
            Role_Abbreviation__c = 'SA',
            List_Price__c = 235
        );
        FMZ_Role__c role2 = new FMZ_Role__c(
            Active__c = TRUE,
            Effective_Start_Date__c = date.valueOf('2020-01-01'),
            Name = 'Technical Architect',
            Role_Abbreviation__c = 'TA',
            List_Price__c = 235
        );
        insert new List<FMZ_Role__c> { role1, role2 };
    }

    @IsTest
    static void emptyProject() {
        FMZ_Project__c project = new FMZ_Project__c();
        insert project;

        Opportunity opportunity = TestObjectFactory.getOpportunity();
        opportunity.FMZ_Project__c = project.Id;
        insert opportunity;
		
        List<FMZ_Role__c> roles = [SELECT Id FROM FMZ_Role__c ORDER WHERE Active__c = TRUE];
        FMZ_Opportunity_Role__c oppRole1 = new FMZ_Opportunity_Role__c(
        	Opportunity__c = opportunity.Id,
            Role__c = roles[0].Id,
            Total_Hours__c = 10, 
            Hourly_Rate__c = 20,
            Number_of_Weeks__c = 1
        );
        FMZ_Opportunity_Role__c oppRole2 = new FMZ_Opportunity_Role__c(
        	Opportunity__c = opportunity.Id,
            Role__c = roles[1].Id,
            Total_Hours__c = 20, 
            Hourly_Rate__c = 40,
            Number_of_Weeks__c = 1
        );
        insert new List<FMZ_Opportunity_Role__c> { oppRole1, oppRole2 };

        Test.startTest();
        ProjectService.createProjectRoles(new Set<Id> { opportunity.Id });
        Test.stopTest();

        List<FMZ_Project_Role__c> projectRoles = [
            SELECT Id, FMZ_Project__c, FMZ_Opportunity_Role__c , SOW_Allocated_Hours__c, SOW_Hourly_Rate__c
            FROM FMZ_Project_Role__c ORDER BY FMZ_Role__r.Name];

        System.assertEquals(2, projectRoles.size());
        System.assertEquals(project.Id, projectRoles[0].FMZ_Project__c);
        System.assertEquals(oppRole1.Id, projectRoles[0].FMZ_Opportunity_Role__c );
        System.assertEquals(10, projectRoles[0].SOW_Allocated_Hours__c);
        System.assertEquals(20, projectRoles[0].SOW_Hourly_Rate__c);
        System.assertEquals(project.id, projectRoles[1].FMZ_Project__c);
        System.assertEquals(oppRole2.Id, projectRoles[1].FMZ_Opportunity_Role__c );
        System.assertEquals(20, projectRoles[1].SOW_Allocated_Hours__c);
        System.assertEquals(40, projectRoles[1].SOW_Hourly_Rate__c);
    }

    @IsTest
    static void existingRole() {
        FMZ_Project__c project = new FMZ_Project__c();
        insert project;

        Opportunity opportunity = TestObjectFactory.getOpportunity();
        opportunity.FMZ_Project__c = project.Id;
        insert opportunity;

        List<FMZ_Role__c> roles = [SELECT Id FROM FMZ_Role__c ORDER WHERE Active__c = TRUE];
        FMZ_Opportunity_Role__c oppRole1 = new FMZ_Opportunity_Role__c(
        	Opportunity__c = opportunity.Id,
            Role__c = roles[0].Id,
            Total_Hours__c = 10, 
            Hourly_Rate__c = 20,
            Number_of_Weeks__c = 1
        );
        FMZ_Opportunity_Role__c oppRole2 = new FMZ_Opportunity_Role__c(
        	Opportunity__c = opportunity.Id,
            Role__c = roles[1].Id,
            Total_Hours__c = 20, 
            Hourly_Rate__c = 40,
            Number_of_Weeks__c = 1
        );
        insert new List<FMZ_Opportunity_Role__c> { oppRole1, oppRole2 };

        insert new FMZ_Project_Role__c(
            FMZ_Project__c = project.Id,
            FMZ_Opportunity_Role__c  = oppRole1.Id,
            SOW_Allocated_Hours__c = 500,
            SOW_Hourly_Rate__c = 1000
        );

        Test.startTest();
        ProjectService.createProjectRoles(new Set<Id> { opportunity.Id });
        Test.stopTest();

        List<FMZ_Project_Role__c> projectRoles = [
            SELECT Id, FMZ_Project__c, FMZ_Opportunity_Role__c , SOW_Allocated_Hours__c, SOW_Hourly_Rate__c
            FROM FMZ_Project_Role__c ORDER BY FMZ_Role__r.Name];

        System.assertEquals(2, projectRoles.size());
        System.assertEquals(project.Id, projectRoles[0].FMZ_Project__c);
        System.assertEquals(oppRole1.Id, projectRoles[0].FMZ_Opportunity_Role__c);
        System.assertEquals(10, projectRoles[0].SOW_Allocated_Hours__c);
        System.assertEquals(20, projectRoles[0].SOW_Hourly_Rate__c);
        System.assertEquals(project.id, projectRoles[1].FMZ_Project__c);
        System.assertEquals(oppRole2.Id, projectRoles[1].FMZ_Opportunity_Role__c);
        System.assertEquals(20, projectRoles[1].SOW_Allocated_Hours__c);
        System.assertEquals(40, projectRoles[1].SOW_Hourly_Rate__c);
    }
}