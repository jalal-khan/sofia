@RestResource(urlMapping='/ExLog/')
global without sharing class FMZ_RESTExLogServices {
    global class Ex{
        public string additionalInfo;
        public integer lineNumber;
        public string location;
        public string message;
        public string stackTrace;
        public string type;
        public string user;
    }
    
    @HttpPost
    global static void addExceptions(list<Ex> exceptions, string orgId){
        
        list<Exception__c> newExceptions = new list<Exception__c>();
        
        for(Ex ex:exceptions){
            Exception__c  exc = new Exception__c ();
            exc.Additional_Info__c = ex.additionalInfo;
            exc.Line_Number__c = ex.lineNumber;
            exc.Location__c = ex.location;
            exc.Message__c = ex.message;
            exc.Stack_Trace__c = ex.stackTrace;
            exc.Type__c = ex.type;
            exc.Client_Org_Id__c = orgId;
            newExceptions.add(exc);
        }
        if(!newExceptions.isEmpty()){
        	insert newExceptions;
        }
        
        string clientOrg = orgId;
        
        list<Client_Link__c> clinks = [SELECT Id, Client_Org_Id__c, Subscription_End__c, Active_Monitoring_Enabled__c, Active_Monitoring_Restarts__c FROM Client_Link__c WHERE Client_Org_Id__c = :clientOrg];
        Client_Link__c cl;
        if(!clinks.isEmpty()){
            cl = clinks[0];
            cl.Last_Org_Communication__c = DateTime.now();
            cl.Last_Exception_Job_Time__c = DateTime.now();
            update cl;
        }
    }
}