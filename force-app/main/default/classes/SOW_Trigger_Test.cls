// ==================================================================================
//  Company : Force Optimized
//  Author : JM
//  Comments : Test Class for SOW_Trigger
// ==================================================================================
//  Changes: 2013-08-28 Initial version. Cloned test to get by code coverage for this inactive trigger
// ==================================================================================
@isTest
private class SOW_Trigger_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Account account = TestDataFactory_AC.createAccounts(1)[0];
    insert account;
    
    Contact contct = TestDataFactory_AC.createContacts(1)[0];
    contct.AccountId = account.id;
    contct.FirstName = 'Test';
    insert contct;
    
    Opportunity opp = TestDataFactory_AC.createOpportunities(1)[0];
    opp.AccountId = account.id;
    opp.SFDC_Account_Executive__c = contct.id;
    
    insert opp;
    

    SOW__c SOW = TestDataFactory_AC.createSOWs(1)[0];
    
    SOW.Opportunity__c = opp.id;
    
    insert SOW;
    

    /* Asserting the result */
    //System.assertEquals(SOW.Opportunity__c, opp.id);
    
    /* Testing for Bulk records */
    List<SOW__c> SOWList = TestDataFactory_AC.createSOWs(150);
    
    for(SOW__c R : SOWList)
    {
      R.Opportunity__c = opp.id;
    }
    
    insert SOWList;
    
    
    }
}