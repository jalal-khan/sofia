// ==================================================================================
//  Company : Force Optimized
//  Author : Mahesh S
//  Comments : Send a notification and Create a task to project coordinator 
// ==================================================================================
//  Changes: 2013-07-16 Initial version.
// ==================================================================================
public with sharing class StartProject_AC {
	
	public String message{get;set;}
	public String opportunityId = '';
	
	/* Constructor */
	public StartProject_AC()
	{
		opportunityId = System.currentPageReference().getParameters().get('Id');
	}
	/* Method to create a task and send email to resource */
	public pageReference autoRun()
	{
		List<Projects_ForceOptimized__c> resourceList = [select id,
																Resource__c,
																Resource__r.Email,
																Opportunity__r.Name,
																Resource__r.Name,
																Project_Coordinator__c from Projects_ForceOptimized__c where 
																Project_Coordinator__c = true and Opportunity__c =: opportunityId];
		if(resourceList.size()>0)
		{
			
			
			/* Creating Task for the resource */
			Task t = new Task();
			t.Subject = 'New project '+resourceList[0].Opportunity__r.Name+' Launched.';
			t.whatId = OpportunityId;
			t.ActivityDate = System.Today();
			t.ownerId = resourceList[0].Resource__c;
			t.Description = 	'1)  Request Courtesy Licenses \n'+
								'2)  Schedule Knowledge Transfer Call \n'+
								'3)  Create Project Kickoff Deck \n'+
								'4)  Schedule Project Kickoff Call \n'+
								'5)  Create Project Smartsheet \n'+
								'6)  Create a project in PartnerPortal \n'+
								'7)  If Professional version, request API Access ';
			
			insert t;
			
			/* Sending email to the resource */
			
			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
				
			string [] toaddress= New string[]{resourceList[0].Resource__r.Email};			     
			email.setSubject('New project '+resourceList[0].Opportunity__r.Name+' Launched.');	
			email.setHtmlBody('<html>'+'<font face="calibre">'+
						   	  '<body> Hi '+resourceList[0].Resource__r.Name+',<br/><br/>'+
						      'New project '+resourceList[0].Opportunity__r.Name+' Launched. Please proceed with project set up process.<br/>'+
						      '1)  Request Courtesy Licenses <br/>'+
						      '2)  Schedule Knowledge Transfer Call  <br/>'+
						      '3)  Create Project Kickoff Deck  <br/>'+
						      '4)  Schedule Project Kickoff Call <br/>'+
						      '5)  Create Project Smartsheet <br/>'+
						      '6)  Create a project in PartnerPortal <br/>'+
						      '7)  If Professional version, request API Access <br/><br/>'+
						      'Thank you !! <br/>'
					      
					      
					      
						      );
				email.setToAddresses(toaddress);
			     Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
			     
			message = 'Task created and Email has been sent to '+ resourceList[0].Resource__r.Name;
		}
		else
		{
			message = 'This opportunity dont have Project Coordinator in Resource assignment';
		}
		return null;
	}
	
	/* Method to redirect back to Opportunity */
	public pageReference redirectToOpp()
	{
		pageReference pf = new PageReference('/'+opportunityId);
		return pf;
	}
	
	/* Test Method for StartProject_AC*/
	
	testMethod private static void testStartProject()
	{
		Account account = TestDataFactory_AC.createAccounts(1)[0];
		insert account;
		
		Contact contct = TestDataFactory_AC.createContacts(1)[0];
		contct.AccountId = account.id;
		contct.FirstName = 'Test';
		insert contct;
		
		Opportunity opp = TestDataFactory_AC.createOpportunities(1)[0];
		opp.AccountId = account.id;
		opp.SFDC_Account_Executive__c = contct.id;
		
		insert opp;
		
		/* Commented on 14-08-14 to remove milestone Package 
		Milestone1_Project__c Project = TestDataFactory_AC.createProjects(1)[0];
		Project.Opportunity__c = opp.id;
		
		insert Project; */
		
		Projects_ForceOptimized__c RA = TestDataFactory_AC.createRAs(1)[0];
		RA.Project_Coordinator__c = true;
		RA.Opportunity__c = opp.id;
		
		insert RA;
		
		System.currentPageReference().getParameters().put('Id', opp.id);
		StartProject_AC SP =New StartProject_AC();
		
		SP.autoRun();
		SP.redirectToOpp();
		
	}
}