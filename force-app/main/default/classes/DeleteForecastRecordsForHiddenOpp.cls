/**********************************************************
@class            DeleteForecastRecordsForHiddenOpp
@brief            This class will be passed opportunity records matching where the hidden flag has been set and will delete forecast records
@bug              None
@todo             None
@version          V1.0
@author           Developer Name (rchapple)
@date             6/4/18
@copyright        (c)2018 Fortimize.  All Rights Reserved. Unauthorized use is prohibited.
***********************************************************/
public class DeleteForecastRecordsForHiddenOpp
{
    @InvocableMethod
    public static void ForecastDelete(List<Id> OpportunityIds)
    {
        List<Forecast__c> forecastsToDelete = [Select Id from Forecast__c where Project_Role__r.Opportunity__c in :OpportunityIds];
        if(forecastsToDelete.size() > 0) {
            delete forecastsToDelete;
        }
   }
}