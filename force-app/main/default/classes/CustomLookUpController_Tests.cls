@isTest
public class CustomLookUpController_Tests {

    @TestSetup
    static void makeData(){
        // we're going to use a standard object so it's not org dependent.
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        // make some opptys too for the related list search methods.
        list<Opportunity> opps = new list<Opportunity>();
        for(integer i = 0; i < 5; i++){
            opps.add(new Opportunity(
            	name='Test Oppty ' + i,
                StageName = 'Open',
                Amount = 100,
                CloseDate = Date.today().addDays(30),
                AccountId = a.Id
            ));
        }
        insert opps;
    }
    
    @isTest
    static void testFetchLookUpValues(){
        Account a = [SELECT Id, Name FROM Account LIMIT 1];
        list<sObject> objs = customLookupController.fetchLookUpValues(a.Name, 'Account');
        system.assertEquals(1, objs.size());
        system.assertEquals(a.Name, objs[0].get('Name'));
    }
    
    @isTest
    static void testGetRecord(){
        Account a = [SELECT Id, Name FROM Account LIMIT 1];
        sObject obj = customLookupController.getRecord(a.Id, 'Account');
        system.assertEquals(a.Name, obj.get('Name'));
    }
}