public class ChecklistTemplateController {

	public static final string DEFAULT_DUE_DATE_BASED_ON = null; //'Plrs__Close_Date__c';  // null for no default

	public Checklist_Template__c template {
		get {
			return (Checklist_Template__c)ctrl.getRecord();
		}
	}
	public id templateId {
		get {
			return this.template.id;
		}
	}

	public integer actionSectionIndex {get; set;}
	public integer actionItemIndex {get; set;}

	public list<ChecklistSection> sections {get; set;}

	public list<ChecklistSection> deletedSections {get; set;}
	public list<ChecklistTemplateEntry> deletedItems {get; set;}

	public string subtitle {
		get {
			if (this.template == null || this.template.id == null ) {
				return 'New Checklist Template';
			}
			return this.template.Name;
		}
	}

	public Boolean disableDates {get; set;}

	private Set<Schema.SObjectType> lookupSObjTypes {get; set;}

	public PageReference updateOptions(){
		updateDealTeamRoles();
		updateDealDateFields();
		return null;
	}

	public list<SelectOption> checklistObjects {
		get {
			if (this.checklistObjects == null) {
				list<SelectOption> options = new list<SelectOption>();
				options.add(new SelectOption('', '--Select--'));
				map<string, string> cloMap = FMZ_SchemaUtils.getPickListValueMap(Checklist_Template__c.sObjectType, Checklist_Template__c.Object__c.getDescribe().getName());
				list<string> cloList = FMZ_SchemaUtils.getPickListValues(Checklist_Template__c.sObjectType, Checklist_Template__c.Object__c.getDescribe().getName());
				cloList.sort();
				for (string clo : cloList) {
					options.add(new SelectOption(clo, cloMap.get(clo)));
				}
				this.checklistObjects = options;
			}
			return checklistObjects;
		}
		private set;
	}

	public list<SelectOption> dealTeamRoles {
		get {
			if (this.dealTeamRoles == null) {
				updateDealTeamRoles();
			}
			return dealTeamRoles;
		}
		private set;
	}

	private void updateDealTeamRoles(){
		list<SelectOption> options = new list<SelectOption>();
		options.add(new SelectOption('', '--Select--'));
		map<string, string> dcrMap = FMZ_SchemaUtils.getPickListValueMap(Internal_Role__c.sObjectType, Internal_Role__c.Role__c.getDescribe().getName());
		map<string, List<string>> dcrDependentValuesMap = FMZ_SchemaUtils.GetDependentOptions(
				Internal_Role__c.sObjectType.getDescribe().getName(),
				Internal_Role__c.Object__c.getDescribe().getName(),
				Internal_Role__c.Role__c.getDescribe().getName()
		);
		list<string> dcrList = dcrDependentValuesMap.get(this.template.Object__c);
		if(dcrList == null || dcrList.size() == 0){
			dcrList = FMZ_SchemaUtils.getPickListValues(Internal_Role__c.sObjectType, Internal_Role__c.Role__c.getDescribe().getName());
		}
		dcrList.sort();
		for (string dcr : dcrList) {
			options.add(new SelectOption(dcr, dcrMap.get(dcr)));
		}
		dealTeamRoles = options;
	}

	public list<SelectOption> dealDateFields {
		get {
			if (this.dealDateFields == null) {
				updateDealDateFields();
			}
			return this.dealDateFields;
 		}
		private set;
	}

	public void updateDealDateFields(){
		list <SelectOption> options = new list<SelectOption>();
		options.add(new SelectOption('', '--None--'));
		Schema.SObjectType objType = Schema.getGlobalDescribe().get(this.template.Object__c);
		if(objType == null){
			String objName = this.template.Object__c == null ? '' : this.template.Object__c;
			for(Schema.SObjectType type : this.lookupSObjTypes){
				if(type.getDescribe().getLabel() == objName){
					objType = type;
				 	break;
				}
			}
		}
		if(objType != null){
			Map<String, Schema.DescribeFieldResult> fieldMap = FMZ_SchemaUtils.getObjectFieldDescribes(objType);

			// sorting list<SelectOption> sorts by SelectOption.value, not by .label; so build a list of options as strings: label~value,
			//  sort the strings (thus sorting by label), then build the options by splitting the strings.
			list<string> optionsForSort = new list<string>();
			for (Schema.DescribeFieldResult dfr : fieldMap.values()) {
				if (dfr.getType() == Schema.DisplayType.Date) {
					optionsForSort.add(dfr.getLabel() + '~' + dfr.getName());
				}
			}
			optionsForSort.sort();

			for (String opt : optionsForSort) {
				list<string> parts = opt.split('~');
				options.add(new SelectOption(parts[1], parts[0]));
			}
		}
		if(options.size() > 1){
			disableDates = false;
		}else{
			disableDates = true;
		}

		this.dealDateFields = options;
	}

	private ApexPages.StandardController ctrl {get; set;}
	public ChecklistTemplateController(ApexPages.StandardController ctrl) {
		this.ctrl = ctrl;
		deletedSections = new list<ChecklistSection>();
		deletedItems = new list<ChecklistTemplateEntry>();

		Map<String,Schema.SObjectField> fields = Checklist__c.sObjectType.getDescribe().fields.getMap();
		Schema.FieldSet fieldset = Checklist__c.sObjectType.getDescribe().fieldsets.getMap().get(FMZ_SchemaUtils.currentNamespacePrefix+'Available_Objects');
		List<DescribeFieldResult> dfrs = new List<DescribeFieldResult>();
		Set<Schema.SObjectType> objlookupTypes = new Set<Schema.SObjectType>();
		for(Schema.FieldSetMember fsm : fieldset.getFields()){
			Schema.SObjectField sof = fields.get(fsm.getFieldPath());
			DescribeFieldResult dfr = sof.getDescribe();
			List<Schema.SObjectType> types = dfr.getReferenceTo();
			if(types != null && types.size() > 0){
				dfrs.add(dfr);
				objLookupTypes.addAll(types);
			}
		}
		this.lookupSObjTypes = objLookupTypes;

		if (this.templateId == null) {
			this.sections = new list<ChecklistSection>();
			newSection();
		}
		else {
			map<string, ChecklistSection> sectionMap = new map<string, ChecklistSection>();
			for (Checklist_Template_Entry__c item : [
					select id, name, Checklist_Template__c, Section__c, Sort_Order__c, Due_Date_Based_On__c, Due_Date_Offset__c, Save_Task_Notes__c,
							Default_Deal_Team_Role__c, Default_Notes__c
						from Checklist_Template_Entry__c
					 where Checklist_Template__c = :this.templateId
			]) {
				if (sectionMap.containsKey(item.Section__c) == false) {
					sectionMap.put(item.Section__c, new ChecklistSection(item.Section__c, sectionMap.size()));
				}
				sectionMap.get(item.Section__c).addItem(new ChecklistTemplateEntry(item));
			}
			this.sections = sectionMap.values();
		}

	}

	public PageReference newItem() {
		system.debug('newItem:  actionSectionIndex = ' + this.actionSectionIndex);
		ChecklistSection section = this.sections[this.actionSectionIndex];
		section.items.add(new ChecklistTemplateEntry(section.items.size()));
		return null;
	}

	public PageReference deleteItem() {
		// track deletions for later DB updates
		ChecklistSection section = this.sections[this.actionSectionIndex];
		deletedItems.add(section.items[this.actionItemIndex]);
		section.removeItem(this.actionItemIndex);
		return null;
	}

	public PageReference newSection() {
		this.sections.add(new ChecklistSection(this.sections.size()));
		return null;
	}

	public PageReference deleteSection() {
		if (this.actionSectionIndex == null || this.actionSectionIndex < 0 || this.actionSectionIndex > this.sections.size()-1 ) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Could not delete section: Invalid Section Index'));
			return null;
		}
		// track deletions for later DB updates
		ChecklistSection section = this.sections[this.actionSectionIndex];
		integer ndx = this.actionSectionIndex;
		deletedItems.addAll(section.items);
		deletedSections.add(section);

		for (integer i = (ndx+1); i<this.sections.size(); i++) {
			this.sections[i].index--;
		}
		this.sections.remove(ndx);
		return null;
	}

	public PageReference saveTemplate() {
		Checklist_Template__c template;
		if (validateTemplate() == true) {
			Savepoint sp = database.setSavepoint();
			try {
				// save the template (parent record)
				// ctrl.save();  // this is broken, c.f. https://salesforce.stackexchange.com/questions/4076/why-do-subsequent-calls-to-standardcontroller-save-insert-records-instead-of-u/4109#4109
				template = this.template; // calls .getRecord()
				upsert template; // now use template.id below instead of calling back to .getRecord() on the controller.

				// save template entries
				list<Checklist_Template_Entry__c> items = new list<Checklist_Template_Entry__c>();
				for (ChecklistSection sect : this.sections) {
					items.addAll(sect.getChecklistTemplateEntries(template.id));
				}
				upsert items;

				// delete deleted entries
				set<id> toDelete = new set<id>();
				for (ChecklistTemplateEntry item : deletedItems) {
					if (item.id != null) {
						toDelete.add(item.id);
					}
				}
				delete [select id from Checklist_Template_Entry__c where id in :toDelete];

			}
			catch (Exception e) {
				Database.Rollback(sp);
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Could not save template: ' + e.getMessage()));
				return null;
			}
		}else{
			return null;
		}
		return new ApexPages.StandardController(template).view();
	}

	private boolean validateTemplate() {
		if (this.template.Name == null || this.template.Name.trim() == '') {
			this.template.Name.addError('You must specify a Template Name.');
		}
		if (this.template.Object__c == null || this.template.Object__c.trim() == '') {
			this.template.Name.addError('You must specify an object type for this template');
		}
		//FIXME Moar tests!!1

		return ApexPages.hasMessages() == false;
	}

	public class ChecklistSection {
		public integer index {get; set;}
		public string name {get; set;}
		public list<ChecklistTemplateEntry> items {get; set;}

		public ChecklistSection(integer ndx) {
			this('Section ' + (ndx+1), ndx);
			this.items.add(new ChecklistTemplateEntry(0));
		}

		public ChecklistSection(string sectionName, integer ndx) {
			this.index = ndx;
			this.name = sectionName;
			this.items = new list<ChecklistTemplateEntry>();
		}

		public void removeItem(integer ndx) {
			if (ndx < 0 || ndx >= this.items.size()) return;
			for (integer i = (ndx+1); i< this.items.size(); i++) {
				this.items[i].index--;
			}
			this.items.remove(ndx);
		}

		public void addItem(ChecklistTemplateEntry item) {
			item.index = this.items.size();
			this.items.add(item);
		}

		public list<Checklist_Template_Entry__c> getChecklistTemplateEntries(id templateId) {
			list<Checklist_Template_Entry__c> res = new list<Checklist_Template_Entry__c>();
			for (ChecklistTemplateEntry item : this.items) {
				res.add(item.getChecklistTemplateEntry(this.name, templateId));
			}
			return res;
		}

	}

	public class ChecklistTemplateEntry {
		private Checklist_Template_Entry__c entry {get; set;}
		public integer index {get; set;}
		public string description {get; set;}
		public string dueDateBasedOn {get; set;}
		public string defaultDealContactRole {get; set;}
		public string defaultTaskNotes {get; set;}
		public integer dueDateOffset {get; set;}
		public boolean saveTaskNotes {get; set;}
		public id id {
			get {
				return this.entry == null ? null : this.entry.id;
			}
		}

		public ChecklistTemplateEntry(integer ndx) {
			this.entry = null;
			this.index = ndx;
			this.dueDateBasedOn = DEFAULT_DUE_DATE_BASED_ON;
			this.dueDateOffset = 0;
			this.saveTaskNotes = true;
		}

		public ChecklistTemplateEntry(Checklist_Template_Entry__c obj) {
			this.entry = obj;
			this.description = obj.name;
			this.dueDateBasedOn = obj.Due_Date_Based_On__c;
			this.dueDateOffset = (integer)obj.Due_Date_Offset__c;
			this.saveTaskNotes = obj.Save_Task_Notes__c;
			this.defaultDealContactRole = obj.Default_Deal_Team_Role__c;
			this.defaultTaskNotes = obj.Default_Notes__c;
		}

		Checklist_Template_Entry__c getChecklistTemplateEntry(string section, id templateId) {
			if (this.entry == null) {
				this.entry = new Checklist_Template_Entry__c(Checklist_Template__c = templateId);
			}
			this.entry.name = this.description;
			this.entry.Section__c = section;
			this.entry.Sort_Order__c = this.index;
			this.entry.Due_Date_Based_On__c = this.dueDateBasedOn;
			this.entry.Due_Date_Offset__c = this.dueDateOffset;
			this.entry.Save_Task_Notes__c = this.saveTaskNotes;
			this.entry.Default_Deal_Team_Role__c = this.defaultDealContactRole;
			this.entry.Default_Notes__c = this.defaultTaskNotes;
			return this.entry;
		}
	}
}